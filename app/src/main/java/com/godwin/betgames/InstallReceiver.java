package com.godwin.betgames;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class InstallReceiver extends BroadcastReceiver {

    private static String referrer = "";
    Session session;

    InstallReceiver(){

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        session = new Session(context);
        referrer = "";
        Bundle extras = intent.getExtras();
        if(extras != null){
            referrer = extras.getString("referrer");
            session.setReferer(referrer);
        }

        Log.w("REFERRER","Referer is: "+referrer);
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
