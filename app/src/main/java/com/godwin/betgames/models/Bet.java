package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Bet implements Serializable {
    @SerializedName("success") @Expose private Success success;
    @SerializedName("uuid") private String uuid;
    @SerializedName("amt") private String amt;
    @SerializedName("game") private String game;
    @SerializedName("state") private String state;
    @SerializedName("created_at") private String created_at;
    private final static long serialVersionUID = 6432583364521051753L;


    public Bet(String uuid, String amt, String game, String state, String created_at, Success success) {

        this.success = success;
        this.uuid = uuid;
        this.amt = amt;
        this.game = game;
        this.state = state;
        this.created_at = created_at;
    }
    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    public String getAmt(){
        return amt;
    }

    public String setAmt(String amt){
        this.amt = amt;
        return amt;
    }

    public String getGame(){
        return game;
    }

    public String setGame(String game){
        this.game = game;
        return game;
    }

    public String getUuid(){
        return uuid;
    }

    public String setUuid(String uuid){
        this.uuid = uuid;
        return uuid;
    }

    public String getState(){
        return state;
    }

    public String setState(String state){
        this.state = state;
        return state;
    }

    public String getCreatedAt(){
        return created_at;
    }

    public String setCreatedAt(String created_at){
        this.created_at = created_at;
        return created_at;
    }

}