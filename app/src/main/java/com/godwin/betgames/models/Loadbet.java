package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Loadbet implements Serializable {
    @SerializedName("success") @Expose private Successer success;
    private final static long serialVersionUID = 7893483364521051753L;


    public Loadbet(Successer success) {
        this.success = success;
    }

    public Successer getSuccess() {
        return success;
    }

    public void setSuccess(Successer success) {
        this.success = success;
    }

}