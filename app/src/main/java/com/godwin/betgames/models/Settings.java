package com.godwin.betgames.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Settings implements Serializable
{

    @SerializedName("locale")
    @Expose
    private String locale;
    private final static long serialVersionUID = -6430602002208456647L;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("locale", locale).toString();
    }

}
