package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SuccessLoad implements Serializable {

    @SerializedName("id") @Expose private String id;
    @SerializedName("uuid") @Expose private String uuid;
    @SerializedName("acceptor") @Expose private String acceptor;
    @SerializedName("amt") @Expose private String amt;
    @SerializedName("game") @Expose private String game;
    @SerializedName("state") @Expose private String state;
    @SerializedName("created_at") @Expose private String created_at;
    private final static long serialVersionUID = 489213543348202708L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAcceptor() {
        return acceptor;
    }

    public void setAcceptor(String acceptor) {
        this.acceptor = acceptor;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    @Override
    public String toString() {
        //return new ToStringBuilder(this).append("id", id).append("uuid", uuid).append("amt", amt).append("game", game).append("state", state).append("created_at", created_at).toString();
        //return "id="+id+",uuid="+uuid+",amt="+amt+",game="+game+",state="+state+",created_at="+created_at;
        return "{id :"+id+",uuid :"+uuid+",amt :"+amt+",game :"+game+",state :"+state+",created_at :"+created_at+"}";
    }

}