package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Successar implements Serializable {

    @SerializedName("nota") @Expose private String nota;
    private final static long serialVersionUID = 578966543348202708L;


    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }
}