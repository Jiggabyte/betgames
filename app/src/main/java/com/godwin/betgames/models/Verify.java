package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

import androidx.annotation.NonNull;

public class Verify implements Serializable {

    @SerializedName("sms_code") @Expose private String sms_code;
    @SerializedName("phone") @Expose private String phone;
    @SerializedName("device") @Expose private String device;
    @SerializedName("status") @Expose private String status;


    private final static long serialVersionUID = 6278069786534567413L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSms_code() {
        return sms_code;
    }

    public void setSms_code(String sms_code) {
        this.sms_code = sms_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Verify{" +
                "sms_code='" + sms_code + '\'' +
                ", phone='" + phone + '\'' +
                ", device='" + device + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}