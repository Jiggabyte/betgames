package com.godwin.betgames.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;



public class BetgamesDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Betgames_DB.db";
    public static final String EXTRAS_TABLE_NAME = "extras";
    public static final String EXTRAS_COLUMN_ID = "id";
    public static final String EXTRAS_COLUMN_NAME = "name";
    public static final String EXTRAS_COLUMN_DETAIL = "detail";
    public static final String EXTRAS_COLUMN_USER = "user";
    public static final String EXTRAS_COLUMN_NOTE = "note";
    public static final String EXTRAS_COLUMN_MISC = "misc";
    private HashMap hp;

    public BetgamesDBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table extras" +
                        "(id integer primary key, name text,detail text,user text, note text,misc text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS extras");
        onCreate(db);
    }

    public boolean inserter (String name, String detail, String user, String note,String misc) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("detail", detail);
        contentValues.put("user", user);
        contentValues.put("note", note);
        contentValues.put("misc", misc);
        db.insert("extras", null, contentValues);
        return true;
    }

    public Cursor getData(String namer) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery( "select detail from extras where name is null or name = '"+namer+"' ", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, EXTRAS_TABLE_NAME);
        return numRows;
    }

    public boolean updater (Integer id, String name, String detail, String user, String note,String misc) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("detail", detail);
        contentValues.put("user", user);
        contentValues.put("note", note);
        contentValues.put("misc", misc);
        db.update("extras", contentValues, " id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleter (String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("extras",
                "name = ? ",
                new String[] { name });
    }

    public ArrayList<String> getAller() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from extras", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex(EXTRAS_COLUMN_NAME)));
            res.moveToNext();
        }
        return array_list;
    }

}