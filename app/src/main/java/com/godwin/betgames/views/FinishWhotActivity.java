package com.godwin.betgames.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.CountDownTimer;
import android.media.MediaPlayer;
import android.widget.ToggleButton;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.godwin.betgames.MainActivity;
import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.Utils;
import com.godwin.betgames.models.Decker;
import com.godwin.betgames.models.Nota;
import com.godwin.betgames.models.Success;
import com.godwin.betgames.models.User;
import com.godwin.betgames.views.ui.challenger.ChallengerFragment;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.bumptech.glide.Glide;


public class FinishWhotActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageView betX;
    private TextView timeX;
    private TextView timeZ;
    private int timor;

    private ImageView carda;
    private ImageView carda1;
    private ImageView carda2;
    private ImageView carda3;
    private ImageView carda4;

    private ImageView cardw;
    private ImageView cardw1;
    private ImageView cardw2;
    private ImageView cardw3;
    private ImageView cardw4;

    private ImageView cardb;
    private ImageView cardb1;
    private ImageView cardb2;
    private ImageView cardb3;
    private ImageView cardb4;

    private ImageView cardxi;
    private ImageView cardyi;

    private ImageView loader;

    private FragmentManager fragmentManager;

    ArrayList<String> cardy = new ArrayList<String>();

   // ArrayList<String> cardx = new ArrayList<String>();
   // ArrayList<String> cardp1 = new ArrayList<String>();
   // ArrayList<String> cardp2 = new ArrayList<String>();

    ArrayList<String> cardx = new ArrayList<String>();
    ArrayList<String> cardp1 = new ArrayList<String>();
    ArrayList<String> cardp2 = new ArrayList<String>();

    ArrayList<String> cardDeck1 = new ArrayList<String>();
    ArrayList<String> cardDeck2 = new ArrayList<String>();

    private String turnCounter = "none";
    private String pickTwo = "none";
    private String generalMarket = "none";

    private String holdOn = "none";
    String commandChange = "none";
    private boolean needWhot;
    private String playedLast;
    private boolean gameOver = false;
    private boolean freezeGame = false;
    private boolean freezeCard = false;
    private boolean freezeWhot = false;
    private boolean freezeMark = false;
    private boolean reloadFreeze = false;

    private String winner = "none";
    private String loser = "none";

    private String cardName;
    private String cardNamePart;
    private int cardNameNum;

    private int cardix1;
    private int cardix2;
    private int cardix3;

    private String cardNameN;
    private String cardNamePartN;
    private int cardNameNumN;

    private TextView cardNum;
    private TextView turner;
    private TextView more;

    private TextView cardWhotix;

    public Button exitButton;
    public Button replayButton;

    private String player;
    private String player1x;
    private String player2x;

    ArrayList<String> playerOneMoves = new ArrayList<String>();
    ArrayList<String> playerTwoMoves = new ArrayList<String>();

    ArrayList<String> cardxy = new ArrayList<String>();

    private String netStatus;

    private int gameId;
    private String bet_id;

    private String marketDeckSize;

    private String cd2;

    private boolean backpressed;

    private boolean playerTwoMove;
    private boolean playerTwoReceive;
    private boolean playerTwoMarketMove;
    private boolean playerTwoMarketReceive;
    private boolean playerTwoWhotMove;
    private boolean playerTwoWhotReceive;

    private boolean dialoger;

    private ProgressDialog pDialog;

    private String[] parameter = new String[9];

    private ToggleButton toggler;

    private boolean soundOn;

    public Session session;

    Handler handler;

    Runnable runnablec;

    Runnable runnable;
    Runnable runnablex;

    Runnable runnablem;
    Runnable runnablemx;

    Runnable runnablew;
    Runnable runnablewx;

    Runnable runnableo;

    Runnable runnablet;

    ObjectAnimator animatorX;
    ObjectAnimator animatorY;

    ViewGroup slotView1;
    ViewGroup slotView2;

    MediaPlayer mediaPlayer;
    MediaPlayer mediaPlayerx;

    HorizontalScrollView hsv1;
    HorizontalScrollView hsv2;

    long megAvailable;

    public static boolean active = false;

    ValueEventListener valueEventListener;

    ValueEventListener startxEventListener;
    ValueEventListener playxEventListener;

    ProgressDialog p;

    String resulter;

    boolean skipper = false;

    private BetgamesDBHelper betgamedb;
    boolean game_state_res;

    Runnable runner;
    Runnable runnablx;


    /**
     * This method is called after {@link #onStart} when the activity is
     * being re-initialized from a previously saved state, given here in
     * <var>savedInstanceState</var>.  Most implementations will simply use {@link #onCreate}
     * to restore their state, but it is sometimes convenient to do it here
     * after all of the initialization has been done or to allow subclasses to
     * decide whether to use your default implementation.  The default
     * implementation of this method performs a restore of any view state that
     * had previously been frozen by {@link #onSaveInstanceState}.
     *
     * <p>This method is called between {@link #onStart} and
     * {@link #onPostCreate}.
     *
     * @param savedInstanceState the data most recently supplied in {@link #onSaveInstanceState}.
     * @see #onCreate
     * @see #onPostCreate
     * @see #onResume
     * @see #onSaveInstanceState
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        skipper = true;
        // FinishWhotActivity.super.onBackPressed();
        RestService servicer = RestClient.getClient(session).create(RestService.class);
        Call<User> callretro = servicer.userOnback("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

        callretro.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String jres = response.body().getSuccessor();

                        //  Toast.makeText(FinishWhotActivity.this, "OnBacker: "+response.body().getSuccessor() , Toast.LENGTH_LONG).show();
                        if (jres.equals("success")) {
                            backpressed = true;

                            AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                            dialog.setCancelable(false);
                            dialog.setTitle("You Lost!");
                            dialog.setMessage("Sorry, you're not lucky this time.");
                            dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    showpDialog();

                                    Intent intentR = new Intent(FinishWhotActivity.this, MainActivity.class);
                                    intentR.putExtra("stopper", "false");
                                    intentR.putExtra("usernome", session.getUserID());
                                    if (session.getUserID().equals(player1x)) {
                                        intentR.putExtra("userchaller", player2x);
                                    } else {
                                        intentR.putExtra("userchaller", player1x);
                                    }
                                    intentR.putExtra("betGame", "Finish&Count");
                                    intentR.putExtra("bet_id", bet_id);
                                    Log.d("BET_ID", "Msg: " + bet_id);
                                    intentR.putExtra("game_id", Integer.toString(gameId));

                                    startActivity(intentR);
                                    hidepDialog();
                                    FinishWhotActivity.this.finish();
                                }
                            });
                            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    exitButton.setVisibility(View.VISIBLE);
                                    exitButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            RestService servicer = RestClient.getClient(session).create(RestService.class);
                                            Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                            call.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {

                                                        } else {

                                                        }

                                                    } else {


                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {

                                                }
                                            });
                                            session.setGameState(null);betgamedb.deleter("game_state");

                                            session.setChallengeState(null);

                                            session.setNotaRec(null);
                                            session.setNoteRec(null);
                                            session.setNotiRec(null);
                                            session.setNotoRec(null);
                                            session.setNotuRec(null);
                                            session.setNotyRec(null);
                                            session.setWinaRec(null);
                                            session.setLosaRec(null);
                                            session.setPlay("false");
                                            session.setReqNota(null);
                                            session.setUserBeta(null);
                                            session.setUserAcc(null);
                                            session.setBetId(null);

                                            Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                            intentOne.putExtra("stopper", "false");
                                            startActivity(intentOne);
                                            FinishWhotActivity.this.finish();
                                        }
                                    });
                                }
                            });

                            final AlertDialog alert = dialog.create();
                            gameOver = true;
                            Handler handler = new Handler();
                            int delay = 3000; //milliseconds

                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    //do something
                                    if (!dialoger) {
                                        if (!isFinishing()) {

                                            //  alert.show();
                                            //  playLoseSound();
                                        } else {

                                        }

                                    }

                                }
                            }, delay);
                            session.setGameState(null);betgamedb.deleter("game_state");
                            session.setEndState(gameId);
                            session.setChallengeState(null);
                            session.setAppState(null);
                            //   timer.cancel();
                            gameSound("stop");
                            soundOn = false;


                            //displayer("Game Over","Sorry, You lost!");
                            //handlerx.removeCallbacks(this);
                            freezeGame = true;
                            timer.cancel();

                            Toast.makeText(FinishWhotActivity.this, "Game Over, You Lost!", Toast.LENGTH_LONG).show();
                            if (checkWinner(0, "p1")) {
                                gameSound("stop");
                                soundOn = false;
                            }
                        }
                        session.setNotaRec(null);
                        session.setNoteRec(null);
                        session.setNotiRec(null);
                        session.setNotoRec(null);
                        session.setNotuRec(null);
                        session.setNotyRec(null);
                        session.setWinaRec(null);
                        session.setLosaRec(null);
                        session.setBetId(null);

                    } else {

                    }

                } else {

                    //    Toast.makeText(FinishWhotActivity.this, "OnBacker: "+response.toString() , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                //     Toast.makeText(FinishWhotActivity.this, "OnBacker: "+t.toString() , Toast.LENGTH_LONG).show();

                Log.d("onFailure", t.toString());


            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // super.onCreate(savedInstanceState);
        super.onCreate(null);
        session = new Session(FinishWhotActivity.this);
        // session.setHomeState(null);

        LocalBroadcastManager.getInstance(FinishWhotActivity.this).registerReceiver(callMsgReceiver,
                new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED));

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //    WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        setContentView(R.layout.activity_finish);
        session.setSplashState("active");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                finishMessageReceiver, new IntentFilter("GamePlayData"));

        pDialog = new ProgressDialog(FinishWhotActivity.this,  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        handler = new Handler();

        active = true;

        backpressed = false;

        dialoger = false;

        toggler = findViewById(R.id.soundButton);

        betgamedb = new BetgamesDBHelper(FinishWhotActivity.this);

        //Performing action on toggleButton click
        toggler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!gameOver) {
                    if (toggler.getText().toString().trim().equals("OFF")) {
                        gameSound("stop");
                        soundOn = false;
                    } else {
                        if (soundOn) {

                        } else {
                            gameSound("play");
                            soundOn = true;
                        }
                    }
                }
            }

        });

        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        megAvailable = bytesAvailable / (1024 * 1024);


        // Creating from scratch
        betX = findViewById(R.id.betImg);
        timeX = findViewById(R.id.timer);
        timeZ = findViewById(R.id.timez);

        cardxi = findViewById(R.id.cardX);
        cardyi = findViewById(R.id.cardY);

        cardNum = findViewById(R.id.cardNumb);

        slotView1 = findViewById(R.id.cl1);
        slotView2 = findViewById(R.id.cl2);

        cardw = findViewById(R.id.t);
        cardw1 = findViewById(R.id.st);
        cardw2 = findViewById(R.id.sq);
        cardw3 = findViewById(R.id.k);
        cardw4 = findViewById(R.id.c);

        cardWhotix = findViewById(R.id.whotter);

        hsv1 = findViewById(R.id.sc1);
        hsv2 = findViewById(R.id.sc2);

        turner = findViewById(R.id.turner);
        more = findViewById(R.id.moreCards);

        exitButton = findViewById(R.id.exitButton);
        replayButton = findViewById(R.id.replayButton);

        loader = findViewById(R.id.loader);

        ConstraintLayout cl3 = findViewById(R.id.clx);

        final float density = this.getResources().getDisplayMetrics().density;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        Intent intent = getIntent();

        // Get the extras (if there are any)
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("id")) {
                int id;
                String player1;
                String player2;
                String marketSize;
                String one;
                String two;
                String play;
                String turn;
                String updater;

                id = intent.getIntExtra("id",0);
                bet_id = intent.getStringExtra("bet_id");
                Log.d("BET_ID","1st One MSG: "+bet_id);
                player1 = intent.getStringExtra("player1");
                player2 = intent.getStringExtra("player2");
                marketSize = intent.getStringExtra("market_size");
                one = intent.getStringExtra("one");
                two = intent.getStringExtra("two");
                play = intent.getStringExtra("play");
                turn = intent.getStringExtra("turn");
                updater = intent.getStringExtra("timer");

                if(player1.equals(session.getUserID())){
                    player = player1;
                    player1x = player1;
                    player2x = player2;
                } else {
                    player = player2;
                    player1x = player1;
                    player2x = player2;
                }

              //  try {

                    gameId = id;

                    marketDeckSize = marketSize;

                    if(player1.equals(session.getUserID())){

                        JSONObject play1Obj = null;
                        try {
                            play1Obj = new JSONObject(one);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for(int i=0;i<play1Obj.length();i++){
                            cardp2.add(play1Obj.optString(""+i));
                            Log.d("cardp2",play1Obj.optString(""+i));


                        }
                       // cardp2 = new ArrayList<>(one);

                        cardp1.add("jz1");
                        cardp1.add("jz2");
                        cardp1.add("jz3");
                        cardp1.add("jz4");
                        cardp1.add("jz5");

                    } else {

                        JSONObject play2Obj = null;
                        try {
                            play2Obj = new JSONObject(two);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for(int i=0;i<play2Obj.length();i++){
                            cardp2.add(play2Obj.optString(""+i));
                            Log.d("cardp2x",play2Obj.optString(""+i));
                        }

                      //  cardp2 = new ArrayList<>(two);

                        cardp1.add("jz1");
                        cardp1.add("jz2");
                        cardp1.add("jz3");
                        cardp1.add("jz4");
                        cardp1.add("jz5");
                    }


                JSONObject playObj = null;
                try {
                    playObj = new JSONObject(play);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for(int i=0;i<playObj.length();i++){
                        cardx.add(playObj.optString(""+i));
                    }

                   // cardx = new ArrayList<>(play);

                    if(player1.equals(session.getUserID())){
                        if(turn.equals("player1")){
                            turnCounter = "p2";
                        } else {
                            turnCounter = "p1";
                            turner.setVisibility(View.INVISIBLE);
                            timer.cancel();
                        }
                    } else if(player2.equals(session.getUserID())){
                        if (turn.equals("player2")) {
                            turnCounter = "p2";
                        } else {
                            turnCounter = "p1";
                            turner.setVisibility(View.INVISIBLE);
                            timer.cancel();
                        }
                    } else {

                    }

                    session.setDataState(updater);
                    // Toast.makeText(FinishWhotActivity.this, "Updater: "+session.getDataState()+" "+updater, Toast.LENGTH_SHORT).show();

              //  } catch (JSONException e) {
              //      e.printStackTrace();
             //   }

            } else {

            }

        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 1000ms
                session.setNotaRec(null);
                session.setNoteRec(null);
                session.setNotiRec(null);
                session.setNotoRec(null);
                session.setNotuRec(null);
                session.setNotyRec(null);

             //   Toast.makeText(FinishWhotActivity.this, "GameStater: "+session.getGameState(), Toast.LENGTH_SHORT).show();

                String game_state = null;

                try {
                    Cursor rs = betgamedb.getData("game_state");

                    if (rs.moveToFirst()) {
                        game_state = rs.getString(rs.getColumnIndex(BetgamesDBHelper.EXTRAS_COLUMN_DETAIL));
                    }
                }
                catch(SQLiteException e){
                    e.printStackTrace();
                }

                if(game_state != null){

                    // FinishWhotActivity.super.onBackPressed();
                    RestService servicer = RestClient.getClient(session).create(RestService.class);
                    Call<User> callretro = servicer.userOnback("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                    callretro.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {

                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    String jres = response.body().getSuccessor();

                                    //  Toast.makeText(FinishWhotActivity.this, "OnBacker: "+response.body().getSuccessor() , Toast.LENGTH_LONG).show();
                                    if (jres.equals("success")) {
                                        backpressed = true;

                                        AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                                        dialog.setCancelable(false);
                                        dialog.setTitle("You Lost!");
                                        dialog.setMessage("Sorry, you're not lucky this time.");
                                        dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {

                                                showpDialog();

                                                Intent intentR = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                intentR.putExtra("stopper", "false");
                                                intentR.putExtra("usernome", session.getUserID());
                                                if (session.getUserID().equals(player1x)) {
                                                    intentR.putExtra("userchaller", player2x);
                                                } else {
                                                    intentR.putExtra("userchaller", player1x);
                                                }
                                                intentR.putExtra("betGame", "Finish&Count");
                                                intentR.putExtra("bet_id", bet_id);
                                                Log.d("BET_ID", "Msg: " + bet_id);
                                                intentR.putExtra("game_id", Integer.toString(gameId));

                                                startActivity(intentR);
                                                hidepDialog();
                                                FinishWhotActivity.this.finish();
                                            }
                                        });
                                        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {

                                                exitButton.setVisibility(View.VISIBLE);
                                                exitButton.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                        Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                        call.enqueue(new Callback<User>() {
                                                            @Override
                                                            public void onResponse(Call<User> call, Response<User> response) {

                                                                if (response.isSuccessful()) {
                                                                    if (response.body() != null) {

                                                                    } else {

                                                                    }

                                                                } else {


                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<User> call, Throwable t) {

                                                            }
                                                        });
                                                        session.setGameState(null);betgamedb.deleter("game_state");

                                                        session.setChallengeState(null);

                                                        session.setNotaRec(null);
                                                        session.setNoteRec(null);
                                                        session.setNotiRec(null);
                                                        session.setNotoRec(null);
                                                        session.setNotuRec(null);
                                                        session.setNotyRec(null);
                                                        session.setWinaRec(null);
                                                        session.setLosaRec(null);
                                                        session.setPlay("false");
                                                        session.setReqNota(null);
                                                        session.setUserBeta(null);
                                                        session.setUserAcc(null);
                                                        session.setBetId(null);

                                                        Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                        intentOne.putExtra("stopper", "false");
                                                        startActivity(intentOne);
                                                        FinishWhotActivity.this.finish();
                                                    }
                                                });
                                            }
                                        });

                                        final AlertDialog alert = dialog.create();
                                        gameOver = true;
                                        Handler handler = new Handler();
                                        int delay = 3000; //milliseconds

                                        handler.postDelayed(new Runnable() {
                                            public void run() {
                                                //do something
                                                if (!dialoger) {
                                                    if (!isFinishing()) {

                                                        //  alert.show();
                                                        //  playLoseSound();
                                                    } else {

                                                    }

                                                }

                                            }
                                        }, delay);
                                        session.setGameState(null);betgamedb.deleter("game_state");
                                        session.setEndState(gameId);
                                        session.setChallengeState(null);
                                        session.setAppState(null);
                                        //   timer.cancel();
                                        gameSound("stop");
                                        soundOn = false;


                                        //displayer("Game Over","Sorry, You lost!");
                                        //handlerx.removeCallbacks(this);
                                        freezeGame = true;
                                        timer.cancel();

                                        Toast.makeText(FinishWhotActivity.this, "Game Over, You Lost!", Toast.LENGTH_LONG).show();
                                        if (checkWinner(0, "p1")) {
                                            gameSound("stop");
                                            soundOn = false;
                                        }
                                    }
                                    session.setNotaRec(null);
                                    session.setNoteRec(null);
                                    session.setNotiRec(null);
                                    session.setNotoRec(null);
                                    session.setNotuRec(null);
                                    session.setNotyRec(null);
                                    session.setWinaRec(null);
                                    session.setLosaRec(null);
                                    session.setBetId(null);

                                } else {

                                }

                            } else {

                                //    Toast.makeText(FinishWhotActivity.this, "OnBacker: "+response.toString() , Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            //     Toast.makeText(FinishWhotActivity.this, "OnBacker: "+t.toString() , Toast.LENGTH_LONG).show();

                            Log.d("onFailure", t.toString());


                        }
                    });

                } else {

                    session.setGameState("Finish");
                    if(betgamedb.inserter("game_state","finish",session.getUserID(),Integer.toString(gameId),"active")){
                        Cursor rs = betgamedb.getData("game_state");

                        if(rs.moveToFirst()) {
                            game_state = rs.getString(rs.getColumnIndex(BetgamesDBHelper.EXTRAS_COLUMN_DETAIL));
                        }

                      //  Toast.makeText(FinishWhotActivity.this, "GameState Saved: "+game_state, Toast.LENGTH_SHORT).show();
                    }



                    initGame(handler);

                }
                cardNum.setText("["+marketDeckSize+"]");
                gameSound("play");
                soundOn = true;


            }
        }, 1000);


        final int delay = 5000; //milliseconds

        cardxi.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          if(cardx.size() >= 4) {
                                              ImageView cardxFirst = creatorz(null,cardx.get(cardx.size() - 1));
                                              ImageView cardxSecond = creatorz(cardxFirst,cardx.get(cardx.size() - 2));
                                              creatorz(cardxSecond,cardx.get(cardx.size() - 3));
                                          }

                                      }
                                  });


        cardyi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!gameOver) {
                    playCardSound();
                    if (freezeMark) {
                        Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (freezeWhot){
                        Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (freezeCard){
                        Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (freezeGame){
                        Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    slotView1 = findViewById(R.id.cl1);
                    slotView2 = findViewById(R.id.cl2);
                    ImageView slotter = (ImageView) slotView2.getChildAt(slotView2.getChildCount() - 1);
                    if (turnCounter.equals("p1")) {


                    } else if(turnCounter.equals("p2")) {
                        loader.setVisibility(View.VISIBLE);
                        Glide.with(FinishWhotActivity.this).load(R.drawable.loader).into(loader);
                        playerTwoMarketMove = true;
                        playerTwoMarketReceive = false;

                        freezeMark = true;

                        RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                        Call<User> call = servicePlay.userMark("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"marketer",Integer.toString(gameId),(String)timeX.getText());

                        call.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {

                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if(response.body().getSuccess().getError() != null){
                                 //           Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                        } else {
                                  //          Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                        }

                                    } else {
                                        Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                               //     Toast.makeText(FinishWhotActivity.this, "Error: "+response.toString(), Toast.LENGTH_SHORT).show();
                                    if(playerTwoMarketReceive){
                                        playerTwoMarketMove = true;
                                        loader.setVisibility(View.INVISIBLE);
                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                    } else {
                                        playerTwoMarketMove = false;

                                        runnablem = new Runnable(){
                                            public void run(){
                                                //do something
                                                if(playerTwoMarketReceive){
                                                    loader.setVisibility(View.INVISIBLE);
                                                    Glide.with(FinishWhotActivity.this).clear(loader);
                                                    handler.removeCallbacks(runnablem);
                                                } else {
                                                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                    Call<User> cally = servicePlay.userMark("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"marketer",Integer.toString(gameId),(String)timeX.getText());

                                                    cally.enqueue(new Callback<User>() {
                                                        @Override
                                                        public void onResponse(Call<User> call, Response<User> response) {

                                                            if (response.isSuccessful()) {
                                                                if (response.body() != null) {
                                                                    if(response.body().getSuccess().getError() != null){
                                                                 //       Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                                    } else {
                                                                //        Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                        handler.removeCallbacks(runnablem);
                                                                    }

                                                                } else {
                                                                    handler.removeCallbacks(runnablem);
                                                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                }
                                                            } else {

                                                                if(playerTwoMarketReceive){
                                                                    playerTwoMarketMove = true;
                                                                    loader.setVisibility(View.INVISIBLE);
                                                                    Glide.with(FinishWhotActivity.this).clear(loader);
                                                                    handler.removeCallbacks(runnablem);
                                                                } else {
                                                                    playerTwoMarketMove = false;

                                                                    runnablemx = new Runnable(){
                                                                        public void run(){
                                                                            //do something
                                                                            if(playerTwoMarketReceive){
                                                                                playerTwoMarketMove = true;
                                                                                loader.setVisibility(View.INVISIBLE);
                                                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                                                handler.removeCallbacks(runnablemx);
                                                                            } else {

                                                                            }

                                                                        }
                                                                    };

                                                                    handler.postDelayed(runnablemx, delay);
                                                                    // Continue from to fix market failure and others

                                                                }
                                                                // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                        @Override
                                                        public void onFailure(Call<User> call, Throwable t) {
                                                            if(playerTwoMarketReceive){
                                                                playerTwoMarketMove = true;
                                                                loader.setVisibility(View.INVISIBLE);
                                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                                handler.removeCallbacks(runnablem);
                                                            } else {
                                                                playerTwoMarketMove = false;

                                                                runnablemx = new Runnable(){
                                                                    public void run(){
                                                                        //do something
                                                                        if(playerTwoMarketReceive){
                                                                            playerTwoMarketMove = true;
                                                                            loader.setVisibility(View.INVISIBLE);
                                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                                            handler.removeCallbacks(runnablemx);
                                                                        } else {

                                                                        }

                                                                    }
                                                                };

                                                                handler.postDelayed(runnablemx, delay);

                                                            }
                                                            // Toast.makeText(FinishWhotActivity.this, "Error: "+t.toString(), Toast.LENGTH_LONG).show();
                                                        }
                                                    });
                                                    if(playerTwoMarketReceive){
                                                        playerTwoMarketMove = true;
                                                        loader.setVisibility(View.INVISIBLE);
                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                    } else {
                                                        handler.postDelayed(this, delay);
                                                    }

                                                }

                                            }
                                        };

                                        if(playerTwoMarketReceive && playerTwoMarketMove){
                                            handler.removeCallbacks(runnablem);

                                        } else {
                                            handler.postDelayed(runnablem, delay);
                                        }

                                    }
                                    // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                              //  Toast.makeText(FinishWhotActivity.this, "Err: "+t.toString(), Toast.LENGTH_SHORT).show();
                                if(playerTwoMarketReceive){
                                    playerTwoMarketMove = true;
                                    loader.setVisibility(View.INVISIBLE);
                                    Glide.with(FinishWhotActivity.this).clear(loader);
                                    handler.removeCallbacks(runnablem);
                                } else {
                                    playerTwoMarketMove = false;

                                    runnablem = new Runnable(){
                                        public void run(){
                                            //do something
                                            if(playerTwoMarketReceive){
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablem);
                                            } else {
                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> cally = servicePlay.userMark("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"marketer",Integer.toString(gameId),(String)timeX.getText());

                                                cally.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                                if(response.body().getSuccess().getError() != null){
                                                             //       Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                                } else {
                                                             //       Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                    handler.removeCallbacks(runnablem);
                                                                }
                                                            } else {
                                                                handler.removeCallbacks(runnablem);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                            }
                                                        } else {

                                                            if(playerTwoMarketReceive){
                                                                playerTwoMarketMove = true;
                                                                loader.setVisibility(View.INVISIBLE);
                                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                                handler.removeCallbacks(runnablem);
                                                            } else {
                                                                playerTwoMarketMove = false;

                                                                runnablemx = new Runnable(){
                                                                    public void run(){
                                                                        //do something
                                                                        if(playerTwoMarketReceive){
                                                                            playerTwoMarketMove = true;
                                                                            loader.setVisibility(View.INVISIBLE);
                                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                                            handler.removeCallbacks(runnablemx);
                                                                        } else {

                                                                        }

                                                                    }
                                                                };

                                                                handler.postDelayed(runnablemx, delay);
                                                                // Continue from to fix market failure and others

                                                            }
                                                            // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        if(playerTwoMarketReceive){
                                                            playerTwoMarketMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablem);
                                                        } else {
                                                            playerTwoMarketMove = false;

                                                            runnablemx = new Runnable(){
                                                                public void run(){
                                                                    //do something
                                                                    if(playerTwoMarketReceive){
                                                                        playerTwoMarketMove = true;
                                                                        loader.setVisibility(View.INVISIBLE);
                                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                                        handler.removeCallbacks(runnablemx);
                                                                    } else {

                                                                    }

                                                                }
                                                            };

                                                            handler.postDelayed(runnablemx, delay);

                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Error: "+t.toString(), Toast.LENGTH_LONG).show();
                                                    }
                                                });
                                                if(playerTwoMarketReceive){
                                                    playerTwoMarketMove = true;
                                                    loader.setVisibility(View.INVISIBLE);
                                                    Glide.with(FinishWhotActivity.this).clear(loader);
                                                } else {
                                                    handler.postDelayed(this, delay);
                                                }

                                            }

                                        }
                                    };

                                    if(playerTwoMarketReceive && playerTwoMarketMove){
                                        handler.removeCallbacks(runnablem);

                                    } else {
                                        handler.postDelayed(runnablem, delay);
                                    }

                                }
                                // Toast.makeText(FinishWhotActivity.this, "Error: "+t.toString(), Toast.LENGTH_LONG).show();
                            }
                        });


                    }

                } else {
                    Toast.makeText(FinishWhotActivity.this, "Game Over!", Toast.LENGTH_LONG).show();
                }

            }

        });


        // Get firebase database reference
        FirebaseDatabase databaser = FirebaseDatabase.getInstance();

        DatabaseReference gameDbReffer = databaser.getReference("gameons");

        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    //dataChanger(gameDbReffer,density,width,true);
                    if(netStatus == null){
                        netStatus = "Connected";
                    } else  if(netStatus.equals("Disconnected")){
                        netStatus = "Reconnected";
                        Toast.makeText(FinishWhotActivity.this, "Internet is Restored!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    netStatus = "Disconnected";
                    Toast.makeText(FinishWhotActivity.this, "Internet is Disconnected!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

        // Get firebase database reference
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference gameDbRef = database.getReference("gameons");

        gameDbRef.keepSynced(true);

        // Read from the database

        // List<Success> successPlay = new ArrayList<>();
        final HashMap<String, Success> successList = new HashMap<String, Success>();
        final ArrayList<Integer> successDiff = new ArrayList<>();

        gameDbRef.child("playx"+gameId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                successList.clear();
                successDiff.clear();
                Success success = new Success();
                Success successx = new Success();

                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    String playKey = postSnapshot.getKey();
                    if(playKey != null) {
                        String[] successArr = playKey.split("y");
                        successDiff.add(Integer.parseInt(successArr[1]));
                        int maxer = Collections.max(successDiff);
                        successx = postSnapshot.getValue(Success.class);
                        successList.put(playKey,successx);
                        success = successList.get("play"+maxer);
                    }

                   //  Toast.makeText(FinishWhotActivity.this, "CarderOne: "+gameOver+" "+turnCounter+" "+success.getNota(), Toast.LENGTH_SHORT).show();
                   //  here you can access to name property like university.name
                   //  Toast.makeText(FinishWhotActivity.this,differences.get(differences.size() - 1).getNota(),Toast.LENGTH_LONG).show();

                }

                if (success != null) {
                    if (!gameOver) {
                        if (turnCounter.equals("p1")) {
                            if (success != null && "".equals(success.getNote()) || success != null && "0".equals(success.getNote())) {

                                if (Integer.parseInt(marketDeckSize) == 0) {
                                    finishAndCount();
                                }

                            }

                            if (success != null && "".equals(success.getNota()) || success != null && "0".equals(success.getNota())) {


                            } else {

                                if (success != null && success.getContinua() != null && success.getContinua().equals("true")) {

                                    //  Toast.makeText(FinishWhotActivity.this, "second toast", Toast.LENGTH_SHORT).show();

                                } else {

                                    if (success != null && success.getUuid().equals(session.getUserID())) {
                                        if (!success.getNoti().equals("") && !success.getNoti().equals("whotter") && !success.getNoti().equals("marketer")) {

                                            playerTwoReceive = true;
                                            String[] checkWordArr = success.getNota().split("z");
                                            if (checkWordArr[0].equals("w") || checkWordArr[1].equals("1") || checkWordArr[1].equals("2") || checkWordArr[1].equals("14")) {

                                            } else {
                                                freezeCard = false;
                                            }
                                            ImageView slotti = (ImageView) slotView1.getChildAt(slotView1.getChildCount() - 1);
                                            String cart = success.getNota();

                                            playedLast = "p2";
                                            timerx.cancel();
                                            int delayO = 5000;

                                            RestService servicer = RestClient.getClient(session).create(RestService.class);
                                            Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                            calltime.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {
                                                            String jres = response.body().getSuccessor();
                                                            int jnum = Integer.parseInt(jres);
                                                            int jnumsecs = jnum % 60;
                                                            int jnummins = jnum / 60;
                                                            timeZ.setTextColor(Color.RED);
                                                            if (jnummins < 0 || jnumsecs < 0) {
                                                                timeZ.setText("Delay:  0  mins 0 secs");
                                                            } else {
                                                                timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                            }
                                                            //here you can have your logic to set text to edittext
                                                        } else {

                                                        }

                                                    } else {


                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {
                                                    // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                                }
                                            });

                                            String jsonResponse = success.getGame_type();
                                            String resPopData = success.getNoto();
                                            String resPopData1 = success.getNotu();
                                            String resPopData2 = success.getNoty();

                                            String[] carter = cart.split("z");
                                            int cardNameNum = Integer.parseInt(carter[1]);

                                            if (freezeGame) return;

                                            if ("Finish&Count".equals(jsonResponse) || "Classic".equals(jsonResponse)) {
                                                marketDeckSize = success.getNote();

                                                if (cardNameNum == 2) {
                                                    setPickTwo(resPopData1, resPopData2, "p2", "p1", slotti);

                                                } else if (cardNameNum == 14) {
                                                    setGeneralMarket(resPopData, "p2", "p1", slotti);

                                                } else {
                                                    if (Integer.parseInt(marketDeckSize) == 0) {
                                                        finishAndCount();


                                                    }
                                                }


                                                if ((cardp2.size() * (50 * density)) < width) {
                                                    more.setVisibility(View.INVISIBLE);
                                                }
                                                cardNum.setText("[" + marketDeckSize + "]");
                                                commandChange = "none";

                                                turner.setVisibility(View.INVISIBLE);
                                                timer.cancel();

                                                Toast.makeText(FinishWhotActivity.this, "Okay.", Toast.LENGTH_SHORT).show();


                                                if (cardp2.size() == 0) {
                                                    if (checkWinner(cardp2.size(), "p2")) {
                                                        gameSound("stop");
                                                        soundOn = false;
                                                    }
                                                }

                                            }

                                        }
                                    }

                                    if (success != null) {
                                        String jsonResponse = success.getNota();
                                        marketDeckSize = success.getNote();
                                        String jsonRes = success.getNoti();
                                        String jsonReso = success.getNoto();
                                        String jsonResu = success.getNotu();
                                        String jsonResy = success.getNoty();
                                        String jsonUpdater = success.getUpdated_at();


                                        try {
                                            if (!"".equals(jsonResponse) && jsonRes.equals("marketer") && Integer.parseInt(jsonResponse) == 0) {
                                                if (Integer.parseInt(marketDeckSize) == 0) {
                                                    finishAndCount();
                                                }
                                            }
                                        } catch (NumberFormatException numberError) {
                                            NumberFormatException nE = numberError;
                                        }


                                        if (!"".equals(jsonResponse) && jsonRes.equals("marketer")) {

                                            if (turnCounter.equals("p1")) {

                                                if (Integer.parseInt(marketDeckSize) == 0) {
                                                    finishAndCount();

                                                }
                                                String cd1 = "jz" + cardp1.size();
                                                cardp1.add(cd1);
                                                cardName = cd1;
                                                String[] cdx1 = cardName.split("z");
                                                cardNamePart = cdx1[0];
                                                try {
                                                    cardNameNum = Integer.parseInt(cdx1[1]);
                                                } catch (NumberFormatException e) {
                                                    cardNameNum = Integer.valueOf(cdx1[1]);
                                                }

                                                creatorx(R.drawable.backofcard, cardName);
                                                turnCounter = "p2";
                                                // turner.setVisibility(View.VISIBLE);
                                                // timer.start();
                                                cardNum.setText("[" + marketDeckSize + "]");

                                                final int delayO = 5000;

                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callr = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                callr.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                                Toast.makeText(FinishWhotActivity.this, "Responser!:marketer " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                calltime.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                String jres = response.body().getSuccessor();
                                                                                int jnum = Integer.parseInt(jres);
                                                                                int jnumsecs = jnum % 60;
                                                                                int jnummins = jnum / 60;
                                                                                timeZ.setTextColor(Color.RED);
                                                                                if (jnummins < 0 || jnumsecs < 0) {
                                                                                    timeZ.setText("Delay:  0  mins 0 secs");
                                                                                } else {
                                                                                    timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                                                }
                                                                                //here you can have your logic to set text to edittext
                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });

                                                                handler.removeCallbacks(runnableo);

                                                            } else {
                                                                handler.removeCallbacks(runnableo);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                                            }

                                                        } else {


                                                            runnableo = new Runnable() {
                                                                public void run() {
                                                                    //do something

                                                                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                    Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                    callz.enqueue(new Callback<User>() {
                                                                        @Override
                                                                        public void onResponse(Call<User> call, Response<User> response) {

                                                                            if (response.isSuccessful()) {
                                                                                if (response.body() != null) {
                                                                                    Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                    handler.removeCallbacks(runnableo);
                                                                                } else {
                                                                                    handler.removeCallbacks(runnableo);
                                                                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                                }

                                                                            } else {
                                                                                handler.postDelayed(runnableo, delayO);
                                                                                Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onFailure(Call<User> call, Throwable t) {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                        }
                                                                    });


                                                                }
                                                            };

                                                            handler.postDelayed(runnableo, delayO);


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        runnableo = new Runnable() {
                                                            public void run() {
                                                                //do something

                                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                callz.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                handler.removeCallbacks(runnableo);
                                                                            } else {
                                                                                handler.removeCallbacks(runnableo);
                                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                            }

                                                                        } else {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        handler.postDelayed(runnableo, delayO);
                                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                    }
                                                                });


                                                            }
                                                        };

                                                        handler.postDelayed(runnableo, delayO);
                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                });

                                                session.setMoveState(jsonUpdater);

                                                session.setNotaRec(null);
                                                session.setNoteRec(null);
                                                session.setNotiRec(null);
                                                session.setNotoRec(null);
                                                session.setNotuRec(null);
                                                session.setNotyRec(null);
                                                session.setWinaRec(null);
                                                session.setLosaRec(null);
                                            }
                                        } else if (jsonResponse.equals("t") || jsonResponse.equals("st") || jsonResponse.equals("c") || jsonResponse.equals("k") || jsonResponse.equals("sq")) {

                                            if (turnCounter.equals("p1")) {

                                                commandChange = jsonResponse;
                                                //   timer.cancel();

                                                switch (jsonResponse) {
                                                    case "t":
                                                        cardxi.setImageResource(R.drawable.t);
                                                        break;
                                                    case "st":
                                                        cardxi.setImageResource(R.drawable.st);
                                                        break;
                                                    case "c":
                                                        cardxi.setImageResource(R.drawable.c);
                                                        break;
                                                    case "sq":
                                                        cardxi.setImageResource(R.drawable.sq);
                                                        break;
                                                    case "k":
                                                        cardxi.setImageResource(R.drawable.k);
                                                        break;
                                                    default:
                                                        return;

                                                }

                                                turnCounter = "p2";
                                                // turner.setVisibility(View.VISIBLE);
                                                //  timer.start();

                                                final int delayO = 5000;

                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callr = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                callr.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                                Toast.makeText(FinishWhotActivity.this, "Responser!:needer " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                calltime.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                String jres = response.body().getSuccessor();
                                                                                int jnum = Integer.parseInt(jres);
                                                                                int jnumsecs = jnum % 60;
                                                                                int jnummins = jnum / 60;
                                                                                timeZ.setTextColor(Color.RED);
                                                                                if (jnummins < 0 || jnumsecs < 0) {
                                                                                    timeZ.setText("Delay:  0  mins 0 secs");
                                                                                } else {
                                                                                    timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                                                }
                                                                                //here you can have your logic to set text to edittext
                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });

                                                                handler.removeCallbacks(runnableo);

                                                            } else {
                                                                handler.removeCallbacks(runnableo);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                                            }

                                                        } else {


                                                            runnableo = new Runnable() {
                                                                public void run() {
                                                                    //do something

                                                                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                    Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                    callz.enqueue(new Callback<User>() {
                                                                        @Override
                                                                        public void onResponse(Call<User> call, Response<User> response) {

                                                                            if (response.isSuccessful()) {
                                                                                if (response.body() != null) {
                                                                                    Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                    handler.removeCallbacks(runnableo);
                                                                                } else {
                                                                                    handler.removeCallbacks(runnableo);
                                                                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                                }

                                                                            } else {
                                                                                handler.postDelayed(runnableo, delayO);
                                                                                Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onFailure(Call<User> call, Throwable t) {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                        }
                                                                    });


                                                                }
                                                            };

                                                            handler.postDelayed(runnableo, delayO);


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        runnableo = new Runnable() {
                                                            public void run() {
                                                                //do something

                                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                callz.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                handler.removeCallbacks(runnableo);
                                                                            } else {
                                                                                handler.removeCallbacks(runnableo);
                                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                            }

                                                                        } else {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        handler.postDelayed(runnableo, delayO);
                                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                    }
                                                                });


                                                            }
                                                        };

                                                        handler.postDelayed(runnableo, delayO);
                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                });

                                                session.setMoveState(jsonUpdater);

                                                session.setNotaRec(null);
                                                session.setNoteRec(null);
                                                session.setNotiRec(null);
                                                session.setNotoRec(null);
                                                session.setNotuRec(null);
                                                session.setNotyRec(null);
                                                session.setWinaRec(null);
                                                session.setLosaRec(null);
                                            }

                                        } else {

                                            playOne(success.getNota(), success.getNote(), success, jsonUpdater);

                                            if (!success.getUuid().equals(session.getUserID())) {

                                                final int delayO = 5000;

                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callr = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                callr.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {

                                                                Toast.makeText(FinishWhotActivity.this, "Responser!:playOne " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                calltime.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                String jres = response.body().getSuccessor();
                                                                                int jnum = Integer.parseInt(jres);
                                                                                int jnumsecs = jnum % 60;
                                                                                int jnummins = jnum / 60;
                                                                                timeZ.setTextColor(Color.RED);
                                                                                if (jnummins < 0 || jnumsecs < 0) {
                                                                                    timeZ.setText("Delay:  0  mins 0 secs");
                                                                                } else {
                                                                                    timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                                                }
                                                                                //here you can have your logic to set text to edittext
                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });

                                                                handler.removeCallbacks(runnableo);

                                                            } else {
                                                                handler.removeCallbacks(runnableo);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                                            }

                                                        } else {


                                                            runnableo = new Runnable() {
                                                                public void run() {
                                                                    //do something

                                                                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                    Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                    callz.enqueue(new Callback<User>() {
                                                                        @Override
                                                                        public void onResponse(Call<User> call, Response<User> response) {

                                                                            if (response.isSuccessful()) {
                                                                                if (response.body() != null) {
                                                                                    Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                    handler.removeCallbacks(runnableo);
                                                                                } else {
                                                                                    handler.removeCallbacks(runnableo);
                                                                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                                }

                                                                            } else {
                                                                                handler.postDelayed(runnableo, delayO);
                                                                                Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onFailure(Call<User> call, Throwable t) {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                        }
                                                                    });


                                                                }
                                                            };

                                                            handler.postDelayed(runnableo, delayO);


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        runnableo = new Runnable() {
                                                            public void run() {
                                                                //do something

                                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                callz.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                handler.removeCallbacks(runnableo);
                                                                            } else {
                                                                                handler.removeCallbacks(runnableo);
                                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                            }

                                                                        } else {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        handler.postDelayed(runnableo, delayO);
                                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                    }
                                                                });


                                                            }
                                                        };

                                                        handler.postDelayed(runnableo, delayO);
                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                });
                                            }

                                            session.setNotaRec(null);
                                            session.setNoteRec(null);
                                            session.setNotiRec(null);
                                            session.setNotoRec(null);
                                            session.setNotuRec(null);
                                            session.setNotyRec(null);
                                            session.setWinaRec(null);
                                            session.setLosaRec(null);

                                        }


                                        if (success.getWinner() != null) {
                                            String jsonWinner = success.getWinner();

                                            if (player.equals(jsonWinner)) {
                                                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                                                dialog.setCancelable(false);
                                                dialog.setTitle("You Won!");
                                                dialog.setMessage("Congratulations!");
                                                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        showpDialog();

                                                        Intent intentR = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                        intentR.putExtra("stopper", "false");
                                                        intentR.putExtra("usernome", session.getUserID());
                                                        if (session.getUserID().equals(player1x)) {
                                                            intentR.putExtra("userchaller", player2x);
                                                        } else {
                                                            intentR.putExtra("userchaller", player1x);
                                                        }
                                                        intentR.putExtra("betGame", "Finish&Count");
                                                        intentR.putExtra("bet_id", bet_id);
                                                        intentR.putExtra("game_id", Integer.toString(gameId));

                                                        Log.d("BET_ID", "Msg: " + bet_id);

                                                        startActivity(intentR);
                                                        hidepDialog();
                                                        FinishWhotActivity.this.finish();
                                                    }
                                                });
                                                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        exitButton.setVisibility(View.VISIBLE);
                                                        exitButton.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                call.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {

                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {

                                                                    }
                                                                });
                                                                session.setGameState(null);
                                                                betgamedb.deleter("game_state");

                                                                session.setChallengeState(null);

                                                                session.setNotaRec(null);
                                                                session.setNoteRec(null);
                                                                session.setNotiRec(null);
                                                                session.setNotoRec(null);
                                                                session.setNotuRec(null);
                                                                session.setNotyRec(null);
                                                                session.setWinaRec(null);
                                                                session.setLosaRec(null);
                                                                session.setPlay("false");
                                                                session.setReqNota(null);
                                                                session.setUserBeta(null);
                                                                session.setUserAcc(null);
                                                                session.setBetId(null);

                                                                Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                                intentOne.putExtra("stopper", "false");
                                                                startActivity(intentOne);
                                                                FinishWhotActivity.this.finish();
                                                            }
                                                        });
                                                    }
                                                });

                                                final AlertDialog alert = dialog.create();
                                                gameOver = true;
                                                Handler handler = new Handler();
                                                int delay = 3000; //milliseconds

                                                handler.postDelayed(new Runnable() {
                                                    public void run() {
                                                        //do something
                                                        if (Integer.parseInt(marketDeckSize) == 0) {
                                                            dialoger = true;
                                                        } else {

                                                        }
                                                        if (!dialoger) {
                                                            if (!isFinishing()) {

                                                                alert.show();
                                                                playWinSound();
                                                            } else {

                                                            }


                                                        }

                                                    }
                                                }, delay);
                                                session.setGameState(null);
                                                betgamedb.deleter("game_state");
                                                session.setEndState(gameId);
                                                session.setChallengeState(null);
                                                session.setAppState(null);
                                                //   timer.cancel();
                                                gameSound("stop");
                                                soundOn = false;


                                                //displayer("Game Over","Sorry, You lost!");
                                                //handlerx.removeCallbacks(this);
                                                freezeGame = true;
                                                timer.cancel();
                                                session.setNotaRec(null);
                                                session.setNoteRec(null);
                                                session.setNotiRec(null);
                                                session.setNotoRec(null);
                                                session.setNotuRec(null);
                                                session.setNotyRec(null);
                                                session.setWinaRec(null);
                                                session.setLosaRec(null);
                                                session.setBetId(null);
                                            } else {


                                                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                                                dialog.setCancelable(false);
                                                dialog.setTitle("You Lost!");
                                                dialog.setMessage("Sorry, you're not lucky this time.");
                                                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        showpDialog();


                                                        Intent intentR = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                        intentR.putExtra("stopper", "false");
                                                        intentR.putExtra("usernome", session.getUserID());
                                                        if (session.getUserID().equals(player1x)) {
                                                            intentR.putExtra("userchaller", player2x);
                                                        } else {
                                                            intentR.putExtra("userchaller", player1x);
                                                        }
                                                        intentR.putExtra("betGame", "Finish&Count");
                                                        intentR.putExtra("bet_id", bet_id);
                                                        intentR.putExtra("game_id", Integer.toString(gameId));

                                                        Log.d("BET_ID", "Msg: " + bet_id);

                                                        startActivity(intentR);
                                                        hidepDialog();
                                                        FinishWhotActivity.this.finish();
                                                    }
                                                });
                                                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        exitButton.setVisibility(View.VISIBLE);
                                                        exitButton.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                call.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {

                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {

                                                                    }
                                                                });
                                                                session.setGameState(null);
                                                                betgamedb.deleter("game_state");

                                                                session.setChallengeState(null);

                                                                session.setNotaRec(null);
                                                                session.setNoteRec(null);
                                                                session.setNotiRec(null);
                                                                session.setNotoRec(null);
                                                                session.setNotuRec(null);
                                                                session.setNotyRec(null);
                                                                session.setWinaRec(null);
                                                                session.setLosaRec(null);
                                                                session.setPlay("false");
                                                                session.setReqNota(null);
                                                                session.setUserBeta(null);
                                                                session.setUserAcc(null);
                                                                session.setBetId(null);

                                                                Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                                intentOne.putExtra("stopper", "false");
                                                                startActivity(intentOne);
                                                                FinishWhotActivity.this.finish();
                                                            }
                                                        });
                                                    }
                                                });

                                                final AlertDialog alert = dialog.create();
                                                gameOver = true;
                                                Handler handler = new Handler();
                                                int delay = 3000; //milliseconds

                                                handler.postDelayed(new Runnable() {
                                                    public void run() {
                                                        //do something
                                                        if (backpressed) {

                                                        } else {
                                                            if (Integer.parseInt(marketDeckSize) == 0) {
                                                                dialoger = true;
                                                            } else {

                                                            }
                                                            if (!dialoger) {
                                                                if (!isFinishing()) {

                                                                    alert.show();
                                                                    playLoseSound();
                                                                } else {

                                                                }

                                                            }
                                                        }

                                                    }
                                                }, delay);
                                                session.setGameState(null);
                                                betgamedb.deleter("game_state");
                                                session.setEndState(gameId);
                                                session.setChallengeState(null);
                                                session.setAppState(null);
                                                //   timer.cancel();
                                                gameSound("stop");
                                                soundOn = false;


                                                //displayer("Game Over","Sorry, You lost!");
                                                //handlerx.removeCallbacks(this);
                                                freezeGame = true;
                                                timer.cancel();
                                                session.setNotaRec(null);
                                                session.setNoteRec(null);
                                                session.setNotiRec(null);
                                                session.setNotoRec(null);
                                                session.setNotuRec(null);
                                                session.setNotyRec(null);
                                                session.setWinaRec(null);
                                                session.setLosaRec(null);
                                                session.setBetId(null);
                                            }

                                        }
                                    }
                                }
                            }


                        } else {

                            if (success != null && success.getContinua() != null && success.getContinua().equals("true")) {
                                if (success.getNoti().equals("marketer")) {

                                } else if (success.getNoti().equals("whotter")) {

                                } else if (!success.getNoti().equals("") && !success.getNoti().equals("whotter") && !success.getNoti().equals("marketer")) {

                                } else {

                                }
                                freezeMark = false;
                                freezeWhot = false;
                                freezeCard = false;

                                Toast.makeText(FinishWhotActivity.this, "Continue", Toast.LENGTH_SHORT).show();

                                turner.setVisibility(View.VISIBLE);
                                timer.start();
                            } else {
                                //   Toast.makeText(FinishWhotActivity.this, "some extra toasting", Toast.LENGTH_SHORT).show();


                                if (success != null && "".equals(success.getNote()) || success != null && "0".equals(success.getNote())) {
                                    if (Integer.parseInt(marketDeckSize) == 0) {
                                        finishAndCount();
                                    }
                                }


                                if (success != null && session.getMoveState() != null && success.getUuid().equals(session.getMoveState())) {

                                }

                                if (success != null && success.getUuid().equals(session.getUserID())) {
                                    if (success.getNoti().equals("marketer")) {
                                        playerTwoMarketReceive = true;
                                        ImageView slotter = (ImageView) slotView2.getChildAt(slotView2.getChildCount() - 1);
                                        String jsonResponse = success.getNota();
                                        // Toast.makeText(FinishWhotActivity.this, jsonResponse, Toast.LENGTH_SHORT).show();
                                        try {
                                            if (!jsonResponse.equals("") || Integer.parseInt(jsonResponse) == 0) {
                                                marketDeckSize = success.getNote();
                                                cd2 = success.getNota();
                                                if (Integer.parseInt(marketDeckSize) == 0) {
                                                    finishAndCount();

                                                }

                                                cardp2.add(cd2);
                                                cardName = cd2;
                                                String[] cdx2 = cardName.split("z");
                                                cardNamePart = cdx2[0];
                                                try {
                                                    cardNameNum = Integer.parseInt(cdx2[1]);
                                                } catch (NumberFormatException e) {
                                                    cardNameNum = Integer.valueOf(cdx2[1]);
                                                }

                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                //freezeMark = false;

                                                int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardName, null, null);
                                                creator(srcImage, cardName);
                                                //  timer.cancel();
                                                //timer.start();
                                                turnCounter = "p1";
                                                turner.setVisibility(View.INVISIBLE);
                                                timer.cancel();
                                                cardNum.setText("[" + marketDeckSize + "]");

                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                calltime.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                                String jres = response.body().getSuccessor();
                                                                int jnum = Integer.parseInt(jres);
                                                                int jnumsecs = jnum % 60;
                                                                int jnummins = jnum / 60;
                                                                timeZ.setTextColor(Color.RED);
                                                                if (jnummins < 0 || jnumsecs < 0) {
                                                                    timeZ.setText("Delay:  0  mins 0 secs");
                                                                } else {
                                                                    timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                                }
                                                                //here you can have your logic to set text to edittext
                                                            } else {

                                                            }

                                                        } else {


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                                    }
                                                });

                                            }

                                        } catch (NumberFormatException e) {
                                            NumberFormatException x = e;
                                        }

                                    } else if (success.getNoti().equals("whotter")) {

                                        // Get the widgets reference from XML layout
                                        final ConstraintLayout cl3 = findViewById(R.id.clx);
                                        final ConstraintLayout cl1 = findViewById(R.id.cl1);
                                        final ConstraintLayout cl2 = findViewById(R.id.cl2);

                                        cl3.setVisibility(View.VISIBLE);
                                        cl3.bringToFront();

                                        cl1.setForeground(getDrawable(R.drawable.backor));
                                        cl2.setForeground(getDrawable(R.drawable.backor));

                                        String jsonResponse = success.getNota();

                                        commandChange = jsonResponse;
                                        // timer.cancel();
                                        // timer.start();

                                        int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + jsonResponse, null, null);
                                        cardxi.setImageResource(srcImage);
                                        cl3.setVisibility(View.INVISIBLE);
                                        cl1.setForeground(null);
                                        cl2.setForeground(null);
                                        playerTwoWhotReceive = true;
                                        loader.setVisibility(View.INVISIBLE);
                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                        // freezeWhot = false;
                                        freezeGame = false;
                                        needWhot = false;
                                        turnCounter = "p1";
                                        //timeX.setTextColor(Color.BLACK);
                                        turner.setVisibility(View.INVISIBLE);
                                        timer.cancel();

                                        RestService servicer = RestClient.getClient(session).create(RestService.class);
                                        Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                        calltime.enqueue(new Callback<User>() {
                                            @Override
                                            public void onResponse(Call<User> call, Response<User> response) {

                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        String jres = response.body().getSuccessor();
                                                        int jnum = Integer.parseInt(jres);
                                                        int jnumsecs = jnum % 60;
                                                        int jnummins = jnum / 60;
                                                        timeZ.setTextColor(Color.RED);
                                                        if (jnummins < 0 || jnumsecs < 0) {
                                                            timeZ.setText("Delay:  0  mins 0 secs");
                                                        } else {
                                                            timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                        }
                                                        //here you can have your logic to set text to edittext
                                                    } else {

                                                    }

                                                } else {


                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<User> call, Throwable t) {
                                                // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                            }
                                        });


                                    } else if (!success.getNoti().equals("") && !success.getNoti().equals("whotter") && !success.getNoti().equals("marketer")) {

                                        playerTwoReceive = true;
                                        String[] checkWordArr = success.getNota().split("z");
                                        if (checkWordArr[0].equals("w") || checkWordArr[1].equals("1") || checkWordArr[1].equals("2") || checkWordArr[1].equals("14")) {

                                        } else {
                                            freezeCard = false;
                                        }
                                        ImageView slotti = (ImageView) slotView1.getChildAt(slotView1.getChildCount() - 1);
                                        String cart = success.getNota();

                                        playedLast = "p2";
                                        timerx.cancel();
                                        int delayO = 5000;

                                        RestService servicer = RestClient.getClient(session).create(RestService.class);
                                        Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                        calltime.enqueue(new Callback<User>() {
                                            @Override
                                            public void onResponse(Call<User> call, Response<User> response) {

                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        String jres = response.body().getSuccessor();
                                                        int jnum = Integer.parseInt(jres);
                                                        int jnumsecs = jnum % 60;
                                                        int jnummins = jnum / 60;
                                                        timeZ.setTextColor(Color.RED);
                                                        if (jnummins < 0 || jnumsecs < 0) {
                                                            timeZ.setText("Delay:  0  mins 0 secs");
                                                        } else {
                                                            timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                        }
                                                        //here you can have your logic to set text to edittext
                                                    } else {

                                                    }

                                                } else {


                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<User> call, Throwable t) {
                                                // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                        String jsonResponse = success.getGame_type();
                                        String resPopData = success.getNoto();
                                        String resPopData1 = success.getNotu();
                                        String resPopData2 = success.getNoty();

                                        String[] carter = cart.split("z");
                                        int cardNameNum = Integer.parseInt(carter[1]);

                                        if (freezeGame) return;

                                        if ("Finish&Count".equals(jsonResponse) || "Classic".equals(jsonResponse)) {
                                            marketDeckSize = success.getNote();

                                            if (cardNameNum == 2) {
                                                setPickTwo(resPopData1, resPopData2, "p2", "p1", slotti);

                                            } else if (cardNameNum == 14) {
                                                setGeneralMarket(resPopData, "p2", "p1", slotti);

                                            } else {
                                                if (Integer.parseInt(marketDeckSize) == 0) {
                                                    finishAndCount();

                                                }
                                            }


                                            if ((cardp2.size() * (50 * density)) < width) {
                                                more.setVisibility(View.INVISIBLE);
                                            }
                                            cardNum.setText("[" + marketDeckSize + "]");
                                            commandChange = "none";

                                            timer.cancel();

                                            Toast.makeText(FinishWhotActivity.this, "Okay.", Toast.LENGTH_SHORT).show();

                                            final int delay_check = 5000;
                                            final Handler handle = new Handler();
                                            final Handler handly = new Handler();

                                            runner = new Runnable() {

                                                @Override
                                                public void run() {

                                                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                    Call<User> callr = servicePlay.userDelay("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                    callr.enqueue(new Callback<User>() {
                                                        @Override
                                                        public void onResponse(Call<User> call, Response<User> response) {

                                                            if (response.isSuccessful()) {
                                                                if (response.body() != null) {
                                                                    if (response.body().getSuccessor().equals("failure")) {
                                                                        handly.removeCallbacks(runnablex);
                                                                        handle.removeCallbacks(runner);
                                                                    }

                                                                    handle.postDelayed(runner, delay_check);

                                                                } else {
                                                                    handle.postDelayed(runner, delay_check);

                                                                }

                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<User> call, Throwable t) {

                                                            handle.postDelayed(runner, delay_check);

                                                        }
                                                    });

                                                }

                                            };

                                            runnablx = new Runnable() {
                                                public void run() {
                                                    //do something
                                                    handle.postDelayed(runner, delay_check);

                                                }
                                            };

                                            handly.postDelayed(runnablx, 35000);


                                            if (cardp2.size() == 0) {
                                                if (checkWinner(cardp2.size(), "p2")) {
                                                    gameSound("stop");
                                                    soundOn = false;
                                                }
                                            }

                                        }

                                    }
                                }
                            }

                        }

                    } else if (cardp1.size() > 0) {
                        if (turnCounter.equals("p1")) {
                            if (success != null && "".equals(success.getNote()) || success != null && "0".equals(success.getNote())) {

                                if (Integer.parseInt(marketDeckSize) == 0) {
                                    finishAndCount();
                                }
                            }

                            if (success != null && "".equals(success.getNota()) || success != null && "0".equals(success.getNota())) {


                            } else {

                                if (success != null && success.getContinua() != null && success.getContinua().equals("true")) {

                                    //   Toast.makeText(FinishWhotActivity.this, "second toast", Toast.LENGTH_SHORT).show();

                                } else {

                                    if (success != null && success.getUuid().equals(session.getUserID())) {
                                        if (!success.getNoti().equals("") && !success.getNoti().equals("whotter") && !success.getNoti().equals("marketer")) {

                                            playerTwoReceive = true;
                                            String[] checkWordArr = success.getNota().split("z");
                                            if (checkWordArr[0].equals("w") || checkWordArr[1].equals("1") || checkWordArr[1].equals("2") || checkWordArr[1].equals("14")) {

                                            } else {
                                                freezeCard = false;
                                            }
                                            ImageView slotti = (ImageView) slotView1.getChildAt(slotView1.getChildCount() - 1);
                                            String cart = success.getNota();

                                            playedLast = "p2";
                                            timerx.cancel();
                                            int delayO = 5000;

                                            RestService servicer = RestClient.getClient(session).create(RestService.class);
                                            Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                            calltime.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {
                                                            String jres = response.body().getSuccessor();
                                                            int jnum = Integer.parseInt(jres);
                                                            int jnumsecs = jnum % 60;
                                                            int jnummins = jnum / 60;
                                                            timeZ.setTextColor(Color.RED);
                                                            if (jnummins < 0 || jnumsecs < 0) {
                                                                timeZ.setText("Delay:  0  mins 0 secs");
                                                            } else {
                                                                timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                            }
                                                            //here you can have your logic to set text to edittext
                                                        } else {

                                                        }

                                                    } else {


                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {
                                                    // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                                }
                                            });

                                            String jsonResponse = success.getGame_type();
                                            String resPopData = success.getNoto();
                                            String resPopData1 = success.getNotu();
                                            String resPopData2 = success.getNoty();

                                            String[] carter = cart.split("z");
                                            int cardNameNum = Integer.parseInt(carter[1]);

                                            if (freezeGame) return;

                                            if ("Finish&Count".equals(jsonResponse) || "Classic".equals(jsonResponse)) {
                                                marketDeckSize = success.getNote();

                                                if (cardNameNum == 2) {
                                                    setPickTwo(resPopData1, resPopData2, "p2", "p1", slotti);

                                                } else if (cardNameNum == 14) {
                                                    setGeneralMarket(resPopData, "p2", "p1", slotti);

                                                } else {
                                                    if (Integer.parseInt(marketDeckSize) == 0) {
                                                        finishAndCount();


                                                    }
                                                }


                                                if ((cardp2.size() * (50 * density)) < width) {
                                                    more.setVisibility(View.INVISIBLE);
                                                }
                                                cardNum.setText("[" + marketDeckSize + "]");
                                                commandChange = "none";

                                                turner.setVisibility(View.INVISIBLE);
                                                timer.cancel();

                                                Toast.makeText(FinishWhotActivity.this, "Okay.", Toast.LENGTH_SHORT).show();


                                                if (cardp2.size() == 0) {
                                                    if (checkWinner(cardp2.size(), "p2")) {
                                                        gameSound("stop");
                                                        soundOn = false;
                                                    }
                                                }

                                            }

                                        }
                                    }

                                    if (success != null) {
                                        String jsonResponse = success.getNota();
                                        marketDeckSize = success.getNote();
                                        String jsonRes = success.getNoti();
                                        String jsonReso = success.getNoto();
                                        String jsonResu = success.getNotu();
                                        String jsonResy = success.getNoty();
                                        String jsonUpdater = success.getUpdated_at();


                                        try {
                                            if (!"".equals(jsonResponse) && jsonRes.equals("marketer") && Integer.parseInt(jsonResponse) == 0) {
                                                if (Integer.parseInt(marketDeckSize) == 0) {
                                                    finishAndCount();
                                                }
                                            }
                                        } catch (NumberFormatException numberError) {
                                            NumberFormatException nE = numberError;
                                        }


                                        if (!"".equals(jsonResponse) && jsonRes.equals("marketer")) {

                                            if (turnCounter.equals("p1")) {

                                                if (Integer.parseInt(marketDeckSize) == 0) {
                                                    finishAndCount();

                                                }
                                                String cd1 = "jz" + cardp1.size();
                                                cardp1.add(cd1);
                                                cardName = cd1;
                                                String[] cdx1 = cardName.split("z");
                                                cardNamePart = cdx1[0];
                                                try {
                                                    cardNameNum = Integer.parseInt(cdx1[1]);
                                                } catch (NumberFormatException e) {
                                                    cardNameNum = Integer.valueOf(cdx1[1]);
                                                }

                                                creatorx(R.drawable.backofcard, cardName);
                                                turnCounter = "p2";
                                                // turner.setVisibility(View.VISIBLE);
                                                // timer.start();
                                                cardNum.setText("[" + marketDeckSize + "]");

                                                final int delayO = 5000;

                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callr = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                callr.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                                Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                calltime.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                String jres = response.body().getSuccessor();
                                                                                int jnum = Integer.parseInt(jres);
                                                                                int jnumsecs = jnum % 60;
                                                                                int jnummins = jnum / 60;
                                                                                timeZ.setTextColor(Color.RED);
                                                                                if (jnummins < 0 || jnumsecs < 0) {
                                                                                    timeZ.setText("Delay:  0  mins 0 secs");
                                                                                } else {
                                                                                    timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                                                }
                                                                                //here you can have your logic to set text to edittext
                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });

                                                                handler.removeCallbacks(runnableo);

                                                            } else {
                                                                handler.removeCallbacks(runnableo);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                                            }

                                                        } else {


                                                            runnableo = new Runnable() {
                                                                public void run() {
                                                                    //do something

                                                                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                    Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                    callz.enqueue(new Callback<User>() {
                                                                        @Override
                                                                        public void onResponse(Call<User> call, Response<User> response) {

                                                                            if (response.isSuccessful()) {
                                                                                if (response.body() != null) {
                                                                                    Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                    handler.removeCallbacks(runnableo);
                                                                                } else {
                                                                                    handler.removeCallbacks(runnableo);
                                                                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                                }

                                                                            } else {
                                                                                handler.postDelayed(runnableo, delayO);
                                                                                Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onFailure(Call<User> call, Throwable t) {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                        }
                                                                    });


                                                                }
                                                            };

                                                            handler.postDelayed(runnableo, delayO);


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        runnableo = new Runnable() {
                                                            public void run() {
                                                                //do something

                                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                callz.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                handler.removeCallbacks(runnableo);
                                                                            } else {
                                                                                handler.removeCallbacks(runnableo);
                                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                            }

                                                                        } else {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        handler.postDelayed(runnableo, delayO);
                                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                    }
                                                                });


                                                            }
                                                        };

                                                        handler.postDelayed(runnableo, delayO);
                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                });

                                                session.setMoveState(jsonUpdater);

                                                session.setNotaRec(null);
                                                session.setNoteRec(null);
                                                session.setNotiRec(null);
                                                session.setNotoRec(null);
                                                session.setNotuRec(null);
                                                session.setNotyRec(null);
                                                session.setWinaRec(null);
                                                session.setLosaRec(null);
                                            }
                                        } else if (jsonResponse.equals("t") || jsonResponse.equals("st") || jsonResponse.equals("c") || jsonResponse.equals("k") || jsonResponse.equals("sq")) {

                                            if (turnCounter.equals("p1")) {

                                                commandChange = jsonResponse;
                                                //   timer.cancel();

                                                switch (jsonResponse) {
                                                    case "t":
                                                        cardxi.setImageResource(R.drawable.t);
                                                        break;
                                                    case "st":
                                                        cardxi.setImageResource(R.drawable.st);
                                                        break;
                                                    case "c":
                                                        cardxi.setImageResource(R.drawable.c);
                                                        break;
                                                    case "sq":
                                                        cardxi.setImageResource(R.drawable.sq);
                                                        break;
                                                    case "k":
                                                        cardxi.setImageResource(R.drawable.k);
                                                        break;
                                                    default:
                                                        return;

                                                }

                                                turnCounter = "p2";
                                                // turner.setVisibility(View.VISIBLE);
                                                //  timer.start();

                                                final int delayO = 5000;

                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callr = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                callr.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                                Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                calltime.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                String jres = response.body().getSuccessor();
                                                                                int jnum = Integer.parseInt(jres);
                                                                                int jnumsecs = jnum % 60;
                                                                                int jnummins = jnum / 60;
                                                                                timeZ.setTextColor(Color.RED);
                                                                                if (jnummins < 0 || jnumsecs < 0) {
                                                                                    timeZ.setText("Delay:  0  mins 0 secs");
                                                                                } else {
                                                                                    timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                                                }
                                                                                //here you can have your logic to set text to edittext
                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });

                                                                handler.removeCallbacks(runnableo);

                                                            } else {
                                                                handler.removeCallbacks(runnableo);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                                            }

                                                        } else {


                                                            runnableo = new Runnable() {
                                                                public void run() {
                                                                    //do something

                                                                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                    Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                    callz.enqueue(new Callback<User>() {
                                                                        @Override
                                                                        public void onResponse(Call<User> call, Response<User> response) {

                                                                            if (response.isSuccessful()) {
                                                                                if (response.body() != null) {
                                                                                    Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                    handler.removeCallbacks(runnableo);
                                                                                } else {
                                                                                    handler.removeCallbacks(runnableo);
                                                                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                                }

                                                                            } else {
                                                                                handler.postDelayed(runnableo, delayO);
                                                                                Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onFailure(Call<User> call, Throwable t) {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                        }
                                                                    });


                                                                }
                                                            };

                                                            handler.postDelayed(runnableo, delayO);


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        runnableo = new Runnable() {
                                                            public void run() {
                                                                //do something

                                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                callz.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                handler.removeCallbacks(runnableo);
                                                                            } else {
                                                                                handler.removeCallbacks(runnableo);
                                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                            }

                                                                        } else {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        handler.postDelayed(runnableo, delayO);
                                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                    }
                                                                });


                                                            }
                                                        };

                                                        handler.postDelayed(runnableo, delayO);
                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                });

                                                session.setMoveState(jsonUpdater);

                                                session.setNotaRec(null);
                                                session.setNoteRec(null);
                                                session.setNotiRec(null);
                                                session.setNotoRec(null);
                                                session.setNotuRec(null);
                                                session.setNotyRec(null);
                                                session.setWinaRec(null);
                                                session.setLosaRec(null);
                                            }
                                        } else {
                                            playOne(success.getNota(), success.getNote(), success, jsonUpdater);

                                            if (!success.getUuid().equals(session.getUserID())) {
                                                final int delayO = 5000;

                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callr = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                callr.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                                Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                calltime.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                String jres = response.body().getSuccessor();
                                                                                int jnum = Integer.parseInt(jres);
                                                                                int jnumsecs = jnum % 60;
                                                                                int jnummins = jnum / 60;
                                                                                timeZ.setTextColor(Color.RED);
                                                                                if (jnummins < 0 || jnumsecs < 0) {
                                                                                    timeZ.setText("Delay:  0  mins 0 secs");
                                                                                } else {
                                                                                    timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                                                }
                                                                                //here you can have your logic to set text to edittext
                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });

                                                                handler.removeCallbacks(runnableo);

                                                            } else {
                                                                handler.removeCallbacks(runnableo);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                                            }

                                                        } else {


                                                            runnableo = new Runnable() {
                                                                public void run() {
                                                                    //do something

                                                                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                    Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                    callz.enqueue(new Callback<User>() {
                                                                        @Override
                                                                        public void onResponse(Call<User> call, Response<User> response) {

                                                                            if (response.isSuccessful()) {
                                                                                if (response.body() != null) {
                                                                                    Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                    handler.removeCallbacks(runnableo);
                                                                                } else {
                                                                                    handler.removeCallbacks(runnableo);
                                                                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                                }

                                                                            } else {
                                                                                handler.postDelayed(runnableo, delayO);
                                                                                Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onFailure(Call<User> call, Throwable t) {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                        }
                                                                    });


                                                                }
                                                            };

                                                            handler.postDelayed(runnableo, delayO);


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        runnableo = new Runnable() {
                                                            public void run() {
                                                                //do something

                                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> callz = servicePlay.userCont("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                callz.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {
                                                                                Toast.makeText(FinishWhotActivity.this, "Responser!: " + response.body().getSuccessor(), Toast.LENGTH_SHORT).show();

                                                                                handler.removeCallbacks(runnableo);
                                                                            } else {
                                                                                handler.removeCallbacks(runnableo);
                                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                            }

                                                                        } else {
                                                                            handler.postDelayed(runnableo, delayO);
                                                                            Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {
                                                                        handler.postDelayed(runnableo, delayO);
                                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();


                                                                    }
                                                                });


                                                            }
                                                        };

                                                        handler.postDelayed(runnableo, delayO);
                                                        Toast.makeText(FinishWhotActivity.this, "Failure Error!" + t.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                });
                                            }

                                            session.setNotaRec(null);
                                            session.setNoteRec(null);
                                            session.setNotiRec(null);
                                            session.setNotoRec(null);
                                            session.setNotuRec(null);
                                            session.setNotyRec(null);
                                            session.setWinaRec(null);
                                            session.setLosaRec(null);

                                        }


                                        if (success.getWinner() != null) {
                                            String jsonWinner = success.getWinner();

                                            if (player.equals(jsonWinner)) {
                                                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                                                dialog.setCancelable(false);
                                                dialog.setTitle("You Won!");
                                                dialog.setMessage("Congratulations!");
                                                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        showpDialog();


                                                        Intent intentR = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                        intentR.putExtra("stopper", "false");
                                                        intentR.putExtra("usernome", session.getUserID());
                                                        if (session.getUserID().equals(player1x)) {
                                                            intentR.putExtra("userchaller", player2x);
                                                        } else {
                                                            intentR.putExtra("userchaller", player1x);
                                                        }
                                                        intentR.putExtra("betGame", "Finish&Count");
                                                        intentR.putExtra("bet_id", bet_id);
                                                        intentR.putExtra("game_id", Integer.toString(gameId));

                                                        Log.d("BET_ID", "Msg: " + bet_id);

                                                        startActivity(intentR);
                                                        hidepDialog();
                                                        FinishWhotActivity.this.finish();
                                                    }
                                                });
                                                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        exitButton.setVisibility(View.VISIBLE);
                                                        exitButton.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                call.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {

                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {

                                                                    }
                                                                });
                                                                session.setGameState(null);
                                                                betgamedb.deleter("game_state");

                                                                session.setChallengeState(null);

                                                                session.setNotaRec(null);
                                                                session.setNoteRec(null);
                                                                session.setNotiRec(null);
                                                                session.setNotoRec(null);
                                                                session.setNotuRec(null);
                                                                session.setNotyRec(null);
                                                                session.setWinaRec(null);
                                                                session.setLosaRec(null);
                                                                session.setPlay("false");
                                                                session.setReqNota(null);
                                                                session.setUserBeta(null);
                                                                session.setUserAcc(null);
                                                                session.setBetId(null);

                                                                Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                                intentOne.putExtra("stopper", "false");
                                                                startActivity(intentOne);
                                                                FinishWhotActivity.this.finish();
                                                            }
                                                        });
                                                    }
                                                });

                                                final AlertDialog alert = dialog.create();
                                                gameOver = true;
                                                Handler handler = new Handler();
                                                int delay = 3000; //milliseconds

                                                handler.postDelayed(new Runnable() {
                                                    public void run() {
                                                        //do something
                                                        if (Integer.parseInt(marketDeckSize) == 0) {
                                                            dialoger = true;
                                                        } else {

                                                        }
                                                        if (!dialoger) {
                                                            if (!isFinishing()) {

                                                                alert.show();
                                                                playWinSound();
                                                            } else {

                                                            }


                                                        }

                                                    }
                                                }, delay);
                                                session.setGameState(null);
                                                betgamedb.deleter("game_state");
                                                session.setEndState(gameId);
                                                session.setChallengeState(null);
                                                session.setAppState(null);
                                                //   timer.cancel();
                                                gameSound("stop");
                                                soundOn = false;


                                                //displayer("Game Over","Sorry, You lost!");
                                                //handlerx.removeCallbacks(this);
                                                freezeGame = true;
                                                timer.cancel();
                                                session.setNotaRec(null);
                                                session.setNoteRec(null);
                                                session.setNotiRec(null);
                                                session.setNotoRec(null);
                                                session.setNotuRec(null);
                                                session.setNotyRec(null);
                                                session.setWinaRec(null);
                                                session.setLosaRec(null);
                                                session.setBetId(null);
                                            } else {


                                                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                                                dialog.setCancelable(false);
                                                dialog.setTitle("You Lost!");
                                                dialog.setMessage("Sorry, you're not lucky this time.");
                                                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        showpDialog();


                                                        Intent intentR = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                        intentR.putExtra("stopper", "false");
                                                        intentR.putExtra("usernome", session.getUserID());
                                                        if (session.getUserID().equals(player1x)) {
                                                            intentR.putExtra("userchaller", player2x);
                                                        } else {
                                                            intentR.putExtra("userchaller", player1x);
                                                        }
                                                        intentR.putExtra("betGame", "Finish&Count");
                                                        intentR.putExtra("bet_id", bet_id);
                                                        intentR.putExtra("game_id", Integer.toString(gameId));

                                                        Log.d("BET_ID", "Msg: " + bet_id);

                                                        startActivity(intentR);
                                                        hidepDialog();
                                                        FinishWhotActivity.this.finish();
                                                    }
                                                });
                                                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        exitButton.setVisibility(View.VISIBLE);
                                                        exitButton.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                call.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {

                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {

                                                                    }
                                                                });
                                                                session.setGameState(null);
                                                                betgamedb.deleter("game_state");

                                                                session.setChallengeState(null);

                                                                session.setNotaRec(null);
                                                                session.setNoteRec(null);
                                                                session.setNotiRec(null);
                                                                session.setNotoRec(null);
                                                                session.setNotuRec(null);
                                                                session.setNotyRec(null);
                                                                session.setWinaRec(null);
                                                                session.setLosaRec(null);
                                                                session.setPlay("false");
                                                                session.setReqNota(null);
                                                                session.setUserBeta(null);
                                                                session.setUserAcc(null);
                                                                session.setBetId(null);

                                                                Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                                intentOne.putExtra("stopper", "false");
                                                                startActivity(intentOne);
                                                                FinishWhotActivity.this.finish();
                                                            }
                                                        });
                                                    }
                                                });

                                                final AlertDialog alert = dialog.create();
                                                gameOver = true;
                                                Handler handler = new Handler();
                                                int delay = 3000; //milliseconds

                                                handler.postDelayed(new Runnable() {
                                                    public void run() {
                                                        //do something
                                                        if (backpressed) {

                                                        } else {
                                                            if (Integer.parseInt(marketDeckSize) == 0) {
                                                                dialoger = true;
                                                            } else {

                                                            }
                                                            if (!dialoger) {
                                                                if (!isFinishing()) {

                                                                    alert.show();
                                                                    playLoseSound();
                                                                } else {

                                                                }

                                                            }
                                                        }

                                                    }
                                                }, delay);
                                                session.setGameState(null);
                                                betgamedb.deleter("game_state");
                                                session.setEndState(gameId);
                                                session.setChallengeState(null);
                                                session.setAppState(null);
                                                //   timer.cancel();
                                                gameSound("stop");
                                                soundOn = false;


                                                //displayer("Game Over","Sorry, You lost!");
                                                //handlerx.removeCallbacks(this);
                                                freezeGame = true;
                                                timer.cancel();
                                                session.setNotaRec(null);
                                                session.setNoteRec(null);
                                                session.setNotiRec(null);
                                                session.setNotoRec(null);
                                                session.setNotuRec(null);
                                                session.setNotyRec(null);
                                                session.setWinaRec(null);
                                                session.setLosaRec(null);
                                                session.setBetId(null);
                                            }

                                        }
                                    }
                                }
                            }


                        } else {

                            if (success != null && success.getContinua() != null && success.getContinua().equals("true")) {
                                if (success.getNoti().equals("marketer")) {

                                } else if (success.getNoti().equals("whotter")) {

                                } else if (!success.getNoti().equals("") && !success.getNoti().equals("whotter") && !success.getNoti().equals("marketer")) {

                                } else {

                                }
                                freezeMark = false;
                                freezeWhot = false;
                                freezeCard = false;

                                Toast.makeText(FinishWhotActivity.this, "Continue", Toast.LENGTH_SHORT).show();

                                turner.setVisibility(View.VISIBLE);
                                timer.start();
                            } else {
                                //   Toast.makeText(FinishWhotActivity.this, "some extra toasting", Toast.LENGTH_SHORT).show();


                                if (success != null && "".equals(success.getNote()) || success != null && "0".equals(success.getNote())) {
                                    if (Integer.parseInt(marketDeckSize) == 0) {
                                        finishAndCount();
                                    }
                                }


                                if (success != null && session.getMoveState() != null && success.getUuid().equals(session.getMoveState())) {

                                }

                                if (success != null && success.getUuid().equals(session.getUserID())) {
                                    if (success.getNoti().equals("marketer") && success.getUuid().equals(session.getUserID())) {
                                        playerTwoMarketReceive = true;
                                        ImageView slotter = (ImageView) slotView2.getChildAt(slotView2.getChildCount() - 1);
                                        String jsonResponse = success.getNota();
                                        // Toast.makeText(FinishWhotActivity.this, jsonResponse, Toast.LENGTH_SHORT).show();
                                        try {
                                            if (!jsonResponse.equals("") || Integer.parseInt(jsonResponse) == 0) {
                                                marketDeckSize = success.getNote();
                                                cd2 = success.getNota();
                                                if (Integer.parseInt(marketDeckSize) == 0) {
                                                    finishAndCount();

                                                }

                                                cardp2.add(cd2);
                                                cardName = cd2;
                                                String[] cdx2 = cardName.split("z");
                                                cardNamePart = cdx2[0];
                                                try {
                                                    cardNameNum = Integer.parseInt(cdx2[1]);
                                                } catch (NumberFormatException e) {
                                                    cardNameNum = Integer.valueOf(cdx2[1]);
                                                }

                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                //freezeMark = false;

                                                int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardName, null, null);
                                                creator(srcImage, cardName);
                                                //  timer.cancel();
                                                //timer.start();
                                                turnCounter = "p1";
                                                turner.setVisibility(View.INVISIBLE);
                                                timer.cancel();
                                                cardNum.setText("[" + marketDeckSize + "]");

                                                int delayO = 5000;

                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                calltime.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                                String jres = response.body().getSuccessor();
                                                                int jnum = Integer.parseInt(jres);
                                                                int jnumsecs = jnum % 60;
                                                                int jnummins = jnum / 60;
                                                                timeZ.setTextColor(Color.RED);
                                                                if (jnummins < 0 || jnumsecs < 0) {
                                                                    timeZ.setText("Delay:  0  mins 0 secs");
                                                                } else {
                                                                    timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                                }
                                                                //here you can have your logic to set text to edittext
                                                            } else {

                                                            }

                                                        } else {


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                                    }
                                                });

                                            }

                                        } catch (NumberFormatException e) {
                                            NumberFormatException x = e;
                                        }

                                    } else if (success.getNoti().equals("whotter")) {

                                        // Get the widgets reference from XML layout
                                        final ConstraintLayout cl3 = findViewById(R.id.clx);
                                        final ConstraintLayout cl1 = findViewById(R.id.cl1);
                                        final ConstraintLayout cl2 = findViewById(R.id.cl2);

                                        cl3.setVisibility(View.VISIBLE);
                                        cl3.bringToFront();

                                        cl1.setForeground(getDrawable(R.drawable.backor));
                                        cl2.setForeground(getDrawable(R.drawable.backor));

                                        String jsonResponse = success.getNota();

                                        commandChange = jsonResponse;
                                        // timer.cancel();
                                        // timer.start();

                                        int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + jsonResponse, null, null);
                                        cardxi.setImageResource(srcImage);
                                        cl3.setVisibility(View.INVISIBLE);
                                        cl1.setForeground(null);
                                        cl2.setForeground(null);
                                        playerTwoWhotReceive = true;
                                        loader.setVisibility(View.INVISIBLE);
                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                        // freezeWhot = false;
                                        freezeGame = false;
                                        needWhot = false;
                                        turnCounter = "p1";
                                        //timeX.setTextColor(Color.BLACK);
                                        turner.setVisibility(View.INVISIBLE);
                                        timer.cancel();

                                        int delayO = 5000;

                                        RestService servicer = RestClient.getClient(session).create(RestService.class);
                                        Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                        calltime.enqueue(new Callback<User>() {
                                            @Override
                                            public void onResponse(Call<User> call, Response<User> response) {

                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        String jres = response.body().getSuccessor();
                                                        int jnum = Integer.parseInt(jres);
                                                        int jnumsecs = jnum % 60;
                                                        int jnummins = jnum / 60;
                                                        timeZ.setTextColor(Color.RED);
                                                        if (jnummins < 0 || jnumsecs < 0) {
                                                            timeZ.setText("Delay:  0  mins 0 secs");
                                                        } else {
                                                            timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                        }
                                                        //here you can have your logic to set text to edittext
                                                    } else {

                                                    }

                                                } else {


                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<User> call, Throwable t) {
                                                // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                            }
                                        });


                                    } else if (!success.getNoti().equals("") && !success.getNoti().equals("whotter") && !success.getNoti().equals("marketer")) {

                                        playerTwoReceive = true;
                                        String[] checkWordArr = success.getNota().split("z");
                                        if (checkWordArr[0].equals("w") || checkWordArr[1].equals("1") || checkWordArr[1].equals("2") || checkWordArr[1].equals("14")) {

                                        } else {
                                            freezeCard = false;
                                        }
                                        ImageView slotti = (ImageView) slotView1.getChildAt(slotView1.getChildCount() - 1);
                                        String cart = success.getNota();

                                        playedLast = "p2";
                                        timerx.cancel();
                                        int delayO = 5000;

                                        RestService servicer = RestClient.getClient(session).create(RestService.class);
                                        Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                        calltime.enqueue(new Callback<User>() {
                                            @Override
                                            public void onResponse(Call<User> call, Response<User> response) {

                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        String jres = response.body().getSuccessor();
                                                        int jnum = Integer.parseInt(jres);
                                                        int jnumsecs = jnum % 60;
                                                        int jnummins = jnum / 60;
                                                        timeZ.setTextColor(Color.RED);
                                                        if (jnummins < 0 || jnumsecs < 0) {
                                                            timeZ.setText("Delay:  0  mins 0 secs");
                                                        } else {
                                                            timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                                        }
                                                        //here you can have your logic to set text to edittext
                                                    } else {

                                                    }

                                                } else {


                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<User> call, Throwable t) {
                                                // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                        String jsonResponse = success.getGame_type();
                                        String resPopData = success.getNoto();
                                        String resPopData1 = success.getNotu();
                                        String resPopData2 = success.getNoty();

                                        String[] carter = cart.split("z");
                                        int cardNameNum = Integer.parseInt(carter[1]);

                                        if (freezeGame) return;

                                        if ("Finish&Count".equals(jsonResponse) || "Classic".equals(jsonResponse)) {
                                            marketDeckSize = success.getNote();

                                            if (cardNameNum == 2) {
                                                setPickTwo(resPopData1, resPopData2, "p2", "p1", slotti);

                                            } else if (cardNameNum == 14) {
                                                setGeneralMarket(resPopData, "p2", "p1", slotti);

                                            } else {
                                                if (Integer.parseInt(marketDeckSize) == 0) {
                                                    finishAndCount();

                                                }
                                            }


                                            if ((cardp2.size() * (50 * density)) < width) {
                                                more.setVisibility(View.INVISIBLE);
                                            }
                                            cardNum.setText("[" + marketDeckSize + "]");
                                            commandChange = "none";

                                            // timer.cancel();

                                            //  Toast.makeText(FinishWhotActivity.this, "Okay.", Toast.LENGTH_SHORT).show();


                                            if (cardp2.size() == 0) {
                                                if (checkWinner(cardp2.size(), "p2")) {
                                                    gameSound("stop");
                                                    soundOn = false;
                                                }
                                            }

                                        }

                                    }
                                }
                            }

                        }
                    }
                }

               // List<Success> differences = new ArrayList<>(successList);
               // successDiff.removeAll(successPlay);

                // success = differences.get(differences.size() - 1);
                // success = successDiff.get(successDiff.size() - 1);

                // successPlay.add(success);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
              //  System.out.println("The read failed: " + databaseError.getMessage());
            }
        });

        DatabaseReference gameDbRefx = database.getReference("startx");

        if(session.getUserID() == null){


        } else {

            // Read from the database

            startxEventListener = gameDbRefx.child(session.getUserID()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    Nota nota = dataSnapshot.getValue(Nota.class);

                    if("".equals(nota.getBacker())) {

                    } else {
                        if (nota.getGamer() != null) {

                            Intent intentRM = new Intent(FinishWhotActivity.this, MainActivity.class);
                            intentRM.putExtra("stopper", "false");
                            intentRM.putExtra("resetter","resetter");

                            startActivity(intentRM);
                            hidepDialog();
                            FinishWhotActivity.this.finish();
                        }
                        if (nota.getWinner() != null) {
                            String jsonWinner = nota.getWinner();

                            if (player.equals(jsonWinner)) {
                                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                                dialog.setCancelable(false);
                                dialog.setTitle("You Won!");
                                dialog.setMessage("Congratulations!");
                                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                        showpDialog();

                                        Intent intentR = new Intent( FinishWhotActivity.this, MainActivity.class);
                                        intentR.putExtra("stopper", "false");
                                        intentR.putExtra("usernome", session.getUserID());
                                        if(session.getUserID().equals(player1x)){
                                            intentR.putExtra("userchaller", player2x);
                                        } else {
                                            intentR.putExtra("userchaller", player1x);
                                        }
                                        intentR.putExtra("betGame", "Finish&Count");
                                        intentR.putExtra("bet_id", bet_id); Log.d("BET_ID","Msg: "+bet_id);
                                        intentR.putExtra("game_id", Integer.toString(gameId));

                                        startActivity(intentR);
                                        hidepDialog();
                                        FinishWhotActivity.this.finish();
                                    }
                                });
                                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                        exitButton.setVisibility(View.VISIBLE);
                                        exitButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                call.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {

                                                            } else {

                                                            }

                                                        } else {


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {

                                                    }
                                                });
                                                session.setGameState(null);betgamedb.deleter("game_state");

                                                session.setChallengeState(null);

                                                session.setNotaRec(null);
                                                session.setNoteRec(null);
                                                session.setNotiRec(null);
                                                session.setNotoRec(null);
                                                session.setNotuRec(null);
                                                session.setNotyRec(null);
                                                session.setWinaRec(null);
                                                session.setLosaRec(null);
                                                session.setPlay("false");
                                                session.setReqNota(null);
                                                session.setUserBeta(null);
                                                session.setUserAcc(null);
                                                session.setBetId(null);

                                                Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                intentOne.putExtra("stopper", "false");
                                                startActivity(intentOne);
                                                FinishWhotActivity.this.finish();
                                            }
                                        });
                                    }
                                });

                                final AlertDialog alert = dialog.create();
                                gameOver = true;
                                Handler handler = new Handler();
                                int delay = 3000; //milliseconds

                                handler.postDelayed(new Runnable(){
                                    public void run(){
                                        //do something
                                        if (Integer.parseInt(marketDeckSize) == 0) {
                                            dialoger = true;
                                        } else {

                                        }
                                        if(!dialoger) {
                                            if (!isFinishing()) {

                                                alert.show();
                                                playWinSound();
                                            } else {

                                            }

                                        }

                                    }
                                }, delay);

                                session.setGameState(null);betgamedb.deleter("game_state");
                                session.setEndState(gameId);
                                session.setChallengeState(null);
                                session.setAppState(null);
                                //   timer.cancel();
                                gameSound("stop");
                                soundOn = false;


                                //displayer("Game Over","Sorry, You lost!");
                                //handlerx.removeCallbacks(this);
                                freezeGame = true;
                                timer.cancel();
                                session.setNotaRec(null);
                                session.setNoteRec(null);
                                session.setNotiRec(null);
                                session.setNotoRec(null);
                                session.setNotuRec(null);
                                session.setNotyRec(null);
                                session.setWinaRec(null);
                                session.setLosaRec(null);

                                session.setPlay("false");
                                session.setReqNota(null);
                                session.setUserBeta(null);
                                session.setUserAcc(null);
                                session.setBetId(null);
                            } else {

                                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                                dialog.setCancelable(false);
                                dialog.setTitle("You Lost!");
                                dialog.setMessage("Sorry, you're not lucky this time.");
                                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                        showpDialog();



                                        Intent intentR = new Intent( FinishWhotActivity.this, MainActivity.class);
                                        intentR.putExtra("stopper", "false");
                                        intentR.putExtra("usernome", session.getUserID());
                                        if(session.getUserID().equals(player1x)){
                                            intentR.putExtra("userchaller", player2x);
                                        } else {
                                            intentR.putExtra("userchaller", player1x);
                                        }
                                        intentR.putExtra("betGame", "Finish&Count");
                                        intentR.putExtra("bet_id", bet_id); Log.d("BET_ID","Msg: "+bet_id);
                                        intentR.putExtra("game_id", Integer.toString(gameId));

                                        startActivity(intentR);
                                        hidepDialog();
                                        FinishWhotActivity.this.finish();
                                    }
                                });
                                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                        exitButton.setVisibility(View.VISIBLE);
                                        exitButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                call.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {

                                                            } else {

                                                            }

                                                        } else {


                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {

                                                    }
                                                });
                                                session.setGameState(null);betgamedb.deleter("game_state");

                                                session.setChallengeState(null);

                                                session.setNotaRec(null);
                                                session.setNoteRec(null);
                                                session.setNotiRec(null);
                                                session.setNotoRec(null);
                                                session.setNotuRec(null);
                                                session.setNotyRec(null);
                                                session.setWinaRec(null);
                                                session.setLosaRec(null);
                                                session.setPlay("false");
                                                session.setReqNota(null);
                                                session.setUserBeta(null);
                                                session.setUserAcc(null);
                                                session.setBetId(null);

                                                Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                intentOne.putExtra("stopper", "false");
                                                startActivity(intentOne);
                                                FinishWhotActivity.this.finish();
                                            }
                                        });
                                    }
                                });

                                final AlertDialog alert = dialog.create();
                                gameOver = true;
                                Handler handler = new Handler();
                                int delay = 3000; //milliseconds

                                handler.postDelayed(new Runnable(){
                                    public void run(){
                                        //do something
                                        if (Integer.parseInt(marketDeckSize) == 0) {
                                            dialoger = true;
                                        } else {

                                        }
                                        if(!dialoger) {
                                            if (!isFinishing()) {

                                                alert.show();
                                                playLoseSound();
                                            } else {

                                            }

                                        }

                                    }
                                }, delay);
                                session.setGameState(null);betgamedb.deleter("game_state");
                                session.setEndState(gameId);
                                session.setChallengeState(null);
                                session.setAppState(null);
                                //   timer.cancel();
                                gameSound("stop");
                                soundOn = false;


                                //displayer("Game Over","Sorry, You lost!");
                                //handlerx.removeCallbacks(this);
                                freezeGame = true;
                                timer.cancel();
                                session.setNotaRec(null);
                                session.setNoteRec(null);
                                session.setNotiRec(null);
                                session.setNotoRec(null);
                                session.setNotuRec(null);
                                session.setNotyRec(null);
                                session.setWinaRec(null);
                                session.setLosaRec(null);
                                session.setBetId(null);
                            }


                        }
                    }

                }




                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value

                    Log.w("FirebaseDB", "Failed to read value.", error.toException());
                }
            });
        }




        int delayt = 300000;


        runnablet = new Runnable(){
            public void run(){
                //do something
                RestService servicer = RestClient.getClient(session).create(RestService.class);
                Call<User> calltime = servicer.userTimer("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));
                calltime.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                String jres = response.body().getSuccessor();
                                int jnum = Integer.parseInt(jres);
                                int jnumsecs = jnum % 60;
                                int jnummins = jnum / 60;
                                timeZ.setTextColor(Color.RED);
                                if (jnummins < 0 || jnumsecs < 0){
                                    timeZ.setText("Delay:  0  mins 0 secs");

                                } else {
                                    timeZ.setText("Delay: " + jnummins + " mins " + jnumsecs + " secs");
                                }
                                //here you can have your logic to set text to edittext
                            } else {

                            }

                        } else {


                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        // Toast.makeText(FinishWhotActivity.this, "Error: " + t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };

        handler.postDelayed(runnablet, delayt);


        try {
            // TELEPHONY MANAGER class object to register one listener
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            //Create Listener
            PhoneStateListener phoneListener = new PhoneStateListener(){

                @Override
                public void onCallStateChanged(int state, String incomingNumber){

                    Log.d("phoneListener",state+"   incoming no:"+incomingNumber);

                    if (state == 1) {
                        gameSound("stop");
                       // Toast.makeText(getApplicationContext(),"Phone Is Riging",Toast.LENGTH_LONG).show();

                    } else if(state == 0){
                     //   gameSound("play");
                      //  Toast.makeText(getApplicationContext(),"Phone Is Idle",Toast.LENGTH_LONG).show();

                    } else if(state == 2){
                      //  Toast.makeText(getApplicationContext(),"Phone Is Picked",Toast.LENGTH_LONG).show();
                        gameSound("stop");

                    }

                }

            };

            // Register listener for LISTEN_CALL_STATE
            telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        } catch (Exception e) {
            Log.e("Phone Receive Error", " " + e);
        }




    }


    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            if(googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.
     */
    @Override
    protected void onResume() {
        super.onResume();
        active = true;
    }


    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        super.onPause();
        active = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }


    @Override
    public void onDestroy () {
        handler.removeCallbacks(runnable);
        handler.removeCallbacks(runnablex);
        handler.removeCallbacks(runnablemx);
        handler.removeCallbacks(runnablew);
        handler.removeCallbacks(runnablewx);
        handler.removeCallbacks(runnablem);
        handler.removeCallbacks(runnablec);
        handler.removeCallbacks(runnablet);


        super.onDestroy();
        active = false;

    }


    final CountDownTimer timer = new CountDownTimer(30000, 1000) {

        public void onTick(long millisUntilFinished) {
            timeX.setTextColor(Color.WHITE);
            timor = (int)millisUntilFinished / 1000;
            timeX.setText("Timer: "+millisUntilFinished / 1000 + " secs");
            //here you can have your logic to set text to edittext
        }

        public void onFinish() {
            checkTimer();
            // gameSound("stop");
            //soundOn = false;
        }

    };



    long totalSeconds = 600;
    long intervalSeconds = 1;

    CountDownTimer timerx = new CountDownTimer(totalSeconds * 1000, intervalSeconds * 1000) {

        public void onTick(long millisUntilFinished) {
            long l = totalSeconds * 1000 - millisUntilFinished / 1000;
            int lnum = (int)l % 60;
            int lmins = lnum/60;

            timeX.setText("Counter: - "+ lmins +" mins "+ lnum +" secs");

        }

        public void onFinish() {
            Log.d( "Done!", "Time's up!");
            if(!turnCounter.equals("p1")){
                winner = "p1";

                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                dialog.setCancelable(false);
                dialog.setTitle("You Lost!");
                dialog.setMessage("Sorry, you're not lucky this time." );
                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        showpDialog();



                        Intent intentR = new Intent( FinishWhotActivity.this, MainActivity.class);
                        intentR.putExtra("stopper", "false");
                        intentR.putExtra("usernome", session.getUserID());
                        if(session.getUserID().equals(player1x)){
                            intentR.putExtra("userchaller", player2x);
                        } else {
                            intentR.putExtra("userchaller", player1x);
                        }
                        intentR.putExtra("betGame", "Finish&Count");
                        intentR.putExtra("bet_id", bet_id); Log.d("BET_ID","Msg: "+bet_id);
                        intentR.putExtra("game_id", Integer.toString(gameId));

                        startActivity(intentR);
                        hidepDialog();
                        FinishWhotActivity.this.finish();
                    }
                });
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    exitButton.setVisibility(View.VISIBLE);
                                    exitButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            RestService servicer = RestClient.getClient(session).create(RestService.class);
                                            Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                            call.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {

                                                        } else {

                                                        }

                                                    } else {


                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {

                                                }
                                            });
                                            session.setGameState(null);betgamedb.deleter("game_state");

                                            session.setChallengeState(null);

                                            session.setNotaRec(null);
                                            session.setNoteRec(null);
                                            session.setNotiRec(null);
                                            session.setNotoRec(null);
                                            session.setNotuRec(null);
                                            session.setNotyRec(null);
                                            session.setWinaRec(null);
                                            session.setLosaRec(null);
                                            session.setPlay("false");
                                            session.setReqNota(null);
                                            session.setUserBeta(null);
                                            session.setUserAcc(null);
                                            session.setBetId(null);

                                            Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                            intentOne.putExtra("stopper", "false");
                                            startActivity(intentOne);
                                            FinishWhotActivity.this.finish();
                                        }
                                    });
                                }
                            });

                final AlertDialog alert = dialog.create();
                gameOver = true;
                Handler handler = new Handler();
                int delay = 3000; //milliseconds

                handler.postDelayed(new Runnable(){
                    public void run(){
                        //do something
                        if(!dialoger) {
                            if (!isFinishing()) {

                                alert.show();
                                playLoseSound();
                            } else {

                            }

                        }

                    }
                }, delay);

                session.setGameState(null);betgamedb.deleter("game_state");
                session.setEndState(gameId);
                session.setChallengeState(null);
                session.setAppState(null);
                //   timer.cancel();
                session.setBetId(null);

            } else {
                winner = "p2";

                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                dialog.setCancelable(false);
                dialog.setTitle("You Won!");
                dialog.setMessage("Congratulations!" );
                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        showpDialog();



                        Intent intentR = new Intent( FinishWhotActivity.this, MainActivity.class);
                        intentR.putExtra("stopper", "false");
                        intentR.putExtra("usernome", session.getUserID());
                        if(session.getUserID().equals(player1x)){
                            intentR.putExtra("userchaller", player2x);
                        } else {
                            intentR.putExtra("userchaller", player1x);
                        }
                        intentR.putExtra("betGame", "Finish&Count");
                        intentR.putExtra("bet_id", bet_id); Log.d("BET_ID","Msg: "+bet_id);
                        intentR.putExtra("game_id", Integer.toString(gameId));

                        startActivity(intentR);
                        hidepDialog();
                        FinishWhotActivity.this.finish();
                    }
                });
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        exitButton.setVisibility(View.VISIBLE);
                        exitButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                call.enqueue(new Callback<User>() {
                                    @Override
                                    public void onResponse(Call<User> call, Response<User> response) {

                                        if (response.isSuccessful()) {
                                            if (response.body() != null) {

                                            } else {

                                            }

                                        } else {


                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<User> call, Throwable t) {

                                    }
                                });
                                session.setGameState(null);betgamedb.deleter("game_state");

                                session.setChallengeState(null);

                                session.setNotaRec(null);
                                session.setNoteRec(null);
                                session.setNotiRec(null);
                                session.setNotoRec(null);
                                session.setNotuRec(null);
                                session.setNotyRec(null);
                                session.setWinaRec(null);
                                session.setLosaRec(null);
                                session.setPlay("false");
                                session.setReqNota(null);
                                session.setUserBeta(null);
                                session.setUserAcc(null);
                                session.setBetId(null);

                                Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                intentOne.putExtra("stopper", "false");
                                startActivity(intentOne);
                                FinishWhotActivity.this.finish();
                            }
                        });
                    }
                });

                final AlertDialog alert = dialog.create();
                gameOver = true;
                Handler handler = new Handler();
                int delay = 3000; //milliseconds

                handler.postDelayed(new Runnable(){
                    public void run(){
                        //do something
                        if(!dialoger) {
                            if (!isFinishing()) {

                                alert.show();
                                playWinSound();
                            } else {

                            }

                        }

                    }
                }, delay);
                session.setGameState(null);betgamedb.deleter("game_state");
                session.setEndState(gameId);
                session.setChallengeState(null);
                session.setAppState(null);
                //  timer.cancel();
                session.setBetId(null);

            }
            gameSound("stop");
            soundOn = false;
        }

    };


    @Override
    public void onClick(View view) {
        //listens for click to reset slotViews


    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(FinishWhotActivity.this)
                .setTitle("Exit Game and Lose?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        if(!gameOver) {
                            // FinishWhotActivity.super.onBackPressed();
                            RestService servicer = RestClient.getClient(session).create(RestService.class);
                            Call<User> callretro = servicer.userOnback("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                            callretro.enqueue(new Callback<User>() {
                                @Override
                                public void onResponse(Call<User> call, Response<User> response) {

                                    if (response.isSuccessful()) {
                                        if (response.body() != null) {
                                            String jres = response.body().getSuccessor();
                                            //  Toast.makeText(FinishWhotActivity.this, "OnBacker: "+response.body().getSuccessor() , Toast.LENGTH_LONG).show();
                                            if (jres.equals("success")) {
                                                backpressed = true;

                                                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                                                dialog.setCancelable(false);
                                                dialog.setTitle("You Lost!");
                                                dialog.setMessage("Sorry, you're not lucky this time.");
                                                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        showpDialog();


                                                        Intent intentR = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                        intentR.putExtra("stopper", "false");
                                                        intentR.putExtra("usernome", session.getUserID());
                                                        if (session.getUserID().equals(player1x)) {
                                                            intentR.putExtra("userchaller", player2x);
                                                        } else {
                                                            intentR.putExtra("userchaller", player1x);
                                                        }
                                                        intentR.putExtra("betGame", "Finish&Count");
                                                        intentR.putExtra("bet_id", bet_id);
                                                        Log.d("BET_ID", "Msg: " + bet_id);
                                                        intentR.putExtra("game_id", Integer.toString(gameId));

                                                        startActivity(intentR);
                                                        hidepDialog();
                                                        FinishWhotActivity.this.finish();
                                                    }
                                                });
                                                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        exitButton.setVisibility(View.VISIBLE);
                                                        exitButton.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                                Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                                                call.enqueue(new Callback<User>() {
                                                                    @Override
                                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                                        if (response.isSuccessful()) {
                                                                            if (response.body() != null) {

                                                                            } else {

                                                                            }

                                                                        } else {


                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<User> call, Throwable t) {

                                                                    }
                                                                });
                                                                session.setGameState(null);betgamedb.deleter("game_state");

                                                                session.setChallengeState(null);

                                                                session.setNotaRec(null);
                                                                session.setNoteRec(null);
                                                                session.setNotiRec(null);
                                                                session.setNotoRec(null);
                                                                session.setNotuRec(null);
                                                                session.setNotyRec(null);
                                                                session.setWinaRec(null);
                                                                session.setLosaRec(null);
                                                                session.setPlay("false");
                                                                session.setReqNota(null);
                                                                session.setUserBeta(null);
                                                                session.setUserAcc(null);
                                                                session.setBetId(null);

                                                                Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                                                intentOne.putExtra("stopper", "false");
                                                                startActivity(intentOne);
                                                                FinishWhotActivity.this.finish();
                                                            }
                                                        });
                                                    }
                                                });

                                                final AlertDialog alert = dialog.create();
                                                gameOver = true;
                                                Handler handler = new Handler();
                                                int delay = 3000; //milliseconds

                                                handler.postDelayed(new Runnable() {
                                                    public void run() {
                                                        //do something
                                                        if (!dialoger) {
                                                            if (!isFinishing()) {

                                                                //  alert.show();
                                                              //  playLoseSound();
                                                            } else {

                                                            }

                                                        }

                                                    }
                                                }, delay);
                                                session.setGameState(null);betgamedb.deleter("game_state");
                                                session.setEndState(gameId);
                                                session.setChallengeState(null);
                                                session.setAppState(null);
                                                //   timer.cancel();
                                                gameSound("stop");
                                                soundOn = false;


                                                //displayer("Game Over","Sorry, You lost!");
                                                //handlerx.removeCallbacks(this);
                                                freezeGame = true;
                                                timer.cancel();

                                                Toast.makeText(FinishWhotActivity.this, "Game Over, You Lost!", Toast.LENGTH_LONG).show();
                                                if (checkWinner(0, "p1")) {
                                                    gameSound("stop");
                                                    soundOn = false;
                                                }
                                            }
                                            session.setNotaRec(null);
                                            session.setNoteRec(null);
                                            session.setNotiRec(null);
                                            session.setNotoRec(null);
                                            session.setNotuRec(null);
                                            session.setNotyRec(null);
                                            session.setWinaRec(null);
                                            session.setLosaRec(null);
                                            session.setBetId(null);

                                        } else {

                                        }

                                    } else {

                                        //    Toast.makeText(FinishWhotActivity.this, "OnBacker: "+response.toString() , Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<User> call, Throwable t) {
                                    //     Toast.makeText(FinishWhotActivity.this, "OnBacker: "+t.toString() , Toast.LENGTH_LONG).show();

                                    Log.d("onFailure", t.toString());


                                }
                            });
                        }

                    }
                }).create().show();
    }

    public void animCard(float slotT, float slotL, final ImageView carder, Handler hnd) {
        //listens for dice roll and or clicks for ludo seed placement (when user selects where he want to place the seeds dueing game play)

        ObjectAnimator anim0 = ObjectAnimator.ofFloat(carder, "translationX", 0f,slotL);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(carder, "translationY", 0f,slotT);
        anim0.setDuration(100);
        anim1.setDuration(100);
        anim0.start();
        anim1.start();

        //ImageView cardero = carder;

        hnd.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 1000ms
                carder.setVisibility(View.GONE);

            }
        }, 255);

    }

    public void animCardx(float slotX, final ImageView carder, Handler hnd) {
        //listens for dice roll and or clicks for ludo seed placement (when user selects where he want to place the seeds dueing game play)

        ObjectAnimator animx = ObjectAnimator.ofFloat(carder, "translationY", 0f,slotX);
        animx.setDuration(100);
        animx.start();

        //ImageView cardero = carder;

        hnd.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 1000ms
                carder.setVisibility(View.GONE);

            }
        }, 255);

    }


    public ImageView creator(int card_type, String card) {

        int containerLeft;
        float density = this.getResources().getDisplayMetrics().density;
        ConstraintSet set = new ConstraintSet();

        // Get the widgets reference from XML layout
        final ConstraintLayout cl2 = findViewById(R.id.cl2);

        ImageView slotter = (ImageView) slotView2.getChildAt(slotView2.getChildCount() - 1);
        if(slotter == null){
            containerLeft = 0;
        } else {
            containerLeft = 1;
        }

        final ImageView cardix = new ImageView(FinishWhotActivity.this);
        cardix.setId(View.generateViewId());
        cardix.setClickable(true);
        cardix.setContentDescription(card);
        cardix.setImageResource(card_type);
        int lx = (int) (8 * density);
        int bx = (int) (8 * density);

        int w = cardxi.getWidth();
        int h = cardxi.getHeight();


        int topper = 0;
        int righter = 0;
        LayoutParams np = new LayoutParams(w, h);

        // Add rule to layout parameters
        // Add the ImageView on top of Decker
        np.setMargins(lx,topper,righter,bx);

        // Add layout parameters to ImageView
        cardix.setLayoutParams(np);

        // Finally, add the ImageView to layout
        cl2.addView(cardix);

        // ImageView slotty = cardix;

        cardix.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (freezeMark) {
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeWhot){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeCard){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeGame){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }

                final int delay = 5000; //milliseconds
                final Handler handler = new Handler();
                ImageView slotti = (ImageView)slotView1.getChildAt(slotView1.getChildCount() - 1);

                final String cart = (String)cardix.getContentDescription();

                String[] carter = cart.split("z");
                String cardNamePart = carter[0];
                int cardNameNum = Integer.parseInt(carter[1]);
              //  Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();

                String boardCard;
                String[] boardTotal;
                String borderPart;
                int bordNum;
                if(commandChange.equals("none")){
                    boardCard = cardx.get(cardx.size() - 1);
                    boardTotal = boardCard.split("z");
                    borderPart = boardTotal[0];
                    bordNum = Integer.parseInt(boardTotal[1]);
                } else {
                    borderPart = commandChange;
                    bordNum = 100;
                }

                Log.d("Centre[0]X: ",borderPart);
                Log.d("Centre[1]X: ", ""+ bordNum);


                if(turnCounter.equals("p2")) {
                    holdOn = "none";
                    if(cardRuler(borderPart,bordNum,cardNamePart,cardNameNum,"p2","p1",slotti)){
                       // timer.cancel();

                        freezeCard = true;
                        //timer.start();
                        playerTwoMove = true;
                        playerTwoReceive = false;
                        playCardSound();
                        session.setMoveState(session.getUserID());
                        animCardx(-100,cardix,handler);
                        int src = getResources().getIdentifier("com.godwin.betgames:drawable/" + cart, null, null);
                        cardxi.setImageResource(src);
                        //cardx.add(cardMover(cardp2, cardp2.indexOf(cart)));
                        cardp2.remove(cart);
                        cardx.add(cart);

                        if (cardNameNum == 2) {
                            String cd3;
                            String cd3i;

                            cd3 = "jz"+cardp1.size();
                            cd3i = "jz"+cardp1.size()+1;
                            cardp1.add(cd3);
                            cardp1.add(cd3i);

                            cardName = cd3;
                            cardNameN = cd3i;
                            String[] cdx3 = cardName.split("z");
                            String[] cdx3i = cardNameN.split("z");
                            cardNamePart = cdx3[0];
                            cardNamePartN = cdx3i[0];
                            try {
                                cardNameNum = Integer.parseInt(cdx3[1]);
                            }
                            catch (NumberFormatException e)
                            {
                                cardNameNum = Integer.valueOf(cdx3[1]);
                            }

                            try {
                                cardNameNumN = Integer.parseInt(cdx3i[1]);
                            }
                            catch (NumberFormatException e)
                            {
                                cardNameNumN = Integer.valueOf(cdx3i[1]);
                            }
                            if(turnCounter.equals("p2")){
                                creatorx(R.drawable.backofcard,cardName);
                                creatorx(R.drawable.backofcard,cardNameN);
                            } else {
                                int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardName, null, null);
                                int srcImagen = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardNameN, null, null);
                                creator(srcImage,cardName);
                                creator(srcImagen,cardNameN);
                            }

                            pickTwo = "none";
                            turnCounter = "p2";


                            //turner.setVisibility(View.VISIBLE);
                            //timer.start();


                        } else if (cardNameNum == 14) {
                            String cd1;

                            cd1 = "jz"+cardp1.size();
                            cardp1.add(cd1);

                            cardName = cd1;
                            String[] cdx1 = cardName.split("z");
                            cardNamePart = cdx1[0];
                            try {
                                cardNameNum = Integer.parseInt(cdx1[1]);
                            }
                            catch (NumberFormatException e)
                            {
                                cardNameNum = Integer.valueOf(cdx1[1]);
                            }

                            if(turnCounter.equals("p2")){
                                creatorx(R.drawable.backofcard,cardName);
                            } else {
                                int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardName, null, null);
                                creator(srcImage,cardName);
                            }
                            generalMarket = "none";
                            turnCounter = "p2";

                          //  turner.setVisibility(View.VISIBLE);
                           // timer.start();

                        } else  if(cardNameNum == 1){

                          //  timer.start();

                        } else if(cardNamePart.equals("w")){

                           // timer.start();

                        } else {


                        }


                        RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                        Call<User> call = servicePlay.userPlay("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,cart,(String)timeX.getText(),Integer.toString(gameId));

                        call.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {

                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if(response.body().getSuccess().getError() != null){
                                         //   Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                        } else {
                                        //    Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();

                                        }
                                     //   Toast.makeText(FinishWhotActivity.this, "Cardx sent: "+cart, Toast.LENGTH_SHORT).show();

                                    }

                                } else {
                                  //  Toast.makeText(FinishWhotActivity.this, "Error: "+response.toString(), Toast.LENGTH_SHORT).show();
                                    if(playerTwoReceive){
                                        playerTwoMove = true;
                               //         Toast.makeText(FinishWhotActivity.this, "Cardx sent1: "+cart, Toast.LENGTH_SHORT).show();
                                    } else {
                                        playerTwoMove = false;

                                        runnable = new Runnable(){
                                            public void run(){
                                                //do something
                                                if(playerTwoReceive){
                                                    playerTwoMove = true;
                                             //       Toast.makeText(FinishWhotActivity.this, "Cardx sentA: "+cart, Toast.LENGTH_SHORT).show();
                                                    handler.removeCallbacks(runnable);
                                                } else {
                                                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                    Call<User> callx = servicePlay.userPlay("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,cart,(String)timeX.getText(),Integer.toString(gameId));

                                                    callx.enqueue(new Callback<User>() {
                                                        @Override
                                                        public void onResponse(Call<User> call, Response<User> response) {

                                                            if (response.isSuccessful()) {
                                                                if (response.body() != null) {
                                                                    if(response.body().getSuccess().getError() != null){
                                                                  //      Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                                    } else {
                                                                    //    Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                        playerTwoMove = true;
                                                                        handler.removeCallbacks(runnable);
                                                                    }
                                                           //         Toast.makeText(FinishWhotActivity.this, "Cardx sentB: "+cart, Toast.LENGTH_SHORT).show();

                                                                } else {
                                                            //        Toast.makeText(FinishWhotActivity.this, "Cardx sentC: "+cart, Toast.LENGTH_SHORT).show();
                                                                    playerTwoMove = true;
                                                                    handler.removeCallbacks(runnable);
                                                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                                }

                                                            } else {
                                                                if(playerTwoReceive){
                                                                    playerTwoMove = true;
                                                            //        Toast.makeText(FinishWhotActivity.this, "Cardx sentD: "+cart, Toast.LENGTH_SHORT).show();
                                                                    handler.removeCallbacks(runnable);
                                                                } else {
                                                                    playerTwoMove = false;
                                                                    handler.postDelayed(runnable, delay);
                                                                }
                                                                // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                        @Override
                                                        public void onFailure(Call<User> call, Throwable t) {
                                                            if(playerTwoReceive){
                                                                playerTwoMove = true;
                                                          //      Toast.makeText(FinishWhotActivity.this, "Cardx sentE: "+cart, Toast.LENGTH_SHORT).show();
                                                                handler.removeCallbacks(runnable);
                                                            } else {
                                                                playerTwoMove = false;
                                                                handler.postDelayed(runnable, delay);
                                                            }
                                                            // Toast.makeText(FinishWhotActivity.this, "PlayError: "+t.toString(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    });

                                                }

                                            }
                                        };
                                        if(playerTwoReceive && playerTwoMove){
                                            handler.removeCallbacks(runnable);
                                     //       Toast.makeText(FinishWhotActivity.this, "Cardx sent2: "+cart, Toast.LENGTH_SHORT).show();

                                        } else {
                                            handler.postDelayed(runnable, delay);
                                        }

                                    }
                                    // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<User> call, Throwable t) {

                                if(playerTwoReceive){
                                    playerTwoMove = true;
                            //        Toast.makeText(FinishWhotActivity.this, "Cardx sent3: "+cart, Toast.LENGTH_SHORT).show();
                                    handler.removeCallbacks(runnable);
                                } else {
                                    playerTwoMove = false;

                                    runnable = new Runnable(){
                                        public void run(){
                                            //do something
                                            if(playerTwoReceive){
                                                playerTwoMove = true;
                                    //            Toast.makeText(FinishWhotActivity.this, "Cardx sentA: "+cart, Toast.LENGTH_SHORT).show();
                                                handler.removeCallbacks(runnable);
                                            } else {
                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callx = servicePlay.userPlay("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,cart,(String)timeX.getText(),Integer.toString(gameId));

                                                callx.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                //                Toast.makeText(FinishWhotActivity.this, "Cardx sentB: "+cart, Toast.LENGTH_SHORT).show();
                                                                playerTwoMove = true;
                                                                handler.removeCallbacks(runnable);
                                                            } else {
                                                 //               Toast.makeText(FinishWhotActivity.this, "Cardx sentC: "+cart, Toast.LENGTH_SHORT).show();
                                                                playerTwoMove = true;
                                                                handler.removeCallbacks(runnable);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                            }

                                                        } else {
                                                            if(playerTwoReceive){
                                                                playerTwoMove = true;
                                                //                Toast.makeText(FinishWhotActivity.this, "Cardx sentD: "+cart, Toast.LENGTH_SHORT).show();
                                                                handler.removeCallbacks(runnable);
                                                            } else {
                                                                playerTwoMove = false;
                                                                handler.postDelayed(runnable, delay);
                                                            }
                                                            // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        if(playerTwoReceive){
                                                            playerTwoMove = true;
                                            //                Toast.makeText(FinishWhotActivity.this, "Cardx sentE: "+cart, Toast.LENGTH_SHORT).show();
                                                            handler.removeCallbacks(runnable);
                                                        } else {
                                                            playerTwoMove = false;
                                                            handler.postDelayed(runnable, delay);
                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "PlayError: "+t.toString(), Toast.LENGTH_SHORT).show();
                                                    }
                                                });

                                            }

                                        }
                                    };
                                    if(playerTwoReceive && playerTwoMove){
                             //           Toast.makeText(FinishWhotActivity.this, "Cardx sent4: "+cart, Toast.LENGTH_SHORT).show();
                                        handler.removeCallbacks(runnable);

                                    } else {
                                        handler.postDelayed(runnable, delay);
                                    }

                                }
                                // Toast.makeText(FinishWhotActivity.this, "PlayError: "+t.toString(), Toast.LENGTH_SHORT).show();

                            }
                        });

                    } else {
                        if(gameOver){
                            Toast.makeText(FinishWhotActivity.this, "Game Over!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(FinishWhotActivity.this, "Wrong Move", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    if(gameOver){
                        Toast.makeText(FinishWhotActivity.this, "Game Over!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FinishWhotActivity.this, "Wrong Turn", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        if(containerLeft == 0){
            set.clone(cl2);
            set.connect(cardix.getId(), ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT);
            set.connect(cardix.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
            set.applyTo(cl2);
        } else {
            set.clone(cl2);
            set.connect(cardix.getId(), ConstraintSet.LEFT, slotter.getId(), ConstraintSet.RIGHT);
            set.connect(cardix.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
            set.applyTo(cl2);
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        if((cardp2.size() * (50 * density)) > width) {
            more.setVisibility(View.VISIBLE);
            hsv2.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
        }
        hsv2.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
        return cardix;

    }


    public ImageView creatorx( int card_type, String card) {

        int containerLeft;
        float density = this.getResources().getDisplayMetrics().density;
        ConstraintSet set = new ConstraintSet();

        // Get the widgets reference from XML layout
        final ConstraintLayout cl1 = findViewById(R.id.cl1);

        ImageView slotter = (ImageView) slotView1.getChildAt(slotView1.getChildCount() - 1);
        if(slotter == null){
            containerLeft = 0;
        } else {
            containerLeft = 1;
        }

        ImageView cardix = new ImageView(FinishWhotActivity.this);
        cardix.setId(View.generateViewId());
        cardix.setContentDescription(card);
        cardix.setImageDrawable(getDrawable(card_type));
        int lx = (int) (8 * density);
        int tx = (int) (8 * density);

        int w = cardxi.getWidth();
        int h = cardxi.getHeight();

        int bottomer = 0;
        int righter = 0;
        LayoutParams np = new LayoutParams(w, h);

        // Add rule to layout parameters
        // Add the ImageView on top of Decker
        np.setMargins(lx,tx,righter,bottomer);

        // Add layout parameters to ImageView
        cardix.setLayoutParams(np);

        // Finally, add the ImageView to layout
        cl1.addView(cardix);


        if(containerLeft == 0){
            set.clone(cl1);
            set.connect(cardix.getId(), ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT);
            set.connect(cardix.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            set.applyTo(cl1);
        } else {

            set.clone(cl1);
            set.connect(cardix.getId(), ConstraintSet.LEFT, slotter.getId(), ConstraintSet.RIGHT);
            set.connect(cardix.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            set.applyTo(cl1);
        }

        hsv1.fullScroll(HorizontalScrollView.FOCUS_RIGHT);

        return cardix;

    }


    public ImageView creatory(int card_type, String card, ImageView slotter) {

        int containerLeft;
        float density = this.getResources().getDisplayMetrics().density;
        ConstraintSet set = new ConstraintSet();

        // Get the widgets reference from XML layout
        final ConstraintLayout cl1 = findViewById(R.id.cl1);

        if(slotter == null){
            containerLeft = 0;
        } else {
            containerLeft = 1;
        }

        ImageView cardix = new ImageView(FinishWhotActivity.this);
        cardix.setId(View.generateViewId());
        cardix.setContentDescription(card);
        cardix.setImageDrawable(getDrawable(card_type));
        int lx = (int) (8 * density);
        int tx = (int) (8 * density);

        int w = cardxi.getWidth();
        int h = cardxi.getHeight();

        int bottomer = 0;
        int righter = 0;
        LayoutParams np = new LayoutParams(w, h);

        // Add rule to layout parameters
        // Add the ImageView on top of Decker
        np.setMargins(lx,tx,righter,bottomer);

        // Add layout parameters to ImageView
        cardix.setLayoutParams(np);

        // Finally, add the ImageView to layout
        cl1.addView(cardix);


        if(containerLeft == 0){
            set.clone(cl1);
            set.connect(cardix.getId(), ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT);
            set.connect(cardix.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            set.applyTo(cl1);
        } else {
            set.clone(cl1);
            set.connect(cardix.getId(), ConstraintSet.LEFT, slotter.getId(), ConstraintSet.RIGHT);
            set.connect(cardix.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            set.applyTo(cl1);
        }

        hsv1.fullScroll(HorizontalScrollView.FOCUS_RIGHT);

        return cardix;

    }

    public ImageView creatorz(ImageView slotter,String cardiz) {

        float density = this.getResources().getDisplayMetrics().density;
        ConstraintSet set = new ConstraintSet();
        final ImageView cardis;
        cardis = new ImageView(FinishWhotActivity.this);

       // if(cardx.size() >= 4) {

            // Get the widgets reference from XML layout
            final ConstraintLayout cls = findViewById(R.id.clx);
            final ConstraintLayout cl1 = findViewById(R.id.cl1);
            final ConstraintLayout cl2 = findViewById(R.id.cl2);

        int containerLeft;

        if(slotter == null){
            containerLeft = 0;
        } else {
            containerLeft = 1;
        }

        cardw.setVisibility(View.INVISIBLE);
        cardw1.setVisibility(View.INVISIBLE);
        cardw2.setVisibility(View.INVISIBLE);
        cardw3.setVisibility(View.INVISIBLE);
        cardw4.setVisibility(View.INVISIBLE);

        cardWhotix.setVisibility(View.INVISIBLE);

        cls.setVisibility(View.VISIBLE);

            cls.bringToFront();
            cl1.setForeground(getDrawable(R.drawable.backor));
            cl2.setForeground(getDrawable(R.drawable.backor));

           // for (int i = cardx.size() - 1; i >= cardx.size() - 3; i--) {

                cardis.setId(View.generateViewId());
                cardis.setContentDescription(cardiz);
                int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardiz, null, null);
                cardis.setImageDrawable(getDrawable(srcImage));


                int lx = (int) (8 * density);
                int tx = (int) (8 * density);

                int w = cardxi.getWidth();
                int h = cardxi.getHeight();

                int bottomer = 0;
                int righter = 0;
                LayoutParams np = new LayoutParams(w, h);

                // Add rule to layout parameters
                // Add the ImageView on top of Decker
                np.setMargins(lx, tx, righter, bottomer);

                // Add layout parameters to ImageView
                cardis.setLayoutParams(np);

                // Finally, add the ImageView to layout
                cls.addView(cardis);


                cardis.setOnClickListener(new View.OnClickListener() {

                                              @Override
                                              public void onClick(View v) {
                                                  cardw.setVisibility(View.VISIBLE);
                                                  cardw1.setVisibility(View.VISIBLE);
                                                  cardw2.setVisibility(View.VISIBLE);
                                                  cardw3.setVisibility(View.VISIBLE);
                                                  cardw4.setVisibility(View.VISIBLE);
                                                  cardWhotix.setVisibility(View.VISIBLE);
                                                  cardis.setVisibility(View.GONE);
                                                  cl1.setForeground(null);
                                                  cl2.setForeground(null);
                                                  cls.setVisibility(View.GONE);
                                              }
                                          });

        if(containerLeft == 0){
            set.clone(cls);
            set.connect(cardis.getId(), ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT);
            set.connect(cardis.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            set.applyTo(cls);
        } else {

            set.clone(cls);
            set.connect(cardis.getId(), ConstraintSet.LEFT, slotter.getId(), ConstraintSet.RIGHT);
            set.connect(cardis.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            set.applyTo(cls);
        }

         //   }

           // hsv1.fullScroll(HorizontalScrollView.FOCUS_RIGHT);

     //   }

        return cardis;

    }


    public void creatorWhotter(final String playerx) {

        freezeGame = true;
        needWhot = true;
        // Get the widgets reference from XML layout
        final ConstraintLayout cl3 = findViewById(R.id.clx);
        final ConstraintLayout cl1 = findViewById(R.id.cl1);
        final ConstraintLayout cl2 = findViewById(R.id.cl2);

        cardw.setVisibility(View.VISIBLE);
        cardw1.setVisibility(View.VISIBLE);
        cardw2.setVisibility(View.VISIBLE);
        cardw3.setVisibility(View.VISIBLE);
        cardw4.setVisibility(View.VISIBLE);

        cardWhotix.setVisibility(View.VISIBLE);

        cl1.setForeground(null);
        cl2.setForeground(null);
        cl3.setVisibility(View.GONE);

        cl3.setVisibility(View.VISIBLE);
        cl3.bringToFront();

        cardw.bringToFront();
        cardw1.bringToFront();
        cardw2.bringToFront();
        cardw3.bringToFront();
        cardw4.bringToFront();

        cl3.setVisibility(View.VISIBLE);
        cl3.bringToFront();

        cl1.setForeground(getDrawable(R.drawable.backor));
        cl2.setForeground(getDrawable(R.drawable.backor));

        ImageView cardWhot = findViewById(R.id.k);

        timeX.setTextColor(Color.WHITE);

        final Handler handler = new Handler();
        final int delay = 5000; //milliseconds

        cardWhot.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                playCardSound();

                if (freezeMark) {
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeWhot){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeCard){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }

                    if(freezeGame){
                        loader.setVisibility(View.VISIBLE);
                        Glide.with(FinishWhotActivity.this).load(R.drawable.loader).into(loader);
                        playerTwoWhotMove = true;
                        playerTwoWhotReceive = false;

                        freezeWhot = true;
                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                    Call<User> call = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {

                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if(response.body().getSuccess().getError() != null){
                                   //     Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                    } else {
                                   //     Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                        handler.removeCallbacks(runnablew);
                                    }

                                } else {
                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                }

                            } else {
                                if(playerTwoWhotReceive){
                                    playerTwoWhotMove = true;
                                    loader.setVisibility(View.INVISIBLE);
                                    Glide.with(FinishWhotActivity.this).clear(loader);
                                } else {
                                    playerTwoWhotMove = false;

                                    runnablew = new Runnable(){
                                        public void run(){
                                            //do something
                                            if(playerTwoWhotReceive){
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablew);
                                            } else {
                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callz = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                                                callz.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {

                                                                if(response.body().getSuccess().getError() != null){
                                              //                      Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                                } else {
                                              //                      Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                    handler.removeCallbacks(runnablew);
                                                                }
                                                            } else {
                                                                handler.removeCallbacks(runnablew);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                            }

                                                        } else {
                                                            if(playerTwoWhotReceive){
                                                                playerTwoWhotMove = true;
                                                                loader.setVisibility(View.INVISIBLE);
                                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                                handler.removeCallbacks(runnablew);
                                                            } else {
                                                                playerTwoWhotMove = false;
                                                                handler.postDelayed(runnablew, delay);
                                                            }
                                                            // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                        }
                                                    }
                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        if(playerTwoWhotReceive){
                                                            playerTwoWhotMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablew);
                                                        } else {
                                                            playerTwoWhotMove = false;
                                                            handler.postDelayed(runnablew, delay);
                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                    }
                                                });
                                            }

                                        }
                                    };

                                    if(playerTwoWhotReceive && playerTwoWhotMove){
                                        handler.removeCallbacks(runnablew);

                                    } else {
                                        handler.postDelayed(runnablew, delay);
                                    }

                                }
                                // Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            if(playerTwoWhotReceive){
                                playerTwoWhotMove = true;
                                loader.setVisibility(View.INVISIBLE);
                                Glide.with(FinishWhotActivity.this).clear(loader);
                            } else {
                                playerTwoWhotMove = false;
                                runnablew = new Runnable(){
                                    public void run(){
                                        //do something
                                        if(playerTwoWhotReceive){
                                            loader.setVisibility(View.INVISIBLE);
                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                            handler.removeCallbacks(runnablew);
                                        } else {
                                            RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                            Call<User> callz = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                                            callz.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {
                                                            /**
                                                             String jsonResponse = response.body().getSuccessor();
                                                             if(jsonResponse.equals("success")){
                                                             commandChange = "k";
                                                             //timer.cancel();
                                                             // timer.start();
                                                             loader.setVisibility(View.INVISIBLE);
                                                             cardxi.setImageResource(R.drawable.k);
                                                             cl3.setVisibility(View.INVISIBLE);
                                                             cl1.setForeground(null);
                                                             cl2.setForeground(null);
                                                             freezeGame = false;
                                                             needWhot = false;
                                                             turnCounter = playerx;
                                                             //timeX.setTextColor(Color.BLACK);
                                                             if(playerx.equals("p2")){
                                                             turner.setVisibility(View.VISIBLE);
                                                             timer.start();
                                                             } else {
                                                             turner.setVisibility(View.INVISIBLE);
                                                             timer.cancel();
                                                             }

                                                             RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                             Call<User> calltime = servicer.userTimer("Bearer "+session.getUserToken(),session.getUserID(),gameId);

                                                             calltime.enqueue(new Callback<User>() {
                                                            @Override
                                                            public void onResponse(Call<User> call, Response<User> response) {

                                                            if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                            String jres = response.body().getSuccessor();
                                                            int jnum = Integer.parseInt(jres);
                                                            int jnumsecs = jnum % 60;
                                                            int jnummins = jnum/60;
                                                            timeZ.setTextColor(Color.RED);
                                                            timeZ.setText("Delay: "+ jnummins + " mins " + jnumsecs + " secs");
                                                            //here you can have your logic to set text to edittext
                                                            } else {

                                                            }

                                                            } else {


                                                            }
                                                            }
                                                            @Override
                                                            public void onFailure(Call<User> call, Throwable t) {

                                                            Log.d("onFailure", t.toString());


                                                            }
                                                            });
                                                             }
                                                             */
                                                            if(response.body().getSuccess().getError() != null){
                                                          //      Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                            } else {
                                                          //      Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                handler.removeCallbacks(runnablew);
                                                            }
                                                        } else {
                                                            handler.removeCallbacks(runnablew);
                                                            Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                        }

                                                    } else {
                                                        if(playerTwoWhotReceive){
                                                            playerTwoWhotMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablew);
                                                        } else {
                                                            playerTwoWhotMove = false;

                                                            runnablewx = new Runnable(){
                                                                public void run(){
                                                                    //do something
                                                                    if(playerTwoWhotReceive){
                                                                        loader.setVisibility(View.INVISIBLE);
                                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                                        handler.removeCallbacks(runnablewx);
                                                                    } else {


                                                                    }

                                                                }
                                                            };

                                                            handler.postDelayed(runnablewx, delay);

                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                }
                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {
                                                    if(playerTwoWhotReceive){
                                                        playerTwoWhotMove = true;
                                                        loader.setVisibility(View.INVISIBLE);
                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                        handler.removeCallbacks(runnablew);
                                                    } else {

                                                    }
                                                    // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                }
                                            });
                                            if(playerTwoWhotReceive){
                                                playerTwoWhotMove = true;
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablew);
                                            } else {
                                                handler.postDelayed(this, delay);
                                            }

                                        }

                                    }
                                };
                                if(playerTwoWhotReceive && playerTwoWhotMove){
                                    handler.removeCallbacks(runnablew);

                                } else {
                                    handler.postDelayed(runnablew, delay);
                                }

                            }
                            // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }





            }
        });


        ImageView cardWhot1 = findViewById(R.id.sq);

        cardWhot1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                playCardSound();
                if (freezeMark) {
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeWhot){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeCard){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(freezeGame){
                    loader.setVisibility(View.VISIBLE);
                    Glide.with(FinishWhotActivity.this).load(R.drawable.loader).into(loader);
                    playerTwoWhotMove = true;
                    playerTwoWhotReceive = false;

                    freezeWhot = true;
                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                    Call<User> call = servicePlay.userWhot("Bearer " + session.getUserToken(), session.getUserID(),player1x,player2x, "sq", Integer.toString(gameId), (String) timeX.getText());

                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {

                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if(response.body().getSuccess().getError() != null){
                                  //      Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                    } else {
                                  //      Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                }

                            } else {
                                if(playerTwoWhotReceive){
                                    playerTwoWhotMove = true;
                                    loader.setVisibility(View.INVISIBLE);
                                    Glide.with(FinishWhotActivity.this).clear(loader);
                                } else {
                                    playerTwoWhotMove = false;

                                    runnablew = new Runnable(){
                                        public void run(){
                                            //do something
                                            if(playerTwoWhotReceive){
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablew);
                                            } else {
                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callz = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                                                callz.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {

                                                                if(response.body().getSuccess().getError() != null){
                                       //                             Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                                } else {
                                       //                             Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                    handler.removeCallbacks(runnablew);
                                                                }
                                                            } else {
                                                                handler.removeCallbacks(runnablew);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                            }

                                                        } else {
                                                            if(playerTwoWhotReceive){
                                                                playerTwoWhotMove = true;
                                                                loader.setVisibility(View.INVISIBLE);
                                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                                handler.removeCallbacks(runnablew);
                                                            } else {
                                                                playerTwoWhotMove = false;
                                                                handler.postDelayed(runnablew, delay);
                                                            }
                                                            // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                        }
                                                    }
                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        if(playerTwoWhotReceive){
                                                            playerTwoWhotMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablew);
                                                        } else {
                                                            playerTwoWhotMove = false;
                                                            handler.postDelayed(runnablew, delay);
                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                    }
                                                });
                                            }

                                        }
                                    };

                                    if(playerTwoWhotReceive && playerTwoWhotMove){
                                        handler.removeCallbacks(runnablew);

                                    } else {
                                        handler.postDelayed(runnablew, delay);
                                    }

                                }
                                // Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            if(playerTwoWhotReceive){
                                playerTwoWhotMove = true;
                                loader.setVisibility(View.INVISIBLE);
                                Glide.with(FinishWhotActivity.this).clear(loader);
                            } else {
                                playerTwoWhotMove = false;
                                runnablew = new Runnable(){
                                    public void run(){
                                        //do something
                                        if(playerTwoWhotReceive){
                                            loader.setVisibility(View.INVISIBLE);
                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                            handler.removeCallbacks(runnablew);
                                        } else {
                                            RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                            Call<User> callz = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                                            callz.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {
                                                            /**
                                                             String jsonResponse = response.body().getSuccessor();
                                                             if(jsonResponse.equals("success")){
                                                             commandChange = "k";
                                                             //timer.cancel();
                                                             // timer.start();
                                                             loader.setVisibility(View.INVISIBLE);
                                                             cardxi.setImageResource(R.drawable.k);
                                                             cl3.setVisibility(View.INVISIBLE);
                                                             cl1.setForeground(null);
                                                             cl2.setForeground(null);
                                                             freezeGame = false;
                                                             needWhot = false;
                                                             turnCounter = playerx;
                                                             //timeX.setTextColor(Color.BLACK);
                                                             if(playerx.equals("p2")){
                                                             turner.setVisibility(View.VISIBLE);
                                                             timer.start();
                                                             } else {
                                                             turner.setVisibility(View.INVISIBLE);
                                                             timer.cancel();
                                                             }

                                                             RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                             Call<User> calltime = servicer.userTimer("Bearer "+session.getUserToken(),session.getUserID(),gameId);

                                                             calltime.enqueue(new Callback<User>() {
                                                            @Override
                                                            public void onResponse(Call<User> call, Response<User> response) {

                                                            if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                            String jres = response.body().getSuccessor();
                                                            int jnum = Integer.parseInt(jres);
                                                            int jnumsecs = jnum % 60;
                                                            int jnummins = jnum/60;
                                                            timeZ.setTextColor(Color.RED);
                                                            timeZ.setText("Delay: "+ jnummins + " mins " + jnumsecs + " secs");
                                                            //here you can have your logic to set text to edittext
                                                            } else {

                                                            }

                                                            } else {


                                                            }
                                                            }
                                                            @Override
                                                            public void onFailure(Call<User> call, Throwable t) {

                                                            Log.d("onFailure", t.toString());


                                                            }
                                                            });
                                                             }
                                                             */
                                                            if(response.body().getSuccess().getError() != null){
                                                       //         Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                            } else {
                                                       //         Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                handler.removeCallbacks(runnablew);
                                                            }
                                                        } else {
                                                            handler.removeCallbacks(runnablew);
                                                            Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                        }

                                                    } else {
                                                        if(playerTwoWhotReceive){
                                                            playerTwoWhotMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablew);
                                                        } else {
                                                            playerTwoWhotMove = false;

                                                            runnablewx = new Runnable(){
                                                                public void run(){
                                                                    //do something
                                                                    if(playerTwoWhotReceive){
                                                                        loader.setVisibility(View.INVISIBLE);
                                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                                        handler.removeCallbacks(runnablewx);
                                                                    } else {


                                                                    }

                                                                }
                                                            };

                                                            handler.postDelayed(runnablewx, delay);

                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                }
                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {
                                                    if(playerTwoWhotReceive){
                                                        playerTwoWhotMove = true;
                                                        loader.setVisibility(View.INVISIBLE);
                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                        handler.removeCallbacks(runnablew);
                                                    } else {

                                                    }
                                                    // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                }
                                            });
                                            if(playerTwoWhotReceive){
                                                playerTwoWhotMove = true;
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablew);
                                            } else {
                                                handler.postDelayed(this, delay);
                                            }

                                        }

                                    }
                                };
                                if(playerTwoWhotReceive && playerTwoWhotMove){
                                    handler.removeCallbacks(runnablew);

                                } else {
                                    handler.postDelayed(runnablew, delay);
                                }

                            }
                            // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }


            }
        });

        ImageView cardWhot2 = findViewById(R.id.c);

        cardWhot2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                playCardSound();
                if (freezeMark) {
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeWhot){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeCard){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(freezeGame){
                    loader.setVisibility(View.VISIBLE);
                    Glide.with(FinishWhotActivity.this).load(R.drawable.loader).into(loader);
                    playerTwoWhotMove = true;
                    playerTwoWhotReceive = false;

                    freezeWhot = true;
                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                    Call<User> call = servicePlay.userWhot("Bearer " + session.getUserToken(), session.getUserID(),player1x,player2x, "c", Integer.toString(gameId), (String) timeX.getText());

                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {

                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if(response.body().getSuccess().getError() != null){
                                  //      Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                    } else {
                                   //     Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    // Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                }

                            } else {
                                if(playerTwoWhotReceive){
                                    playerTwoWhotMove = true;
                                    loader.setVisibility(View.INVISIBLE);
                                    Glide.with(FinishWhotActivity.this).clear(loader);
                                } else {
                                    playerTwoWhotMove = false;

                                    runnablew = new Runnable(){
                                        public void run(){
                                            //do something
                                            if(playerTwoWhotReceive){
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablew);
                                            } else {
                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callz = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                                                callz.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {

                                                                if(response.body().getSuccess().getError() != null){
                                                   //                 Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                                } else {
                                                  //                  Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                    handler.removeCallbacks(runnablew);
                                                                }
                                                            } else {
                                                                handler.removeCallbacks(runnablew);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                            }

                                                        } else {
                                                            if(playerTwoWhotReceive){
                                                                playerTwoWhotMove = true;
                                                                loader.setVisibility(View.INVISIBLE);
                                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                                handler.removeCallbacks(runnablew);
                                                            } else {
                                                                playerTwoWhotMove = false;
                                                                handler.postDelayed(runnablew, delay);
                                                            }
                                                            // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                        }
                                                    }
                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        if(playerTwoWhotReceive){
                                                            playerTwoWhotMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablew);
                                                        } else {
                                                            playerTwoWhotMove = false;
                                                            handler.postDelayed(runnablew, delay);
                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                    }
                                                });
                                            }

                                        }
                                    };

                                    if(playerTwoWhotReceive && playerTwoWhotMove){
                                        handler.removeCallbacks(runnablew);

                                    } else {
                                        handler.postDelayed(runnablew, delay);
                                    }

                                }
                                // Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            if(playerTwoWhotReceive){
                                playerTwoWhotMove = true;
                                loader.setVisibility(View.INVISIBLE);
                                Glide.with(FinishWhotActivity.this).clear(loader);
                            } else {
                                playerTwoWhotMove = false;
                                runnablew = new Runnable(){
                                    public void run(){
                                        //do something
                                        if(playerTwoWhotReceive){
                                            loader.setVisibility(View.INVISIBLE);
                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                            handler.removeCallbacks(runnablew);
                                        } else {
                                            RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                            Call<User> callz = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                                            callz.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {
                                                            /**
                                                             String jsonResponse = response.body().getSuccessor();
                                                             if(jsonResponse.equals("success")){
                                                             commandChange = "k";
                                                             //timer.cancel();
                                                             // timer.start();
                                                             loader.setVisibility(View.INVISIBLE);
                                                             cardxi.setImageResource(R.drawable.k);
                                                             cl3.setVisibility(View.INVISIBLE);
                                                             cl1.setForeground(null);
                                                             cl2.setForeground(null);
                                                             freezeGame = false;
                                                             needWhot = false;
                                                             turnCounter = playerx;
                                                             //timeX.setTextColor(Color.BLACK);
                                                             if(playerx.equals("p2")){
                                                             turner.setVisibility(View.VISIBLE);
                                                             timer.start();
                                                             } else {
                                                             turner.setVisibility(View.INVISIBLE);
                                                             timer.cancel();
                                                             }

                                                             RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                             Call<User> calltime = servicer.userTimer("Bearer "+session.getUserToken(),session.getUserID(),gameId);

                                                             calltime.enqueue(new Callback<User>() {
                                                            @Override
                                                            public void onResponse(Call<User> call, Response<User> response) {

                                                            if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                            String jres = response.body().getSuccessor();
                                                            int jnum = Integer.parseInt(jres);
                                                            int jnumsecs = jnum % 60;
                                                            int jnummins = jnum/60;
                                                            timeZ.setTextColor(Color.RED);
                                                            timeZ.setText("Delay: "+ jnummins + " mins " + jnumsecs + " secs");
                                                            //here you can have your logic to set text to edittext
                                                            } else {

                                                            }

                                                            } else {


                                                            }
                                                            }
                                                            @Override
                                                            public void onFailure(Call<User> call, Throwable t) {

                                                            Log.d("onFailure", t.toString());


                                                            }
                                                            });
                                                             }
                                                             */
                                                            if(response.body().getSuccess().getError() != null){
                                                      //          Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                            } else {
                                                     //           Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                handler.removeCallbacks(runnablew);
                                                            }
                                                        } else {
                                                            handler.removeCallbacks(runnablew);
                                                            Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                        }

                                                    } else {
                                                        if(playerTwoWhotReceive){
                                                            playerTwoWhotMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablew);
                                                        } else {
                                                            playerTwoWhotMove = false;

                                                            runnablewx = new Runnable(){
                                                                public void run(){
                                                                    //do something
                                                                    if(playerTwoWhotReceive){
                                                                        loader.setVisibility(View.INVISIBLE);
                                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                                        handler.removeCallbacks(runnablewx);
                                                                    } else {


                                                                    }

                                                                }
                                                            };

                                                            handler.postDelayed(runnablewx, delay);

                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                }
                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {
                                                    if(playerTwoWhotReceive){
                                                        playerTwoWhotMove = true;
                                                        loader.setVisibility(View.INVISIBLE);
                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                        handler.removeCallbacks(runnablew);
                                                    } else {

                                                    }
                                                    // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                }
                                            });
                                            if(playerTwoWhotReceive){
                                                playerTwoWhotMove = true;
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablew);
                                            } else {
                                                handler.postDelayed(this, delay);
                                            }

                                        }

                                    }
                                };
                                if(playerTwoWhotReceive && playerTwoWhotMove){
                                    handler.removeCallbacks(runnablew);

                                } else {
                                    handler.postDelayed(runnablew, delay);
                                }

                            }
                            // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }



            }
        });


        ImageView cardWhot3 = findViewById(R.id.st);

        cardWhot3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                playCardSound();
                if (freezeMark) {
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeWhot){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeCard){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(freezeGame){
                    loader.setVisibility(View.VISIBLE);
                    Glide.with(FinishWhotActivity.this).load(R.drawable.loader).into(loader);
                    playerTwoWhotMove = true;
                    playerTwoWhotReceive = false;

                    freezeWhot = true;
                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                    Call<User> call = servicePlay.userWhot("Bearer " + session.getUserToken(), session.getUserID(),player1x,player2x, "st", Integer.toString(gameId), (String) timeX.getText());

                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {

                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if(response.body().getSuccess().getError() != null){
                                 //       Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                    } else {
                                //        Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                }

                            } else {
                                if(playerTwoWhotReceive){
                                    playerTwoWhotMove = true;
                                    loader.setVisibility(View.INVISIBLE);
                                    Glide.with(FinishWhotActivity.this).clear(loader);
                                } else {
                                    playerTwoWhotMove = false;

                                    runnablew = new Runnable(){
                                        public void run(){
                                            //do something
                                            if(playerTwoWhotReceive){
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablew);
                                            } else {
                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callz = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                                                callz.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {

                                                                if(response.body().getSuccess().getError() != null){
                                                 //                   Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                                } else {
                                                 //                   Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                    handler.removeCallbacks(runnablew);
                                                                }
                                                            } else {
                                                                handler.removeCallbacks(runnablew);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                            }

                                                        } else {
                                                            if(playerTwoWhotReceive){
                                                                playerTwoWhotMove = true;
                                                                loader.setVisibility(View.INVISIBLE);
                                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                                handler.removeCallbacks(runnablew);
                                                            } else {
                                                                playerTwoWhotMove = false;
                                                                handler.postDelayed(runnablew, delay);
                                                            }
                                                            // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                        }
                                                    }
                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        if(playerTwoWhotReceive){
                                                            playerTwoWhotMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablew);
                                                        } else {
                                                            playerTwoWhotMove = false;
                                                            handler.postDelayed(runnablew, delay);
                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                    }
                                                });
                                            }

                                        }
                                    };

                                    if(playerTwoWhotReceive && playerTwoWhotMove){
                                        handler.removeCallbacks(runnablew);

                                    } else {
                                        handler.postDelayed(runnablew, delay);
                                    }

                                }
                                // Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            if(playerTwoWhotReceive){
                                playerTwoWhotMove = true;
                                loader.setVisibility(View.INVISIBLE);
                                Glide.with(FinishWhotActivity.this).clear(loader);
                            } else {
                                playerTwoWhotMove = false;
                                runnablew = new Runnable(){
                                    public void run(){
                                        //do something
                                        if(playerTwoWhotReceive){
                                            loader.setVisibility(View.INVISIBLE);
                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                            handler.removeCallbacks(runnablew);
                                        } else {
                                            RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                            Call<User> callz = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                                            callz.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {
                                                            /**
                                                             String jsonResponse = response.body().getSuccessor();
                                                             if(jsonResponse.equals("success")){
                                                             commandChange = "k";
                                                             //timer.cancel();
                                                             // timer.start();
                                                             loader.setVisibility(View.INVISIBLE);
                                                             cardxi.setImageResource(R.drawable.k);
                                                             cl3.setVisibility(View.INVISIBLE);
                                                             cl1.setForeground(null);
                                                             cl2.setForeground(null);
                                                             freezeGame = false;
                                                             needWhot = false;
                                                             turnCounter = playerx;
                                                             //timeX.setTextColor(Color.BLACK);
                                                             if(playerx.equals("p2")){
                                                             turner.setVisibility(View.VISIBLE);
                                                             timer.start();
                                                             } else {
                                                             turner.setVisibility(View.INVISIBLE);
                                                             timer.cancel();
                                                             }

                                                             RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                             Call<User> calltime = servicer.userTimer("Bearer "+session.getUserToken(),session.getUserID(),gameId);

                                                             calltime.enqueue(new Callback<User>() {
                                                            @Override
                                                            public void onResponse(Call<User> call, Response<User> response) {

                                                            if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                            String jres = response.body().getSuccessor();
                                                            int jnum = Integer.parseInt(jres);
                                                            int jnumsecs = jnum % 60;
                                                            int jnummins = jnum/60;
                                                            timeZ.setTextColor(Color.RED);
                                                            timeZ.setText("Delay: "+ jnummins + " mins " + jnumsecs + " secs");
                                                            //here you can have your logic to set text to edittext
                                                            } else {

                                                            }

                                                            } else {


                                                            }
                                                            }
                                                            @Override
                                                            public void onFailure(Call<User> call, Throwable t) {

                                                            Log.d("onFailure", t.toString());


                                                            }
                                                            });
                                                             }
                                                             */
                                                            if(response.body().getSuccess().getError() != null){
                                                      //          Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                            } else {
                                                      //          Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                handler.removeCallbacks(runnablew);
                                                            }
                                                        } else {
                                                            handler.removeCallbacks(runnablew);
                                                            Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                        }

                                                    } else {
                                                        if(playerTwoWhotReceive){
                                                            playerTwoWhotMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablew);
                                                        } else {
                                                            playerTwoWhotMove = false;

                                                            runnablewx = new Runnable(){
                                                                public void run(){
                                                                    //do something
                                                                    if(playerTwoWhotReceive){
                                                                        loader.setVisibility(View.INVISIBLE);
                                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                                        handler.removeCallbacks(runnablewx);
                                                                    } else {


                                                                    }

                                                                }
                                                            };

                                                            handler.postDelayed(runnablewx, delay);

                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                }
                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {
                                                    if(playerTwoWhotReceive){
                                                        playerTwoWhotMove = true;
                                                        loader.setVisibility(View.INVISIBLE);
                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                        handler.removeCallbacks(runnablew);
                                                    } else {

                                                    }
                                                    // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                }
                                            });
                                            if(playerTwoWhotReceive){
                                                playerTwoWhotMove = true;
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablew);
                                            } else {
                                                handler.postDelayed(this, delay);
                                            }

                                        }

                                    }
                                };
                                if(playerTwoWhotReceive && playerTwoWhotMove){
                                    handler.removeCallbacks(runnablew);

                                } else {
                                    handler.postDelayed(runnablew, delay);
                                }

                            }
                            // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }



            }
        });


        ImageView cardWhot4 = findViewById(R.id.t);

        cardWhot4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                playCardSound();
                if (freezeMark) {
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeWhot){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (freezeCard){
                    Toast.makeText(FinishWhotActivity.this, "Please Wait!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(freezeGame){
                    loader.setVisibility(View.VISIBLE);
                    Glide.with(FinishWhotActivity.this).load(R.drawable.loader).into(loader);
                    playerTwoWhotMove = true;
                    playerTwoWhotReceive = false;

                    freezeWhot = true;
                    RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                    Call<User> call = servicePlay.userWhot("Bearer " + session.getUserToken(), session.getUserID(),player1x,player2x, "t", Integer.toString(gameId), (String) timeX.getText());

                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {

                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if(response.body().getSuccess().getError() != null){
                                 //       Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                    } else {
                                 //       Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                     Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                }

                            } else {
                                if(playerTwoWhotReceive){
                                    playerTwoWhotMove = true;
                                    loader.setVisibility(View.INVISIBLE);
                                    Glide.with(FinishWhotActivity.this).clear(loader);
                                } else {
                                    playerTwoWhotMove = false;

                                    runnablew = new Runnable(){
                                        public void run(){
                                            //do something
                                            if(playerTwoWhotReceive){
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablew);
                                            } else {
                                                RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                                Call<User> callz = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                                                callz.enqueue(new Callback<User>() {
                                                    @Override
                                                    public void onResponse(Call<User> call, Response<User> response) {

                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {

                                                                if(response.body().getSuccess().getError() != null){
                                                    //                Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                                } else {
                                                   //                 Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                    handler.removeCallbacks(runnablew);
                                                                }
                                                            } else {
                                                                handler.removeCallbacks(runnablew);
                                                                Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                            }

                                                        } else {
                                                            if(playerTwoWhotReceive){
                                                                playerTwoWhotMove = true;
                                                                loader.setVisibility(View.INVISIBLE);
                                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                                handler.removeCallbacks(runnablew);
                                                            } else {
                                                                playerTwoWhotMove = false;
                                                                handler.postDelayed(runnablew, delay);
                                                            }
                                                            // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                        }
                                                    }
                                                    @Override
                                                    public void onFailure(Call<User> call, Throwable t) {
                                                        if(playerTwoWhotReceive){
                                                            playerTwoWhotMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablew);
                                                        } else {
                                                            playerTwoWhotMove = false;
                                                            handler.postDelayed(runnablew, delay);
                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                    }
                                                });
                                            }

                                        }
                                    };

                                    if(playerTwoWhotReceive && playerTwoWhotMove){
                                        handler.removeCallbacks(runnablew);

                                    } else {
                                        handler.postDelayed(runnablew, delay);
                                    }

                                }
                                // Toast.makeText(FinishWhotActivity.this, "Game Move Error!" + response.toString(), Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            if(playerTwoWhotReceive){
                                playerTwoWhotMove = true;
                                loader.setVisibility(View.INVISIBLE);
                                Glide.with(FinishWhotActivity.this).clear(loader);
                            } else {
                                playerTwoWhotMove = false;
                                runnablew = new Runnable(){
                                    public void run(){
                                        //do something
                                        if(playerTwoWhotReceive){
                                            loader.setVisibility(View.INVISIBLE);
                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                            handler.removeCallbacks(runnablew);
                                        } else {
                                            RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                            Call<User> callz = servicePlay.userWhot("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,"k",Integer.toString(gameId),(String)timeX.getText());

                                            callz.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {
                                                            /**
                                                             String jsonResponse = response.body().getSuccessor();
                                                             if(jsonResponse.equals("success")){
                                                             commandChange = "k";
                                                             //timer.cancel();
                                                             // timer.start();
                                                             loader.setVisibility(View.INVISIBLE);
                                                             cardxi.setImageResource(R.drawable.k);
                                                             cl3.setVisibility(View.INVISIBLE);
                                                             cl1.setForeground(null);
                                                             cl2.setForeground(null);
                                                             freezeGame = false;
                                                             needWhot = false;
                                                             turnCounter = playerx;
                                                             //timeX.setTextColor(Color.BLACK);
                                                             if(playerx.equals("p2")){
                                                             turner.setVisibility(View.VISIBLE);
                                                             timer.start();
                                                             } else {
                                                             turner.setVisibility(View.INVISIBLE);
                                                             timer.cancel();
                                                             }

                                                             RestService servicer = RestClient.getClient(session).create(RestService.class);
                                                             Call<User> calltime = servicer.userTimer("Bearer "+session.getUserToken(),session.getUserID(),gameId);

                                                             calltime.enqueue(new Callback<User>() {
                                                            @Override
                                                            public void onResponse(Call<User> call, Response<User> response) {

                                                            if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                            String jres = response.body().getSuccessor();
                                                            int jnum = Integer.parseInt(jres);
                                                            int jnumsecs = jnum % 60;
                                                            int jnummins = jnum/60;
                                                            timeZ.setTextColor(Color.RED);
                                                            timeZ.setText("Delay: "+ jnummins + " mins " + jnumsecs + " secs");
                                                            //here you can have your logic to set text to edittext
                                                            } else {

                                                            }

                                                            } else {


                                                            }
                                                            }
                                                            @Override
                                                            public void onFailure(Call<User> call, Throwable t) {

                                                            Log.d("onFailure", t.toString());


                                                            }
                                                            });
                                                             }
                                                             */
                                                            if(response.body().getSuccess().getError() != null){
                                                      //          Toast.makeText(FinishWhotActivity.this, "Error: "+response.body().getSuccess().getError(), Toast.LENGTH_SHORT).show();
                                                            } else {
                                                       //         Toast.makeText(FinishWhotActivity.this, "Success: "+response.body().getSuccess().getNota(), Toast.LENGTH_SHORT).show();
                                                                handler.removeCallbacks(runnablew);
                                                            }
                                                        } else {
                                                            handler.removeCallbacks(runnablew);
                                                            Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                        }

                                                    } else {
                                                        if(playerTwoWhotReceive){
                                                            playerTwoWhotMove = true;
                                                            loader.setVisibility(View.INVISIBLE);
                                                            Glide.with(FinishWhotActivity.this).clear(loader);
                                                            handler.removeCallbacks(runnablew);
                                                        } else {
                                                            playerTwoWhotMove = false;

                                                            runnablewx = new Runnable(){
                                                                public void run(){
                                                                    //do something
                                                                    if(playerTwoWhotReceive){
                                                                        loader.setVisibility(View.INVISIBLE);
                                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                                        handler.removeCallbacks(runnablewx);
                                                                    } else {


                                                                    }

                                                                }
                                                            };

                                                            handler.postDelayed(runnablewx, delay);

                                                        }
                                                        // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                }
                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {
                                                    if(playerTwoWhotReceive){
                                                        playerTwoWhotMove = true;
                                                        loader.setVisibility(View.INVISIBLE);
                                                        Glide.with(FinishWhotActivity.this).clear(loader);
                                                        handler.removeCallbacks(runnablew);
                                                    } else {

                                                    }
                                                    // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                }
                                            });
                                            if(playerTwoWhotReceive){
                                                playerTwoWhotMove = true;
                                                loader.setVisibility(View.INVISIBLE);
                                                Glide.with(FinishWhotActivity.this).clear(loader);
                                                handler.removeCallbacks(runnablew);
                                            } else {
                                                handler.postDelayed(this, delay);
                                            }

                                        }

                                    }
                                };
                                if(playerTwoWhotReceive && playerTwoWhotMove){
                                    handler.removeCallbacks(runnablew);

                                } else {
                                    handler.postDelayed(runnablew, delay);
                                }

                            }
                            // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }

            }
        });

    }


    public void playOne(String cardp1x,String marktDckSze,Success success,String jsonUpdaterx) {

        if(success.getUuid().equals(session.getUserID()) || success.getNoti().equals("whotter") || success.getNoti().equals("marketer")){


        } else {

            int item = slotView1.getChildCount();
            ImageView slotty = (ImageView) slotView1.getChildAt(item - 1);
            Handler handler = new Handler();
            ImageView slotti = (ImageView) slotView2.getChildAt(slotView2.getChildCount() - 1);
            String boardCard;
            String[] boardTotal;
            String borderPart;
            int bordNum;
            String[] carter = cardp1x.split("z");
            String cardNamePart = carter[0];
            int cardNameNum = Integer.parseInt(carter[1]);
            marketDeckSize = marktDckSze;
            if (commandChange.equals("none")) {
                boardCard = cardx.get(cardx.size() - 1);
                boardTotal = boardCard.split("z");
                borderPart = boardTotal[0];
                bordNum = Integer.parseInt(boardTotal[1]);
            } else {
                borderPart = commandChange;
                bordNum = 100;

            }

            Log.d("Centre[0]P1: ", borderPart);
            Log.d("Centre[1]P1: ", "" + bordNum);

            if (freezeGame && cardp1.size() == 0) return;

            if (turnCounter.equals("p1")) {
                playCardSound();
                holdOn = "none";
                if (cardRuler(borderPart, bordNum, cardNamePart, cardNameNum, "p1", "p2", slotti)) {
                    // timer.cancel();
                    if (cardNameNum == 2) {
                        setPickTwo(success.getNotu(), success.getNoty(), "p1", "p2", slotti);
                    } else if (cardNameNum == 14) {
                        setGeneralMarket(success.getNoto(), "p1", "p2", slotti);
                    }

                    JSONObject playXbj = null;
                    try {
                        playXbj = new JSONObject(success.getNoth());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Iterator itr = playXbj.keys();
                    while(itr.hasNext()){
                        String key = (String)itr.next();
                        cardxy.add(playXbj.optString(key));
                        Log.d("Keys", "----"+key+": "+playXbj.optString(key));
                    }


                    playedLast = "p1";
                    cardNum.setText("[" + marketDeckSize + "]");
                    commandChange = "none";
                    cardx.add(cardp1x);
                    cardMover(cardp1, cardp1.size() - 1);

                    // Toast.makeText(this, "playOne-Card: "+cardp1x, Toast.LENGTH_SHORT).show();

                    animCardx(100, slotty, handler);
                    slotView1.removeViewAt(item - 1);
                  //  if(success.getNotx() == cardp1.size()){

                  //  } else {
                      //  for(int i = 0;i < (success.getNotx() - cardp1.size());i++){
                      //      int itemx = slotView1.getChildCount();
                      //      slotView1.removeViewAt(itemx - 1);
                      //  }

                 //   }

                 //   cardx.clear();
                 //   cardx = new ArrayList<String>(cardxy);

                    int delay = 5000;
                    String[] checkWordArr = cardp1x.split("z");
                    if (checkWordArr[0].equals("w") || checkWordArr[1].equals("1") || checkWordArr[1].equals("2") || checkWordArr[1].equals("14")) {
                      //  RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                      //  Call<User> callr = servicePlay.userCont("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,Integer.toString(gameId));

                       /** callr.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {

                                if (response.isSuccessful()) {
                                    if (response.body() != null) {

                                        handler.removeCallbacks(runnableo);

                                    } else {
                                        handler.removeCallbacks(runnableo);
                                        Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();

                                    }

                                } else {


                                    runnableo = new Runnable(){
                                        public void run(){
                                            //do something

                                            RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                            Call<User> callz = servicePlay.userCont("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,Integer.toString(gameId));

                                            callz.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {

                                                            handler.removeCallbacks(runnableo);
                                                        } else {
                                                            handler.removeCallbacks(runnableo);
                                                            Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                        }

                                                    } else {
                                                        handler.postDelayed(runnableo, delay);
                                                        // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                    }
                                                }
                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {
                                                    handler.postDelayed(runnableo, delay);
                                                    // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                                }
                                            });


                                        }
                                    };

                                    handler.postDelayed(runnableo, delay);


                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                runnableo = new Runnable(){
                                    public void run(){
                                        //do something

                                        RestService servicePlay = RestClient.getClient(session).create(RestService.class);
                                        Call<User> callz = servicePlay.userCont("Bearer "+session.getUserToken(),session.getUserID(),player1x,player2x,Integer.toString(gameId));

                                        callz.enqueue(new Callback<User>() {
                                            @Override
                                            public void onResponse(Call<User> call, Response<User> response) {

                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {

                                                        handler.removeCallbacks(runnableo);
                                                    } else {
                                                        handler.removeCallbacks(runnableo);
                                                        Toast.makeText(FinishWhotActivity.this, "Empty Response!", Toast.LENGTH_SHORT).show();
                                                    }

                                                } else {
                                                    handler.postDelayed(runnableo, delay);
                                                    // Toast.makeText(FinishWhotActivity.this, "Game Move Error!"+response.toString(), Toast.LENGTH_SHORT).show();

                                                }
                                            }
                                            @Override
                                            public void onFailure(Call<User> call, Throwable t) {
                                                handler.postDelayed(runnableo, delay);
                                                // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();


                                            }
                                        });


                                    }
                                };

                                handler.postDelayed(runnableo, delay);
                                // Toast.makeText(FinishWhotActivity.this, "Failure Error!"+t.toString(), Toast.LENGTH_SHORT).show();

                            }
                        }); **/


                    }

                    float density = getApplicationContext().getResources().getDisplayMetrics().density;
                    int dx = (int) ((2 * density) + 2);
                    slotty.setElevation(dx);
                    int src = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardp1x, null, null);
                    slotty.setImageResource(src);
                    cardxi.setImageResource(src);
                    if (success.getNoti().equals("0")) {
                        if (checkWinner(cardp1.size(), "p1")) {
                            gameSound("stop");
                            soundOn = false;
                        }

                    }

                    session.setMoveState(session.getUserID());


                } else {
                    if (gameOver) {
                        Toast.makeText(FinishWhotActivity.this, "Game Over!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FinishWhotActivity.this, "Wrong Move", Toast.LENGTH_SHORT).show();
                    }

                }
            } else {
                if (gameOver) {
                    Toast.makeText(FinishWhotActivity.this, "Game Over!", Toast.LENGTH_SHORT).show();
                }
            }

        }

    }



    public void gameSound(String action){
        if(action.equals("play")){
            try {
                mediaPlayerx = MediaPlayer.create(FinishWhotActivity.this, R.raw.whotter);
                mediaPlayerx.setLooping(true);
                mediaPlayerx.start();
            } catch (Exception e){
                e.printStackTrace();
            }

        } else {
            try{
                if(mediaPlayerx != null){
                    mediaPlayerx.stop();
                    mediaPlayerx.reset();
                    mediaPlayerx.release();
                }
            } catch (Exception e){
                e.printStackTrace();
            }

        }

    }


    public void playWinSound(){
        mediaPlayer = MediaPlayer.create(FinishWhotActivity.this, R.raw.applause);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }

    public void playLoseSound(){
        mediaPlayer = MediaPlayer.create(FinishWhotActivity.this, R.raw.failtrombone);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }

    public void playCardSound(){
        mediaPlayer = MediaPlayer.create(FinishWhotActivity.this, R.raw.cardtap);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }


    public void playFailSound(){
        mediaPlayer = MediaPlayer.create(FinishWhotActivity.this, R.raw.failbuzzer);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }

    public void initGame(final Handler handler) {

        /**
        for(String cartus: cardp1){
            creatorx(R.drawable.backofcard,cartus);
        }

         **/

            String cartus = cardp1.get(0);
            creatorx(R.drawable.backofcard,cartus);

            String cartus1 = cardp1.get(1);
            creatorx(R.drawable.backofcard,cartus1);

            String cartus2 = cardp1.get(2);
            creatorx(R.drawable.backofcard,cartus2);

            String cartus3 = cardp1.get(3);
            creatorx(R.drawable.backofcard,cartus3);

            String cartus4 = cardp1.get(4);
            creatorx(R.drawable.backofcard,cartus4);


            /**
        for(String cartusa: cardp2){
            int srca = getResources().getIdentifier("com.godwin.betgames:drawable/" + cartusa, null, null);
            creator(srca,cartusa);
        }
             **/

            String cartusa = cardp2.get(0);
            int srca = getResources().getIdentifier("com.godwin.betgames:drawable/" + cartusa, null, null);
            creator(srca,cartusa);

            String cartusb = cardp2.get(1);
            int srcb = getResources().getIdentifier("com.godwin.betgames:drawable/" + cartusb, null, null);
            creator(srcb,cartusb);

            String cartusc = cardp2.get(2);
            int srcc = getResources().getIdentifier("com.godwin.betgames:drawable/" + cartusc, null, null);
            creator(srcc,cartusc);

            String cartusd = cardp2.get(3);
            int srcd = getResources().getIdentifier("com.godwin.betgames:drawable/" + cartusd, null, null);
            creator(srcd,cartusd);

            String cartuse = cardp2.get(4);
            int srce = getResources().getIdentifier("com.godwin.betgames:drawable/" + cartuse, null, null);
            creator(srce,cartuse);




            String cartusx = cardx.get(cardx.size() - 1);
            cardxi.setVisibility(View.VISIBLE);
            int srcx = getResources().getIdentifier("com.godwin.betgames:drawable/" + cartusx, null, null);
            cardxi.setImageResource(srcx);
            cardxi.setContentDescription(cartusx);
            ImageView slotte = (ImageView) slotView1.getChildAt(slotView1.getChildCount() - 1);
            ImageView slottu = (ImageView) slotView2.getChildAt(slotView2.getChildCount() - 1);
            if(cartusx.startsWith("w")){
                //Show UI to select next play card
                if(player.equals(player1x)) {
                    creatorWhotter("p1");
                }

            } else {
                String[] carter = cartusx.split("z");
                String bordPart = carter[0];
                int bordNum = Integer.parseInt(carter[1]);
                if (bordNum == 2) {
                    if(player.equals(player1x)) {
                        pickTwo = "p1";
                        String cd3 = "jz"+cardp1.size();
                        String cd3i = "jz"+cardp1.size()+1;
                        cardp1.add(cd3);
                        cardp1.add(cd3i);
                        cardName = cd3;
                        cardNameN = cd3i;
                        String[] cdx3 = cardName.split("z");
                        String[] cdx3i = cardNameN.split("z");
                        cardNamePart = cdx3[0];
                        cardNamePartN = cdx3i[0];
                        try {
                            cardNameNum = Integer.parseInt(cdx3[1]);
                        }
                        catch (NumberFormatException e)
                        {
                            cardNameNum = Integer.valueOf(cdx3[1]);
                        }

                        try {
                            cardNameNumN = Integer.parseInt(cdx3i[1]);
                        }
                        catch (NumberFormatException e)
                        {
                            cardNameNumN = Integer.valueOf(cdx3i[1]);
                        }
                        ImageView primer = creatorx(R.drawable.backofcard,cardName);
                        creatorx(R.drawable.backofcard,cardNameN);
                        pickTwo = "none";
                        turnCounter = "p2";
                    } else {
                        pickTwo = "p2";
                        String cd3 = cardp2.get(5);
                        String cd3i = cardp2.get(6);
                        cardp1.add(cd3);
                        cardp1.add(cd3i);
                        cardName = cd3;
                        cardNameN = cd3i;
                        String[] cdx3 = cardName.split("z");
                        String[] cdx3i = cardNameN.split("z");
                        cardNamePart = cdx3[0];
                        cardNamePartN = cdx3i[0];
                        try {
                            cardNameNum = Integer.parseInt(cdx3[1]);
                        }
                        catch (NumberFormatException e)
                        {
                            cardNameNum = Integer.valueOf(cdx3[1]);
                        }

                        try {
                            cardNameNumN = Integer.parseInt(cdx3i[1]);
                        }
                        catch (NumberFormatException e)
                        {
                            cardNameNumN = Integer.valueOf(cdx3i[1]);
                        }
                        int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardName, null, null);
                        int srcImagen = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardNameN, null, null);
                        ImageView prima = creator(srcImage,cardName);
                        creator(srcImagen,cardNameN);
                        pickTwo = "none";
                        turnCounter = "p1";
                    }

                    Toast.makeText(FinishWhotActivity.this, "Pick Two", Toast.LENGTH_SHORT).show();
                } else if (bordNum == 14) {
                    if(player.equals(player1x)) {
                        generalMarket = "p1";
                        String cd1 = "jz"+cardp1.size();
                        cardp1.add(cd1);
                        cardName = cd1;
                        String[] cdx1 = cardName.split("z");
                        cardNamePart = cdx1[0];
                        try {
                            cardNameNum = Integer.parseInt(cdx1[1]);
                        }
                        catch (NumberFormatException e)
                        {
                            cardNameNum = Integer.valueOf(cdx1[1]);
                        }
                        creatorx(R.drawable.backofcard,cardName);
                        generalMarket = "none";
                        turnCounter = "p2";
                    } else {
                        generalMarket = "p2";
                        String cd1 = cardp2.get(5);
                        cardp1.add(cd1);
                        cardName = cd1;
                        String[] cdx1 = cardName.split("z");
                        cardNamePart = cdx1[0];
                        try {
                            cardNameNum = Integer.parseInt(cdx1[1]);
                        }
                        catch (NumberFormatException e)
                        {
                            cardNameNum = Integer.valueOf(cdx1[1]);
                        }
                        int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardName, null, null);
                        creator(srcImage,cardName);
                        generalMarket = "none";
                        turnCounter = "p1";
                    }

                    Toast.makeText(FinishWhotActivity.this, "General Market", Toast.LENGTH_SHORT).show();
                } else if (bordNum == 1) {
                    if(player.equals(player1x)) {
                        holdOn = "p1";
                        turnCounter = "p2";
                    } else {
                        holdOn = "p2";
                        turnCounter = "p1";
                    }

                } else {
                    //Do Nothing.
                }

            }
            if(turnCounter.equals("p2")){
                turner.setVisibility(View.VISIBLE);
                timer.start();
            }
            else {
                turner.setVisibility(View.INVISIBLE);
                timer.cancel();
            }




    }


    public void contGame(ArrayList<String> cardOne,ArrayList<String> cardTwo, ArrayList<String> cardDeck) {

        for(String item: cardOne){
            creatorx(R.drawable.backofcard,item);
        }

        for(String item: cardTwo){
            int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + item, null, null);
            creator(srcImage,item);
        }

        String cartus = cardDeck.get(cardDeck.size() - 1);
        cardxi.setVisibility(View.VISIBLE);
        int src = getResources().getIdentifier("com.godwin.betgames:drawable/" + cartus, null, null);
        cardxi.setImageResource(src);
        cardxi.setContentDescription(cartus);


    }


    public String cardMover(ArrayList<String> decker,int index) {
        String item = decker.get(index);
        decker.remove(index);
        return item;
    }


    public void finishAndCount() {

        RestService servicer = RestClient.getClient(session).create(RestService.class);
        Call<User> callfin = servicer.userFinish("Bearer "+session.getUserToken(),session.getUserID(), player1x, player2x,Integer.toString(gameId));

        callfin.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if (response.isSuccessful()) {
                    if (response.body() != null) {

                        final ArrayList<String> cardDeck1 = response.body().getSuccess().getDecker();
                        String wina = response.body().getSuccess().getWina();
                        String losa = response.body().getSuccess().getLosa();


                        if(session.getUserID().equals(losa)){

                            winner = "p1";
                            dialoger = true;

                            exitButton.setVisibility(View.VISIBLE);
                            exitButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    RestService servicer = RestClient.getClient(session).create(RestService.class);
                                    Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                    call.enqueue(new Callback<User>() {
                                        @Override
                                        public void onResponse(Call<User> call, Response<User> response) {

                                            if (response.isSuccessful()) {
                                                if (response.body() != null) {

                                                } else {

                                                }

                                            } else {


                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<User> call, Throwable t) {

                                        }
                                    });

                                    Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                    intentOne.putExtra("stopper","false");
                                    startActivity(intentOne);
                                    FinishWhotActivity.this.finish();
                                }
                            });

                            replayButton.setVisibility(View.VISIBLE);
                            replayButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {

                                    showpDialog();



                                    Intent intentR = new Intent( FinishWhotActivity.this, MainActivity.class);
                                    intentR.putExtra("stopper", "false");
                                    intentR.putExtra("usernome", session.getUserID());
                                    if(session.getUserID().equals(player1x)){
                                        intentR.putExtra("userchaller", player2x);
                                    } else {
                                        intentR.putExtra("userchaller", player1x);
                                    }
                                    intentR.putExtra("betGame", "Finish&Count");
                                    intentR.putExtra("bet_id", bet_id); Log.d("BET_ID","Msg: "+bet_id);
                                    intentR.putExtra("game_id", Integer.toString(gameId));

                                    startActivity(intentR);
                                    hidepDialog();
                                    FinishWhotActivity.this.finish();
                                }
                            });

                            AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                            dialog.setCancelable(false);
                            dialog.setTitle("You Lost!");
                            dialog.setMessage("Sorry, you're not lucky this time." );
                            dialog.setNegativeButton("View Cards", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    slotView1.removeAllViews();
                                    for(String i: cardDeck1){

                                        ImageView slotter = (ImageView) slotView1.getChildAt(slotView1.getChildCount() - 1);
                                        int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + i, null, null);
                                        creatory(srcImage,i,slotter);

                                    }


                                }
                            });
                            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    exitButton.setVisibility(View.VISIBLE);
                                    exitButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            RestService servicer = RestClient.getClient(session).create(RestService.class);
                                            Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                            call.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {

                                                        } else {

                                                        }

                                                    } else {


                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {

                                                }
                                            });
                                            session.setGameState(null);betgamedb.deleter("game_state");

                                            session.setChallengeState(null);

                                            session.setNotaRec(null);
                                            session.setNoteRec(null);
                                            session.setNotiRec(null);
                                            session.setNotoRec(null);
                                            session.setNotuRec(null);
                                            session.setNotyRec(null);
                                            session.setWinaRec(null);
                                            session.setLosaRec(null);
                                            session.setPlay("false");
                                            session.setReqNota(null);
                                            session.setUserBeta(null);
                                            session.setUserAcc(null);
                                            session.setBetId(null);

                                            Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                            intentOne.putExtra("stopper", "false");
                                            startActivity(intentOne);
                                            FinishWhotActivity.this.finish();
                                        }
                                    });
                                }
                            });

                            final AlertDialog alert = dialog.create();

                            alert.show();
                            gameSound("stop");
                            soundOn = false;
                            playLoseSound();
                            gameOver = true;
                            session.setGameState(null);betgamedb.deleter("game_state");
                            session.setEndState(gameId);
                            session.setChallengeState(null);
                            session.setAppState(null);
                            // timer.cancel();
                            session.setBetId(null);


                        } else {

                            winner = "p2";
                            dialoger = true;

                            exitButton.setVisibility(View.VISIBLE);
                            exitButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    RestService servicer = RestClient.getClient(session).create(RestService.class);
                                    Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                    call.enqueue(new Callback<User>() {
                                        @Override
                                        public void onResponse(Call<User> call, Response<User> response) {

                                            if (response.isSuccessful()) {
                                                if (response.body() != null) {

                                                } else {

                                                }

                                            } else {


                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<User> call, Throwable t) {

                                        }
                                    });

                                    Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                    intentOne.putExtra("stopper","false");
                                    startActivity(intentOne);
                                    FinishWhotActivity.this.finish();
                                }
                            });

                            replayButton.setVisibility(View.VISIBLE);
                            replayButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {

                                    showpDialog();



                                    Intent intentR = new Intent( FinishWhotActivity.this, MainActivity.class);
                                    intentR.putExtra("stopper", "false");
                                    intentR.putExtra("usernome", session.getUserID());
                                    if(session.getUserID().equals(player1x)){
                                        intentR.putExtra("userchaller", player2x);
                                    } else {
                                        intentR.putExtra("userchaller", player1x);
                                    }
                                    intentR.putExtra("betGame", "Finish&Count");
                                    intentR.putExtra("bet_id", bet_id); Log.d("BET_ID","Msg: "+bet_id);
                                    intentR.putExtra("game_id", Integer.toString(gameId));

                                    startActivity(intentR);
                                    hidepDialog();
                                    FinishWhotActivity.this.finish();
                                }
                            });

                            AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                            dialog.setCancelable(false);
                            dialog.setTitle("You Won!");
                            dialog.setMessage("Congratulations!" );
                            dialog.setNegativeButton("View Cards", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    slotView1.removeAllViews();
                                    for(String i: cardDeck1){

                                        ImageView slotter = (ImageView) slotView1.getChildAt(slotView1.getChildCount() - 1);
                                        int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + i, null, null);
                                        creatory(srcImage,i,slotter);

                                    }

                                }
                            });
                            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    exitButton.setVisibility(View.VISIBLE);
                                    exitButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            RestService servicer = RestClient.getClient(session).create(RestService.class);
                                            Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                            call.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {

                                                        } else {

                                                        }

                                                    } else {


                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {

                                                }
                                            });
                                            session.setGameState(null);betgamedb.deleter("game_state");

                                            session.setChallengeState(null);

                                            session.setNotaRec(null);
                                            session.setNoteRec(null);
                                            session.setNotiRec(null);
                                            session.setNotoRec(null);
                                            session.setNotuRec(null);
                                            session.setNotyRec(null);
                                            session.setWinaRec(null);
                                            session.setLosaRec(null);
                                            session.setPlay("false");
                                            session.setReqNota(null);
                                            session.setUserBeta(null);
                                            session.setUserAcc(null);
                                            session.setBetId(null);

                                            Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                            intentOne.putExtra("stopper", "false");
                                            startActivity(intentOne);
                                            FinishWhotActivity.this.finish();
                                        }
                                    });
                                }
                            });

                            final AlertDialog alert = dialog.create();

                            alert.show();
                            gameSound("stop");
                            soundOn = false;
                            playWinSound();
                            gameOver = true;
                            session.setGameState(null);betgamedb.deleter("game_state");
                            session.setEndState(gameId);
                            session.setChallengeState(null);
                            session.setAppState(null);
                            //timer.cancel();
                            session.setBetId(null);



                        }


                    } else {
                        Toast.makeText(FinishWhotActivity.this, "Empty Response", Toast.LENGTH_SHORT).show();

                    }

                } else {
                    Toast.makeText(FinishWhotActivity.this, "Error: ", Toast.LENGTH_SHORT).show();

                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {

                Log.d("onFailure", t.toString());
            }
        });


    }

    public boolean checkInternet(){

        try {
            URL url = new URL("http://www.google.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            Toast.makeText(getApplicationContext(), "Internet is connected", Toast.LENGTH_SHORT).show();
            return true;
        } catch (MalformedURLException e) {
            Toast.makeText(getApplicationContext(), "Internet is not connected", Toast.LENGTH_SHORT).show();
            return false;
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Internet is not connected!", Toast.LENGTH_SHORT).show();
            return false;
        }

    }


    public boolean checkWinner(Integer numero,String player) {

        if(numero.equals(0)){

            RestService servicer = RestClient.getClient(session).create(RestService.class);
            Call<User> call = servicer.userOverX("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    if (response.isSuccessful()) {
                        if (response.body() != null) {

                        } else {

                        }

                    } else {


                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {

                }
            });

            if(player.equals("p1")){
                winner = "p1";

                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                dialog.setCancelable(false);
                dialog.setTitle("You Lost!");
                dialog.setMessage("Sorry, you're not lucky this time." );
                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        showpDialog();



                        Intent intentR = new Intent( FinishWhotActivity.this, MainActivity.class);
                        intentR.putExtra("stopper", "false");
                        intentR.putExtra("usernome", session.getUserID());
                        if(session.getUserID().equals(player1x)){
                            intentR.putExtra("userchaller", player2x);
                        } else {
                            intentR.putExtra("userchaller", player1x);
                        }
                        intentR.putExtra("betGame", "Finish&Count");
                        intentR.putExtra("bet_id", bet_id); Log.d("BET_ID","Msg: "+bet_id);
                        intentR.putExtra("game_id", Integer.toString(gameId));

                        startActivity(intentR);
                        hidepDialog();
                        FinishWhotActivity.this.finish();
                    }
                });
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    exitButton.setVisibility(View.VISIBLE);
                                    exitButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            RestService servicer = RestClient.getClient(session).create(RestService.class);
                                            Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                            call.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {

                                                        } else {

                                                        }

                                                    } else {


                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {

                                                }
                                            });
                                            session.setGameState(null);betgamedb.deleter("game_state");

                                            session.setChallengeState(null);

                                            session.setNotaRec(null);
                                            session.setNoteRec(null);
                                            session.setNotiRec(null);
                                            session.setNotoRec(null);
                                            session.setNotuRec(null);
                                            session.setNotyRec(null);
                                            session.setWinaRec(null);
                                            session.setLosaRec(null);
                                            session.setPlay("false");
                                            session.setReqNota(null);
                                            session.setUserBeta(null);
                                            session.setUserAcc(null);
                                            session.setBetId(null);

                                            Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                            intentOne.putExtra("stopper", "false");
                                            startActivity(intentOne);
                                            FinishWhotActivity.this.finish();
                                        }
                                    });
                                }
                            });

                final AlertDialog alert = dialog.create();
                gameOver = true;
                Handler handler = new Handler();
                int delay = 3000; //milliseconds

                handler.postDelayed(new Runnable(){
                    public void run(){
                        //do something
                        if(backpressed) {
                            //  alert.show();
                            //  playLoseSound();
                        } else {
                            if(!dialoger) {
                                if (!isFinishing()) {

                                    alert.show();
                                    playLoseSound();
                                } else {

                                }

                            }
                        }


                    }
                }, delay);
                session.setGameState(null);betgamedb.deleter("game_state");
                session.setEndState(gameId);
                session.setChallengeState(null);
                session.setAppState(null);
                // timer.cancel();
                session.setBetId(null);
                return true;
            } else {
                winner = "p2";

                AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                dialog.setCancelable(false);
                dialog.setTitle("You Won!");
                dialog.setMessage("Congratulations!" );
                dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        showpDialog();



                        Intent intentR = new Intent( FinishWhotActivity.this, MainActivity.class);
                        intentR.putExtra("stopper", "false");
                        intentR.putExtra("usernome", session.getUserID());
                        if(session.getUserID().equals(player1x)){
                            intentR.putExtra("userchaller", player2x);
                        } else {
                            intentR.putExtra("userchaller", player1x);
                        }
                        intentR.putExtra("betGame", "Finish&Count");
                        intentR.putExtra("bet_id", bet_id); Log.d("BET_ID","Msg: "+bet_id);
                        intentR.putExtra("game_id", Integer.toString(gameId));

                        startActivity(intentR);
                        hidepDialog();
                        FinishWhotActivity.this.finish();
                    }
                });
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        exitButton.setVisibility(View.VISIBLE);
                        exitButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                RestService servicer = RestClient.getClient(session).create(RestService.class);
                                Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                call.enqueue(new Callback<User>() {
                                    @Override
                                    public void onResponse(Call<User> call, Response<User> response) {

                                        if (response.isSuccessful()) {
                                            if (response.body() != null) {

                                            } else {

                                            }

                                        } else {


                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<User> call, Throwable t) {

                                    }
                                });
                                session.setGameState(null);betgamedb.deleter("game_state");

                                session.setChallengeState(null);

                                session.setNotaRec(null);
                                session.setNoteRec(null);
                                session.setNotiRec(null);
                                session.setNotoRec(null);
                                session.setNotuRec(null);
                                session.setNotyRec(null);
                                session.setWinaRec(null);
                                session.setLosaRec(null);
                                session.setPlay("false");
                                session.setReqNota(null);
                                session.setUserBeta(null);
                                session.setUserAcc(null);
                                session.setBetId(null);

                                Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                intentOne.putExtra("stopper", "false");
                                startActivity(intentOne);
                                FinishWhotActivity.this.finish();
                            }
                        });
                    }
                });

                final AlertDialog alert = dialog.create();
                gameOver = true;
                Handler handler = new Handler();
                int delay = 3000; //milliseconds

                handler.postDelayed(new Runnable(){
                    public void run(){
                        //do something
                        if(!dialoger) {
                            if (!isFinishing()) {

                                alert.show();
                                playWinSound();
                            } else {

                            }

                        }

                    }
                }, delay);
                session.setGameState(null);betgamedb.deleter("game_state");
                session.setEndState(gameId);
                session.setChallengeState(null);
                session.setAppState(null);
                //  timer.cancel();
                session.setBetId(null);
                return true;
            }
        }
        return false;
    }

    public void checkTimer() {
        timeX.setTextColor(Color.RED);
        //  timerx.start();
    }


    public boolean cardRuler(String boarderPart,Integer boarderNum,String cardorPart,Integer cardorNum,String player,String playerx,ImageView slotter){

        if(!gameOver) {

            if (boarderPart.equals(cardorPart)) {
                if (cardorNum.equals(2)) {
                    pickTwo = playerx;

                    if (Integer.parseInt(marketDeckSize) == 0) {
                        finishAndCount();

                    }

                    Toast.makeText(FinishWhotActivity.this, "Pick Two", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (cardorNum.equals(14)) {
                    generalMarket = playerx;
                    if (Integer.parseInt(marketDeckSize) == 0) {
                        finishAndCount();

                    }

                    Toast.makeText(FinishWhotActivity.this, "General Market", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (cardorNum.equals(1)) {
                    holdOn = playerx;
                    turnCounter = player;
                    Toast.makeText(FinishWhotActivity.this, "Hold On", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    turnCounter = playerx;
                 //   Toast.makeText(this, "CardRuler: "+playerx, Toast.LENGTH_SHORT).show();
                    if(playerx.equals("p2")){
                       // turner.setVisibility(View.VISIBLE);
                      //  timer.start();
                    }
                    else {
                       // turner.setVisibility(View.INVISIBLE);
                      // timer.cancel();
                    }
                    return true;
                }
            } else if (boarderNum.equals(cardorNum)) {

                // Continue here repeat the above here too
                if (cardorNum.equals(2)) {
                    pickTwo = playerx;

                    if (Integer.parseInt(marketDeckSize) == 0) {
                        finishAndCount();

                    }

                    Toast.makeText(FinishWhotActivity.this, "Pick Two", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (cardorNum.equals(14)) {
                    generalMarket = playerx;
                    if (Integer.parseInt(marketDeckSize) == 0) {
                        finishAndCount();

                    }

                    Toast.makeText(FinishWhotActivity.this, "General Market", Toast.LENGTH_SHORT).show();

                    return true;
                } else if (cardorNum.equals(1)) {
                    holdOn = playerx;
                    turnCounter = player;
                    Toast.makeText(FinishWhotActivity.this, "Hold On", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    turnCounter = playerx;
                    if(playerx.equals("p2")){
                      //  turner.setVisibility(View.VISIBLE);
                      //  timer.start();
                    }
                    else {
                     //   turner.setVisibility(View.INVISIBLE);
                      //  timer.cancel();
                    }
                    return true;
                }
            } else if (cardorPart.equals("w") || cardorNum >= 20) {
                if(turnCounter.equals("p2")) {
                    creatorWhotter(playerx);
                }
                return true;
            } else {
                return false;
            }
        } else if(cardp1.size() > 0){


            if (boarderPart.equals(cardorPart)) {
                if (cardorNum.equals(2)) {
                    pickTwo = playerx;

                    if (Integer.parseInt(marketDeckSize) == 0) {
                        finishAndCount();

                    }

                    Toast.makeText(FinishWhotActivity.this, "Pick Two", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (cardorNum.equals(14)) {
                    generalMarket = playerx;
                    if (Integer.parseInt(marketDeckSize) == 0) {
                        finishAndCount();

                    }

                    Toast.makeText(FinishWhotActivity.this, "General Market", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (cardorNum.equals(1)) {
                    holdOn = playerx;
                    turnCounter = player;
                    Toast.makeText(FinishWhotActivity.this, "Hold On", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    turnCounter = playerx;
                    //   Toast.makeText(this, "CardRuler: "+playerx, Toast.LENGTH_SHORT).show();
                    if(playerx.equals("p2")){
                        // turner.setVisibility(View.VISIBLE);
                        //  timer.start();
                    }
                    else {
                        // turner.setVisibility(View.INVISIBLE);
                        // timer.cancel();
                    }
                    return true;
                }
            } else if (boarderNum.equals(cardorNum)) {

                // Continue here repeat the above here too
                if (cardorNum.equals(2)) {
                    pickTwo = playerx;

                    if (Integer.parseInt(marketDeckSize) == 0) {
                        finishAndCount();

                    }

                    Toast.makeText(FinishWhotActivity.this, "Pick Two", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (cardorNum.equals(14)) {
                    generalMarket = playerx;
                    if (Integer.parseInt(marketDeckSize) == 0) {
                        finishAndCount();

                    }

                    Toast.makeText(FinishWhotActivity.this, "General Market", Toast.LENGTH_SHORT).show();

                    return true;
                } else if (cardorNum.equals(1)) {
                    holdOn = playerx;
                    turnCounter = player;
                    Toast.makeText(FinishWhotActivity.this, "Hold On", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    turnCounter = playerx;
                    if(playerx.equals("p2")){
                        //  turner.setVisibility(View.VISIBLE);
                        //  timer.start();
                    }
                    else {
                        //   turner.setVisibility(View.INVISIBLE);
                        //  timer.cancel();
                    }
                    return true;
                }
            } else if (cardorPart.equals("w") || cardorNum >= 20) {
                if(turnCounter.equals("p2")) {
                    creatorWhotter(playerx);
                }
                return true;
            } else {
                return false;
            }
        } else {

            Toast.makeText(FinishWhotActivity.this, "Game Over!", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }


    public void setPickTwo(String popTat1,String popTat2,String player,String playerx,ImageView slotter){

        if(turnCounter.equals("p2")){
            if (Integer.parseInt(marketDeckSize) == 0) {
                finishAndCount();

            }

        } else {
            String cd3;
            String cd3i;

            cd3 = popTat1;
            cd3i = popTat2;
            cardp2.add(cd3);
            cardp2.add(cd3i);

            cardName = cd3;
            cardNameN = cd3i;
            String[] cdx3 = cardName.split("z");
            String[] cdx3i = cardNameN.split("z");
            cardNamePart = cdx3[0];
            cardNamePartN = cdx3i[0];
            try {
                cardNameNum = Integer.parseInt(cdx3[1]);
            }
            catch (NumberFormatException e)
            {
                cardNameNum = Integer.valueOf(cdx3[1]);
            }

            try {
                cardNameNumN = Integer.parseInt(cdx3i[1]);
            }
            catch (NumberFormatException e)
            {
                cardNameNumN = Integer.valueOf(cdx3i[1]);
            }
            if(turnCounter.equals("p2")){
                creatorx(R.drawable.backofcard,cardName);
                creatorx(R.drawable.backofcard,cardNameN);
            } else {
                int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardName, null, null);
                int srcImagen = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardNameN, null, null);
                creator(srcImage,cardName);
                creator(srcImagen,cardNameN);
            }

            pickTwo = "none";
            turnCounter = player;

            if(player.equals("p2")){
                turner.setVisibility(View.VISIBLE);
                timer.start();
            }
        }


    }


    public void setGeneralMarket(String popTat,String player,String playerx,ImageView slotter){

        if(turnCounter.equals("p2")){
            if (Integer.parseInt(marketDeckSize) == 0) {
                finishAndCount();

            }
        } else {
            String cd1;

            cd1 = popTat;
            cardp2.add(cd1);

            cardName = cd1;
            String[] cdx1 = cardName.split("z");
            cardNamePart = cdx1[0];
            try {
                cardNameNum = Integer.parseInt(cdx1[1]);
            }
            catch (NumberFormatException e)
            {
                cardNameNum = Integer.valueOf(cdx1[1]);
            }

            if(turnCounter.equals("p2")){
                creatorx(R.drawable.backofcard,cardName);
            } else {
                int srcImage = getResources().getIdentifier("com.godwin.betgames:drawable/" + cardName, null, null);
                creator(srcImage,cardName);
            }
            generalMarket = "none";
            turnCounter = player;

            if(player.equals("p2")){
                turner.setVisibility(View.VISIBLE);
                timer.start();
            }
        }


    }



    private BroadcastReceiver finishMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String notar = intent.getStringExtra("Nota");
            String noter = intent.getStringExtra("Note");
            String notir = intent.getStringExtra("Noti");
            String notor = intent.getStringExtra("Noto");
            String notur = intent.getStringExtra("Notu");
            String notyr = intent.getStringExtra("Noty");

            String wina = intent.getStringExtra("Wina");
            String losa = intent.getStringExtra("Losa");

            session.setNotaRec(notar);
            session.setNoteRec(noter);
            session.setNotiRec(notir);
            session.setNotoRec(notor);
            session.setNotuRec(notur);
            session.setNotyRec(notyr);

            session.setWinaRec(wina);
            session.setLosaRec(losa);

            if (session.getWinaRec() != null) {
                String jsonWinner = session.getWinaRec();

                if(player.equals(jsonWinner)){
                    AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                    dialog.setCancelable(false);
                    dialog.setTitle("You Won!");
                    dialog.setMessage("Congratulations, Your Opponent surrendered." );
                    dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            showpDialog();



                            Intent intentR = new Intent( FinishWhotActivity.this, MainActivity.class);
                            intentR.putExtra("stopper", "false");
                            intentR.putExtra("usernome", session.getUserID());
                            if(session.getUserID().equals(player1x)){
                                intentR.putExtra("userchaller", player2x);
                            } else {
                                intentR.putExtra("userchaller", player1x);
                            }
                            intentR.putExtra("betGame", "Finish&Count");
                            intentR.putExtra("bet_id", bet_id); Log.d("BET_ID","Msg: "+bet_id);
                            intentR.putExtra("game_id", Integer.toString(gameId));

                            startActivity(intentR);
                            hidepDialog();
                            FinishWhotActivity.this.finish();
                        }
                    });
                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            exitButton.setVisibility(View.VISIBLE);
                            exitButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    RestService servicer = RestClient.getClient(session).create(RestService.class);
                                    Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                    call.enqueue(new Callback<User>() {
                                        @Override
                                        public void onResponse(Call<User> call, Response<User> response) {

                                            if (response.isSuccessful()) {
                                                if (response.body() != null) {

                                                } else {

                                                }

                                            } else {


                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<User> call, Throwable t) {

                                        }
                                    });
                                    session.setGameState(null);betgamedb.deleter("game_state");

                                    session.setChallengeState(null);

                                    session.setNotaRec(null);
                                    session.setNoteRec(null);
                                    session.setNotiRec(null);
                                    session.setNotoRec(null);
                                    session.setNotuRec(null);
                                    session.setNotyRec(null);
                                    session.setWinaRec(null);
                                    session.setLosaRec(null);
                                    session.setPlay("false");
                                    session.setReqNota(null);
                                    session.setUserBeta(null);
                                    session.setUserAcc(null);
                                    session.setBetId(null);

                                    Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                    intentOne.putExtra("stopper", "false");
                                    startActivity(intentOne);
                                    FinishWhotActivity.this.finish();
                                }
                            });
                        }
                    });

                    final AlertDialog alert = dialog.create();
                    gameOver = true;
                    Handler handler = new Handler();
                    int delay = 3000; //milliseconds

                    handler.postDelayed(new Runnable(){
                        public void run(){
                            //do something
                            if (Integer.parseInt(marketDeckSize) == 0) {
                                dialoger = true;
                            } else {

                            }
                            if(!dialoger){
                                if(!isFinishing())
                                {

                                    alert.show();
                                    playWinSound();
                                } else {

                                }

                            }

                        }
                    }, delay);
                    session.setGameState(null);betgamedb.deleter("game_state");
                    session.setEndState(gameId);
                    session.setChallengeState(null);
                    session.setAppState(null);
                    //   timer.cancel();
                    gameSound("stop");
                    soundOn = false;


                    //displayer("Game Over","Sorry, You lost!");
                    //handlerx.removeCallbacks(this);
                    freezeGame = true;
                    timer.cancel();
                    session.setNotaRec(null);
                    session.setNoteRec(null);
                    session.setNotiRec(null);
                    session.setNotoRec(null);
                    session.setNotuRec(null);
                    session.setNotyRec(null);
                    session.setWinaRec(null);
                    session.setLosaRec(null);
                    session.setBetId(null);
                } else {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(FinishWhotActivity.this);
                    dialog.setCancelable(false);
                    dialog.setTitle("You Lost!");
                    dialog.setMessage("Sorry, you're not lucky this time." );
                    dialog.setNegativeButton("Replay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            showpDialog();



                            Intent intentR = new Intent( FinishWhotActivity.this, MainActivity.class);
                            intentR.putExtra("stopper", "false");
                            intentR.putExtra("usernome", session.getUserID());
                            if(session.getUserID().equals(player1x)){
                                intentR.putExtra("userchaller", player2x);
                            } else {
                                intentR.putExtra("userchaller", player1x);
                            }
                            intentR.putExtra("betGame", "Finish&Count");
                            intentR.putExtra("bet_id", bet_id); Log.d("BET_ID","Msg: "+bet_id);
                            intentR.putExtra("game_id", Integer.toString(gameId));

                            startActivity(intentR);
                            hidepDialog();
                            FinishWhotActivity.this.finish();
                        }
                    });
                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    exitButton.setVisibility(View.VISIBLE);
                                    exitButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            RestService servicer = RestClient.getClient(session).create(RestService.class);
                                            Call<User> call = servicer.userOver("Bearer " + session.getUserToken(), session.getUserID(), player1x, player2x, Integer.toString(gameId));

                                            call.enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {

                                                    if (response.isSuccessful()) {
                                                        if (response.body() != null) {

                                                        } else {

                                                        }

                                                    } else {


                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {

                                                }
                                            });
                                            session.setGameState(null);betgamedb.deleter("game_state");

                                            session.setChallengeState(null);

                                            session.setNotaRec(null);
                                            session.setNoteRec(null);
                                            session.setNotiRec(null);
                                            session.setNotoRec(null);
                                            session.setNotuRec(null);
                                            session.setNotyRec(null);
                                            session.setWinaRec(null);
                                            session.setLosaRec(null);
                                            session.setPlay("false");
                                            session.setReqNota(null);
                                            session.setUserBeta(null);
                                            session.setUserAcc(null);
                                            session.setBetId(null);

                                            Intent intentOne = new Intent(FinishWhotActivity.this, MainActivity.class);
                                            intentOne.putExtra("stopper", "false");
                                            startActivity(intentOne);
                                            FinishWhotActivity.this.finish();
                                        }
                                    });
                                }
                            });

                    final AlertDialog alert = dialog.create();
                    gameOver = true;
                    Handler handler = new Handler();
                    int delay = 3000; //milliseconds

                    handler.postDelayed(new Runnable(){
                        public void run(){
                            //do something
                            if (Integer.parseInt(marketDeckSize) == 0) {
                                dialoger = true;
                            } else {

                            }
                            if(!dialoger) {
                                if (!isFinishing()) {

                                    alert.show();
                                    playLoseSound();
                                } else {

                                }

                            }

                        }
                    }, delay);
                    session.setGameState(null);betgamedb.deleter("game_state");
                    session.setEndState(gameId);
                    session.setChallengeState(null);
                    session.setAppState(null);
                    //   timer.cancel();
                    gameSound("stop");
                    soundOn = false;


                    //displayer("Game Over","Sorry, You lost!");
                    //handlerx.removeCallbacks(this);
                    freezeGame = true;
                    timer.cancel();
                    session.setNotaRec(null);
                    session.setNoteRec(null);
                    session.setNotiRec(null);
                    session.setNotoRec(null);
                    session.setNotuRec(null);
                    session.setNotyRec(null);
                    session.setWinaRec(null);
                    session.setLosaRec(null);
                    session.setBetId(null);
                }


            }

        }
    };


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



    private BroadcastReceiver callMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (TelephonyManager.ACTION_PHONE_STATE_CHANGED.equals(intent.getAction())) {

                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

                if (TelephonyManager.EXTRA_STATE_RINGING.equals(state)) {
                    //stop the player or mute the audio here
                    gameSound("stop");
                    Toast.makeText(FinishWhotActivity.this, "Stop Sound!", Toast.LENGTH_SHORT).show();
                } else if (TelephonyManager.EXTRA_STATE_IDLE.equals(state)) {
                    //start the player or unmute the audio here
                    gameSound("play");
                    Toast.makeText(FinishWhotActivity.this, "Play Sound!", Toast.LENGTH_SHORT).show();
                } else if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(state)) {
                    //stop the player or mute the audio here
                    gameSound("stop");
                    Toast.makeText(FinishWhotActivity.this, "Stop Sound!", Toast.LENGTH_SHORT).show();
                }
            }

        }
    };



}