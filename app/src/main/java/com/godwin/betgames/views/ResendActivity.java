package com.godwin.betgames.views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.godwin.betgames.PreferenceHelper;
import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.models.User;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResendActivity extends AppCompatActivity implements View.OnClickListener {

    private View view;

    private EditText coder,uuid;
    private Button sendButton;
    private TextView verify, login;
    private FragmentManager fragmentManager;
    private ProgressDialog pDialog;

    //Shared Preference
    private PreferenceHelper preferenceHelper;
    //Session Handler
    private Session userSession;

    public ResendActivity() {

    }


    String stopper = "false";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_resend);
        preferenceHelper = new PreferenceHelper(this);
        userSession = new Session(this);
        initViews();
        setListeners();

    }


    @Override
    public void onBackPressed() {
        /**
        new AlertDialog.Builder(this)
                .setTitle("Click OK to Exit")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // FinishWhotActivity.super.onBackPressed();
                        //Toast.makeText(MainActivity.this, "Game Over, You Lost!", Toast.LENGTH_LONG).show();
                        ResendActivity.this.finish();

                    }
                }).create().show();
         */

    }

    // Initiate Views
    private void initViews() {
        fragmentManager = getSupportFragmentManager();

        coder = findViewById(R.id.coder);
        uuid = findViewById(R.id.user_uuid);
        sendButton = findViewById(R.id.resenda);
        login = findViewById(R.id.loginBtn);
        verify = findViewById(R.id.verify);

        // Setting text selector over textviews
        int[][] states = new int[][] {
                new int[] {-android.R.attr.state_pressed}, // unpressed
                new int[] { android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[] {
                Color.WHITE,
                Color.BLACK
        };

        try {
            ColorStateList csl = new ColorStateList(states, colors);

            coder.setTextColor(csl);
        } catch (Exception e) {
        }
    }

    // Set Listeners
    private void setListeners() {
        sendButton.setOnClickListener(this);
        login.setOnClickListener(this);
        verify.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBtn:
                // Replace login activity
                Intent intentLog;
                intentLog = new Intent(ResendActivity.this, LoginActivity.class);
                startActivity(intentLog);
                break;

            case R.id.verify:

                // Go to verification
                Intent intentVer;
                intentVer = new Intent(ResendActivity.this, VerifyActivity.class);
                startActivity(intentVer);
                break;
            case R.id.resenda:

                // Resend sms code
                checkValidation1();
                break;
        }

    }


    // Check Validation before login
    private void checkValidation1() {
        // Get sms code
        String getUuid = uuid.getText().toString();

        // Check for both field is empty or not
        if (getUuid.equals("") || getUuid.length() == 0) {
            displayer("Error!","Check Fields.");

        }

        // Else do login and do your stuff
        else {
            if(userSession.getResendStop() == null){
                resendVerification();
                userSession.setResendStop("active");
            } else {
                Toast.makeText(this, "Contact Us to verify your account 08115219777 !", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    private void resendVerification() {
        pDialog = new ProgressDialog(this,  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showpDialog();

        final String uuider = uuid.getText().toString();

        RestService service = RestClient.getClient(userSession).create(RestService.class);
        Call<User> call = service.userResend(uuider);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                hidepDialog();

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String jsonResponse = response.body().toString();
                        Toast.makeText(ResendActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                        String tokener = response.body().getSuccess().getToken();
                        Session session = new Session(ResendActivity.this);
                        session.setUserToken(tokener);

                        // Display Response
                        Toast.makeText(ResendActivity.this,"SMS Sent!",Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(ResendActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else if (!response.isSuccessful()){
                    Toast.makeText(ResendActivity.this, "Invalid Username or Password", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ResendActivity.this, "Unable to Login", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
                Toast.makeText(ResendActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });


    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
