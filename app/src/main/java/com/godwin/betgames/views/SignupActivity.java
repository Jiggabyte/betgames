package com.godwin.betgames.views;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.godwin.betgames.PreferenceHelper;
import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.models.User;
import com.godwin.betgames.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {


    private View view;
    private EditText fullName, emailId, username, mobileNumber,
            password, confirmPassword;
    private TextView login;
    private Button signUpButton;
    private CheckBox terms_conditions;
    private ProgressDialog pDialog;

    //Shared Preference
    private PreferenceHelper preferenceHelper;
    //Session Handler
    private Session userSession;

    public SignupActivity() {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_signup);
        preferenceHelper = new PreferenceHelper(this);
        userSession = new Session(this);
        initViews();
        setListeners();
        userSession.setResendStop(null);
    }

    @Override
    public void onBackPressed() {
        /**
        new AlertDialog.Builder(this)
                .setTitle("Click OK to Exit")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        SignupActivity.this.finish();
                    }
                }).create().show();
         */

    }

    // Initialize all views
    private void initViews() {
        fullName = findViewById(R.id.fullName);
        emailId = findViewById(R.id.userEmailId);
        username = findViewById(R.id.userName);
        mobileNumber = findViewById(R.id.mobileNumber);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirmPassword);
        signUpButton = findViewById(R.id.signUpBtn);
        login = findViewById(R.id.already_user);
        terms_conditions = findViewById(R.id.terms_conditions);

        // Setting text selector over textviews
        int[][] states = new int[][] {
                new int[] {-android.R.attr.state_pressed}, // unpressed
                new int[] { android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[] {
                Color.WHITE,
                Color.BLACK
        };

        try {
            ColorStateList csl = new ColorStateList(states, colors);

            login.setTextColor(csl);
            terms_conditions.setTextColor(csl);
        } catch (Exception e) {
        }
    }

    // Set Listeners
    private void setListeners() {
        signUpButton.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpBtn:

                // Call checkValidation method
                checkValidation();
                break;

            case R.id.already_user:

                // Replace login activity
                Intent intentLogin;
                intentLogin = new Intent(SignupActivity.this, LoginActivity.class);
                intentLogin.putExtra("stopper","false");
                startActivity(intentLogin);
                break;
        }

    }

    // Check Validation Method
    private void checkValidation() {

        // Get all editText texts
        String getFullName = fullName.getText().toString();
        String getEmailId = emailId.getText().toString();
        String getUsernameId = username.getText().toString();
        String getMobileNumber = mobileNumber.getText().toString();
        String getPassword = password.getText().toString();
        String getConfirmPassword = confirmPassword.getText().toString();

        // Pattern match for email id
        Pattern p = Pattern.compile(Utils.regEx);
        Matcher m = p.matcher(getEmailId);

        // Check if all strings are null or not
        if (getFullName.equals("") || getFullName.length() == 0
                || getUsernameId.equals("") || getUsernameId.length() == 0
                || getMobileNumber.equals("") || getMobileNumber.length() == 0
                || getPassword.equals("") || getPassword.length() == 0
                || getConfirmPassword.equals("")
                || getConfirmPassword.length() == 0)

        displayer("Sign Up Error!","Check required fields.");

            // Check if email id valid or not
       // else if (!m.find())

       // displayer("Sign Up Error!","Your Email Id is Invalid.");

            // Check if both password should be equal
        else if (!getConfirmPassword.equals(getPassword))

        displayer("Sign Up Error!","Both password doesn't match.");

            // Make sure user should check Terms and Conditions checkbox
        else if (!terms_conditions.isChecked())

        displayer("Sign Up Error!","Please select Terms and Conditions.");

            // Else do signup or do your stuff
        else
            signUpByServer();

    }

    private void signUpByServer() {
        pDialog = new ProgressDialog(this,  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showpDialog();

        String name = fullName.getText().toString();
        String email = emailId.getText().toString();
        final String usernom = username.getText().toString();
        String passmot = password.getText().toString();
        String c_passmot = confirmPassword.getText().toString();
        final String phone = mobileNumber.getText().toString();

        RestService serviceSign = RestClient.getClient(userSession).create(RestService.class);
        Call<User> call = serviceSign.userSignUp(name,email,usernom,passmot,c_passmot,phone);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                hidepDialog();

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                       // String jsonResponse = response.body().toString();
                       // saveInfo(jsonResponse);
                        if (response.body().getErrorList() != null) {
                            if (response.body().getErrorList().getName() != null) {
                                // code block
                                Toast.makeText(SignupActivity.this, "Name Error: " + response.body().getErrorList().getName(), Toast.LENGTH_SHORT).show();
                            } else if(response.body().getErrorList().getEmail() != null){
                                // code block
                                Toast.makeText(SignupActivity.this, "Email Error: " + response.body().getErrorList().getEmail(), Toast.LENGTH_SHORT).show();

                            } else if(response.body().getErrorList().getPhone() != null){
                                // code block
                                Toast.makeText(SignupActivity.this, "Phone Error: " + response.body().getErrorList().getPhone(), Toast.LENGTH_SHORT).show();

                            } else if(response.body().getErrorList().getUuid() != null){
                                // code block
                                Toast.makeText(SignupActivity.this, "Username Error: The Username is already taken.", Toast.LENGTH_SHORT).show();

                            }

                           // Toast.makeText(SignupActivity.this, "Sign Error: "+response.body().getErrorList().toString(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignupActivity.this, "Sign Up Successful ", Toast.LENGTH_SHORT).show();
                            String tokener = response.body().getSuccess().getToken();
                            Session session = new Session(SignupActivity.this);
                            session.setUserToken(phone);
                            session.setUserID(usernom);
                            Toast.makeText(SignupActivity.this, "Check Your Email for Verification!", Toast.LENGTH_SHORT).show();

                            if (tokener == null) {
                                Toast.makeText(SignupActivity.this, "Unsuccessful!", Toast.LENGTH_SHORT).show();

                            } else {
                                // Replace account fragment with animation
                                Intent intentHome;
                                intentHome = new Intent(SignupActivity.this, VerifyActivity.class);
                                startActivity(intentHome);
                            }
                        }

                    } else {
                        Toast.makeText(SignupActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                    }


                } else {
                    Toast.makeText(SignupActivity.this, "Unable to Sign Up", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
                Toast.makeText(SignupActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void saveInfo(String response){
        preferenceHelper.putIsLogin(true);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equals("true")) {
                JSONArray dataArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataobj = dataArray.getJSONObject(i);
                    //preferenceHelper.putName(dataobj.getString("name"));
                    preferenceHelper.putToken(dataobj.getString("token"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }
    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void onLoginSuccess() {


    }
    public void onLoginFailed() {
        Toast.makeText(SignupActivity.this, "Login failed", Toast.LENGTH_LONG).show();
    }
}