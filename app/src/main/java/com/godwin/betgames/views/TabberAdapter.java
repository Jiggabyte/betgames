package com.godwin.betgames.views;

import android.content.Context;

import com.godwin.betgames.views.ui.bet.PendingBet;
import com.godwin.betgames.views.ui.bet.SentBet;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentManager;

public class TabberAdapter extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public TabberAdapter(Context c, FragmentManager fm, int totalTabs) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                PendingBet pendingFragment = new PendingBet();
                return pendingFragment;
            case 1:
                SentBet sentFragment = new SentBet();
                return sentFragment;
           // case 2:
            //    DoneBet doneFragment = new DoneBet();
            //    return doneFragment;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}