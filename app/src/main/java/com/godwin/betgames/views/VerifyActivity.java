package com.godwin.betgames.views;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.godwin.betgames.MainActivity;
import com.godwin.betgames.OnNewMessageListener;
import com.godwin.betgames.PreferenceHelper;
import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.SmsListener;
import com.godwin.betgames.models.Nota;
import com.godwin.betgames.models.User;
import com.godwin.betgames.models.Verify;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

public class VerifyActivity extends AppCompatActivity implements View.OnClickListener {

    private View view;

    private EditText coder,uuid;
    private Button sendButton;
    private TextView resend, login;
    private FragmentManager fragmentManager;
    private ProgressDialog pDialog;

    SmsListener smsListener;

    //Shared Preference
    private PreferenceHelper preferenceHelper;
    //Session Handler
    private Session userSession;
    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

    ValueEventListener verifyEventListener;

    public VerifyActivity() {

    }


    String stopper = "false";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_verify);
        preferenceHelper = new PreferenceHelper(this);
        userSession = new Session(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        initViews();
        setListeners();

        // Get firebase database reference
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference gameDbRef = database.getReference("verify");

        verifyEventListener = gameDbRef.child(userSession.getUserID()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Verify verify = dataSnapshot.getValue(Verify.class);

                if(verify != null){
                    if(userSession.getUserToken().equals(verify.getPhone())) {
                        coder.setText(verify.getSms_code());
                        uuid.setText(userSession.getUserID());

                        displayer("Verification","Please, Click Verify.");

                    } else {

                        displayer("Verification Error","Please, Retry");

                    }

                }




            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

                Log.w("FirebaseDB", "Failed to read value.", error.toException());
            }
        });

      //  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
           // requestSmsPermission();
       // else  {
          //  smsListener = new SmsListener();
          //  IntentFilter intentFilter = new IntentFilter();
          //  intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
           // registerReceiver(smsListener, intentFilter);
      //  }

    }


    private void requestSmsPermission() {
        String permission = Manifest.permission.RECEIVE_SMS;
        int grant = ContextCompat.checkSelfPermission(this, permission);
        if ( grant != PackageManager.PERMISSION_GRANTED) {
            String[] permission_list = new String[1];
            permission_list[0] = permission;
            ActivityCompat.requestPermissions(this, permission_list, 1);

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
         //   smsListener= new SmsListener();
         //   IntentFilter intentFilter=new IntentFilter();
         //   intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
           // registerReceiver(smsListener, intentFilter);
        }
    }


    @Override
    public void onBackPressed() {
        /**
        new AlertDialog.Builder(this)
                .setTitle("Click OK to Exit")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // FinishWhotActivity.super.onBackPressed();
                        //Toast.makeText(MainActivity.this, "Game Over, You Lost!", Toast.LENGTH_LONG).show();
                        VerifyActivity.this.finish();

                    }
                }).create().show();
         */
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    // Initiate Views
    private void initViews() {
        fragmentManager = getSupportFragmentManager();

        coder = findViewById(R.id.coder);
        uuid = findViewById(R.id.user_uuid);
        sendButton = findViewById(R.id.verifier);
        login = findViewById(R.id.loginBtn);
        resend = findViewById(R.id.resender);

        // Setting text selector over textviews
        int[][] states = new int[][] {
                new int[] {-android.R.attr.state_pressed}, // unpressed
                new int[] { android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[] {
                Color.WHITE,
                Color.BLACK
        };

        try {
            ColorStateList csl = new ColorStateList(states, colors);

            coder.setTextColor(csl);
        } catch (Exception e) {
        }
    }

    // Set Listeners
    private void setListeners() {
        sendButton.setOnClickListener(this);
        login.setOnClickListener(this);
        resend.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBtn:
                // Replace login activity
                Intent intentLog;
                intentLog = new Intent(VerifyActivity.this, LoginActivity.class);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                startActivity(intentLog);

                break;

            case R.id.resender:

                // Go to verification
                Intent intentRes;
                intentRes = new Intent(VerifyActivity.this, ResendActivity.class);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                startActivity(intentRes);
                break;
            case R.id.verifier:

                // Verify sms code
                checkValidation();
                break;
        }

    }



    public void handleMessage(String msg){

        checkValidationAndVerify(msg,userSession.getPhoneNo());

    }


    // Check Validation before login
    private void checkValidationAndVerify(String codix,String usix) {
        //Set field data
        coder.setText(codix);
        uuid.setText(usix);
        // Get sms code
        String getCode = coder.getText().toString().trim();
        String getUuid = uuid.getText().toString().trim();

        // Check for both field is empty or not
        if (getCode.equals("") || getCode.length() == 0 || getUuid.equals("") || getUuid.length() == 0) {
            displayer("Error!","Check Fields.");

        }

        // Else do login and do your stuff
        else
            verifyByServer();

    }

    // Check Validation before login
    private void checkValidation() {
        // Get sms code
        String getCode = coder.getText().toString().trim();
        String getUuid = uuid.getText().toString().trim();

        // Check for both field is empty or not
        if (getCode.equals("") || getCode.length() == 0 || getUuid.equals("") || getUuid.length() == 0) {
            displayer("Error!","Check Fields.");

        }

        // Else do login and do your stuff
        else
            verifyByServer();

    }


    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    private void verifyByServer() {
        pDialog = new ProgressDialog(this,  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showpDialog();

        final String coda = coder.getText().toString().trim();
        final String uuida = uuid.getText().toString().trim();

        RestService service = RestClient.getClient(userSession).create(RestService.class);
        Call<User> call = service.userVerify(coda,uuida);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                hidepDialog();

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String jsonResponse = response.body().toString();
                        Toast.makeText(VerifyActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                        String tokener = response.body().getSuccess().getToken();
                        String userone = response.body().getSuccess().getUuid();
                        Session session = new Session(VerifyActivity.this);
                        session.setUserToken(tokener);
                        session.setUserID(userone);

                        Intent intent = getIntent();

                        // Get the extras (if there are any)
                        Bundle extra = intent.getExtras();

                        if (extra != null) {

                            if (extra.containsKey("forgot") && intent.getStringExtra("forgot").equals("true")) {
                                // Load ChangeActivity
                                Intent intentChanger;
                                intentChanger = new Intent(VerifyActivity.this, ChangeActivity.class);
                                startActivity(intentChanger);
                                VerifyActivity.this.finish();
                            }
                        }

                        // Load ChangeActivity
                        Intent intentLogger;
                        intentLogger = new Intent(VerifyActivity.this, LoginActivity.class);
                        startActivity(intentLogger);
                        VerifyActivity.this.finish();


                    } else {
                        Toast.makeText(VerifyActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else if (!response.isSuccessful()){
                    Toast.makeText(VerifyActivity.this, "Invalid Username or Password ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(VerifyActivity.this, "Unable to Connect", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
                Toast.makeText(VerifyActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public static String userDeviceID(Session session) {
        String uniqueUUID;
        if (session.getDeviceID() != null) {
            uniqueUUID = session.getDeviceID();
        } else {
            uniqueUUID = UUID.randomUUID().toString();
            session.setDeviceID(uniqueUUID);
        }
        return uniqueUUID;
    }


    public synchronized static String id(Context context) {
        if (uniqueID != null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                Log.d("onUserUUID","uuid: "+UUID.randomUUID().toString());
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();
            }
        }
        return uniqueID;
    }

    private void saveUserInfo(String response){
        userSession.setUserJSON(response);
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



}
