package com.godwin.betgames.views.ui.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.Utils;
import com.godwin.betgames.models.Bet;
import com.godwin.betgames.models.User;
import com.godwin.betgames.views.BetgamesDBHelper;
import com.godwin.betgames.views.ClassicWhotActivity;
import com.godwin.betgames.views.FinishWhotActivity;
import com.godwin.betgames.views.LoginActivity;
import com.godwin.betgames.views.ui.challenger.ChallengerFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements View.OnClickListener {


    private ImageView whot2;
    private ImageView whot;
    private Button backGamer;
    Context context;
    Spinner spinner;
    private EditText betAmt;
    private String betGame;
    private Button betBtn;
    Spinner spinnerx;
    private EditText betAmtx;
    private String betGamex;
    private Button betBtnx;
    private ProgressDialog pDialog;
    private Session userSession;
    private TextView challenger_text;
    private FragmentManager fragmentManager;
    private SearchView searcher;
    private Button searchButton;
    private TextView user_nom_text;
    private TextView xcloser;
    private EditText challengeOne;
    private Button challenger;
    private LinearLayout linearView;
    private String loadCounter;
    private CardView card;
    private String queryGlobal;

    private BetgamesDBHelper betgamedb;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        userSession = new Session(getActivity());

        searcher = root.findViewById(R.id.searchHome);
        searchButton = root.findViewById(R.id.searchButtonHome);
        card = root.findViewById(R.id.cardis);


        whot2 = root.findViewById(R.id.whotr2);
        whot = root.findViewById(R.id.whotr);

        backGamer = root.findViewById(R.id.back_gamer);

        betgamedb = new BetgamesDBHelper(getActivity());

        String game_state = null;

        try {
            Cursor rs = betgamedb.getData("game_state");

            if (rs.moveToFirst()) {
                game_state = rs.getString(rs.getColumnIndex(BetgamesDBHelper.EXTRAS_COLUMN_DETAIL));
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }

        if (game_state != null) {

            backGamer.setVisibility(View.VISIBLE);
            whot.setVisibility(View.INVISIBLE);
            whot2.setVisibility(View.INVISIBLE);
        }

        //  challenger_text = root.findViewById(R.id.challenger_btnx);

        user_nom_text = root.findViewById(R.id.user_nom_text);
        xcloser = root.findViewById(R.id.cardis_closer);


        // challenger_text = root.findViewById(R.id.challenger_btnx);

        final Intent intentOne = new Intent(getActivity(), FinishWhotActivity.class);
        final Intent intentTwo = new Intent(getActivity(), ClassicWhotActivity.class);

        whot2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userSession.getUserToken() != null) {
                    // Replace account fragment with animation
                    Bundle bundle = new Bundle();
                    bundle.putString("game", "whot2");
                    // Replace account fragment with animation
                    fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(null)
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new ChallengerFragment(),
                                    Utils.Challenger_Fragment).commit();


                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);

                }
            }
        });

        whot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userSession.getUserToken() != null) {
                    // Replace account fragment with animation
                    Bundle bundle = new Bundle();
                    bundle.putString("game", "whot");
                    // Set Fragment Class Arguments
                    // Replace account fragment with animation
                    fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(null)
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new ChallengerFragment(),
                                    Utils.Challenger_Fragment).commit();


                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);

                }

            }
        });
        /**
         challenger_text.setOnClickListener(new View.OnClickListener(){
        @Override public void onClick(View view) {
        if(userSession.getUserToken() != null){
        // Replace Bets Activity
        Intent intentBets;
        intentBets = new Intent(getActivity(), BetsActivity.class);
        startActivity(intentBets);

        } else {
        // Replace login activity
        Intent intentLogin;
        intentLogin = new Intent(getActivity(), LoginActivity.class);
        startActivity(intentLogin);
        }

        }
        });
         **/

        backGamer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Load Current Game
                Intent intentGame;
                if (userSession.getGameState().equals("Classic")) {
                    intentGame = new Intent(getActivity(), ClassicWhotActivity.class);
                } else {
                    intentGame = new Intent(getActivity(), FinishWhotActivity.class);
                }
                startActivity(intentGame);
                getActivity().finish();

            }
        });

        searcher.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchUserFromServer(query);
                queryGlobal = query;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });


        userSession = new Session(getActivity());

        spinnerx = root.findViewById(R.id.game_choicer);
        betAmtx = root.findViewById(R.id.challenger_one);
        betBtnx = root.findViewById(R.id.challengeButton);


        spinner = root.findViewById(R.id.game_choice);
        betAmt = root.findViewById(R.id.challenger_edit);
        betBtn = root.findViewById(R.id.challengerBtn);


        String[] games = {"Finish&Count Whot", "Classic Whot"};


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, games);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        Drawable spinnerDrawable = spinner.getBackground().getConstantState().newDrawable();
        spinnerDrawable.setColorFilter(getResources().getColor(R.color.white, getActivity().getTheme()), PorterDuff.Mode.SRC_ATOP);
        spinner.setBackground(spinnerDrawable);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        betGame = "Finish&Count";
                        break;
                    case 1:
                        betGame = "Classic";
                        break;
                    default:
                        betGame = "Nil";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                betGame = "Nil";

            }
        });


        betBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check for both field is empty or not
                if (betAmt.equals("") || betAmt.length() == 0
                        || betGame.equals("Nil") || Integer.parseInt(betAmt.getText().toString().trim()) < 99 && Integer.parseInt(betAmt.getText().toString().trim()) != 0) {
                    displayer("Bet Error!", "Check Details.");

                } else {
                    sendBetToServer();
                }

            }
        });


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check for both field is empty or not
                if (searcher.getQuery().toString().equals("")) {
                    displayer("Search Error!", "Check Details.");

                } else {
                    searchUserFromServer(searcher.getQuery().toString());
                }

            }
        });


        return root;
    }


    private void searchUserFromServer(String query) {
        pDialog = new ProgressDialog(getContext(), R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Searching...");
        pDialog.setCancelable(false);

        showpDialog();
        card.setVisibility(View.INVISIBLE);
        RestService serviceBet = RestClient.getClient(userSession).create(RestService.class);
        Call<User> call = serviceBet.userSearch("Bearer " + userSession.getUserToken(), userSession.getUserID(), query);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                hidepDialog();

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String counta = response.body().getDetail().getCount();
                        if (counta.equals("0")) {

                            displayer("Search:", "No Match Found!");
                        } else {

                            String usernon = response.body().getDetail().getUuid();
                            card.setVisibility(View.VISIBLE);
                            xcloser.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    card.setVisibility(View.INVISIBLE);
                                }
                            });

                            user_nom_text.setText(usernon);

                            String[] gamesx = {"Finish&Count Whot", "Classic Whot"};

                            ArrayAdapter<String> adapterx = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, gamesx);
                            spinnerx.setAdapter(adapterx);

                            spinnerx.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    switch (position) {
                                        case 0:
                                            betGamex = "Finish&Count";
                                            break;
                                        case 1:
                                            betGamex = "Classic";
                                            break;
                                        default:
                                            betGamex = "Nil";
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    betGamex = "Nil";
                                }
                            });


                            betBtnx.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // Check for both field is empty or not
                                    if (betAmtx.equals("") || betAmtx.length() == 0
                                            || betGamex.equals("Nil") || Integer.parseInt(betAmtx.getText().toString().trim()) < 99 && Integer.parseInt(betAmtx.getText().toString().trim()) != 0) {
                                        displayer("Challenge Error!", "Check Details.");

                                    } else {
                                        sendBetToServerX();
                                    }

                                }
                            });

                        }

                    } else {
                        Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                    }
                } else if (!response.isSuccessful()) {
                    Log.d("SearchUser", response.toString());

                    Toast.makeText(getActivity(), "Search Error! ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Unable to Connect", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hidepDialog();
                Log.d("onFailureUser", t.toString());
                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onClick(View view) {


    }

    private void sendBetToServer() {
        pDialog = new ProgressDialog(getContext(), R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showpDialog();

        final String betAmtInfo = betAmt.getText().toString();
        final String betGameInfo = betGame;
        final String uuid = userSession.getUserID();
        final String state = "initiated";
        Date dayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en", "NG"));
        final String toDate = formatter.format(dayDate);


        RestService serviceBet = RestClient.getClient(userSession).create(RestService.class);
        Call<Bet> call = serviceBet.userBet("Bearer " + userSession.getUserToken(), uuid, betAmtInfo, betGameInfo, state, toDate);
        call.enqueue(new Callback<Bet>() {
            @Override
            public void onResponse(Call<Bet> call, Response<Bet> response) {
                hidepDialog();

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String responser = response.body().getSuccess().getNota();
                        Toast.makeText(getActivity(), responser, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                    }
                } else if (!response.isSuccessful()) {
                    Log.d("onUnsuccessfullBet", response.toString());
                    Toast.makeText(getActivity(), "Failed Check Balance or Placed Number of Bets!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Unable to Connect", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Bet> call, Throwable t) {
                hidepDialog();
                Log.d("onFailureBet", t.toString());
                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void sendBetToServerX() {
        pDialog = new ProgressDialog(getContext(), R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showpDialog();

        final String betAmtInfo = betAmtx.getText().toString();
        final String betGameInfo = betGamex;
        final String uuid = userSession.getUserID();
        final String betGameUserInfo = user_nom_text.getText().toString().trim();
        final String state = "private";
        Date dayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en", "NG"));
        final String toDate = formatter.format(dayDate);


        RestService serviceBet = RestClient.getClient(userSession).create(RestService.class);
        Call<Bet> call = serviceBet.userBetX("   Bearer " + userSession.getUserToken(), uuid, betGameUserInfo, betAmtInfo, betGameInfo, state, toDate);
        call.enqueue(new Callback<Bet>() {
            @Override
            public void onResponse(Call<Bet> call, Response<Bet> response) {
                hidepDialog();

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String responser = response.body().getSuccess().getNota();
                        Toast.makeText(getActivity(), responser, Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                    }
                } else if (!response.isSuccessful()) {
                    Log.d("onUnsuccessfulBet", response.toString());
                    Toast.makeText(getActivity(), "Failed Check Balance or Placed Number of Bets!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Unable to Connect", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Bet> call, Throwable t) {
                hidepDialog();
                Log.d("onFailureBet", t.toString());
                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void displayer(String titler, String messager) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}