package com.godwin.betgames.views.ui.wallet;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.Utils;
import com.godwin.betgames.models.LoadTrans;
import com.godwin.betgames.models.Transaction;
import com.godwin.betgames.models.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletFragment extends Fragment {

    private ProgressDialog pDialog;
    private  TextView monyVal;
    private TextView monyTake;
    Session session;
    Button refresher;
    private LinearLayout linearView;
    private FragmentManager fragmentManager;
    ImageView noImg;

    @SuppressLint("SetJavaScriptEnabled")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_walleter, container, false);
        session = new Session(getActivity());

       // ImageView depositButton = root.findViewById(R.id.depImgBut);
       // ImageView withdrawButton = root.findViewById(R.id.withImgBut);

        noImg = root.findViewById(R.id.noDataImg);

       // TextView depositTXT = root.findViewById(R.id.depTxt);
       // TextView withdrawTXT = root.findViewById(R.id.withTxt);

        monyVal = root.findViewById(R.id.monyVal);
        monyTake = root.findViewById(R.id.moniTake);
        refresher = root.findViewById(R.id.reloader);

        linearView = root.findViewById(R.id.list_box);

        RestService service = RestClient.getClient(session).create(RestService.class);
        Call<User> call = service.userBalance("Bearer " + session.getUserToken(), session.getUserID());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        monyVal.setText("₦"+response.body().getSuccess().getCredit());
                        monyTake.setText("₦"+response.body().getSuccess().getBalance());
                    }
                } else {
                    Toast.makeText(getActivity(), "Failure!", Toast.LENGTH_LONG).show();
                    refresher.setVisibility(View.VISIBLE);
                    refresher.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Replace account fragment with animation
                            fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction
                                    .addToBackStack(null)
                                    //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                    .replace(R.id.home_host, new WalletFragment(),
                                            Utils.Wallet_Fragment).commit();

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_LONG).show();
                refresher.setVisibility(View.VISIBLE);
                refresher.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Replace account fragment with animation
                        fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction
                                .addToBackStack(null)
                                //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                .replace(R.id.home_host, new WalletFragment(),
                                        Utils.Wallet_Fragment).commit();

                    }
                });

            }
        });



        pDialog = new ProgressDialog(getActivity(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);



        RestService serviceTrans = RestClient.getClient(session).create(RestService.class);
        Call<LoadTrans> calls = serviceTrans.userTrans("Bearer "+session.getUserToken(),session.getUserID());
        calls.enqueue(new Callback<LoadTrans>() {
            @Override
            public void onResponse(Call<LoadTrans> call, Response<LoadTrans> response) {

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for(int k=0;k<linearView.getChildCount();k++){
                            linearView.getChildAt(k).setVisibility(View.INVISIBLE);
                        }

                        ArrayList<Transaction> responseMsg = response.body().getTransactioner().getTransaction();
                        if(responseMsg.size() == 0){
                            noImg.setVisibility(View.VISIBLE);
                            noImg.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // Replace account fragment with animation
                                    fragmentManager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction
                                            .addToBackStack(null)
                                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                            .replace(R.id.home_host, new WalletFragment(),
                                                    Utils.Wallet_Fragment).commit();

                                }
                            });
                        } else {
                            for (int i = 0; i < responseMsg.size(); i++) {
                                GsonBuilder gsonBuilder = new GsonBuilder();
                                Gson gson = gsonBuilder.create();
                                String objecto = gson.toJson(responseMsg.get(i));
                                try {
                                    JSONObject objector = new JSONObject(objecto);
                                    String responseDate = objector.getString("created_at");
                                    String[] responseArr = responseDate.split(" ");
                                    String responseTitle = objector.getString("title");
                                    String responseAmt = "₦" + objector.getString("amount");

                                    linearView.getChildAt(i).setVisibility(View.VISIBLE);
                                    CardView cardia = (CardView) linearView.getChildAt(i);
                                    TextView textName = (TextView) cardia.getChildAt(0);
                                    TextView textDet = (TextView) cardia.getChildAt(1);
                                    TextView textAmt = (TextView) cardia.getChildAt(2);

                                    textName.setText(responseArr[0]);
                                    textDet.setText(responseTitle);
                                    textAmt.setText(responseAmt);

                                } catch (JSONException error) {
                                    error.printStackTrace();
                                }


                            }
                        }



                    } else {
                        Toast.makeText(getActivity(),"Empty response",Toast.LENGTH_LONG).show();
                        noImg.setVisibility(View.VISIBLE);
                        noImg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // Replace account fragment with animation
                                fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction
                                        .addToBackStack(null)
                                        //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                        .replace(R.id.home_host, new WalletFragment(),
                                                Utils.Wallet_Fragment).commit();

                            }
                        });
                    }
                } else {

                    Log.d("onFailureBet", response.toString());
                    Toast.makeText(getActivity(), "Load Error!", Toast.LENGTH_SHORT).show();
                    noImg.setVisibility(View.VISIBLE);
                    noImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Replace account fragment with animation
                            fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction
                                    .addToBackStack(null)
                                    //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                    .replace(R.id.home_host, new WalletFragment(),
                                            Utils.Wallet_Fragment).commit();

                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<LoadTrans> call, Throwable t) {
                Log.d("onFailureBet", t.toString());
                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                noImg.setVisibility(View.VISIBLE);
                noImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Replace account fragment with animation
                        fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction
                                .addToBackStack(null)
                                //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                .replace(R.id.home_host, new WalletFragment(),
                                        Utils.Wallet_Fragment).commit();

                    }
                });
            }
        });



        /**
        depositButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showpDialog();
                // Replace account fragment with animation
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction
                        .addToBackStack(null)
                        //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                        .replace(R.id.home_host, new WebFragment(),
                                Utils.Web_Fragment).commit();
                hidepDialog();

            }
        });
        withdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showpDialog();
                // Replace account fragment with animation
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction
                        .addToBackStack(null)
                        //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                        .replace(R.id.home_host, new Web2Fragment(),
                                Utils.Web2_Fragment).commit();
                hidepDialog();
            }
        });



        depositTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showpDialog();
                // Replace account fragment with animation
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction
                        .addToBackStack(null)
                        //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                        .replace(R.id.home_host, new WebFragment(),
                                Utils.Web_Fragment).commit();
                hidepDialog();

            }
        });
        withdrawTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showpDialog();
                // Replace account fragment with animation
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction
                        .addToBackStack(null)
                        //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                        .replace(R.id.home_host, new WebFragment(),
                                Utils.Web2_Fragment).commit();
                hidepDialog();

            }
        });

        **/
        return root;
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}