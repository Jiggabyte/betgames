package com.godwin.betgames.views.ui.web;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.Utils;
import com.godwin.betgames.models.User;
import com.godwin.betgames.views.WebAppInterface;
import com.godwin.betgames.views.ui.challenger.ChallengerFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Web2Fragment extends Fragment implements View.OnClickListener{

    Toolbar toolbarx;
    private ProgressDialog pDialog;
    WebView mWebView;
    WebSettings settings;
    private ProgressBar progressBar;
    Session session;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_web, container, false);
        
        session = new Session(getActivity());

        session.setBackState("non-active-x");

        toolbarx = root.findViewById(R.id.toolbarx);
        toolbarx.setNavigationIcon(R.drawable.back_arrow);


        pDialog = new ProgressDialog(getActivity(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        // stopService(new Intent(this, GameService.class));

        RestService service = RestClient.getClient(session).create(RestService.class);
        Call<User> call = service.userSetOnline("Bearer " + session.getUserToken(), session.getUserID());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

        toolbarx.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  onBackPressed();
                // Load MainActivity
                // Replace account fragment with animation
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction
                        .addToBackStack(null)
                        //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                        .replace(R.id.home_host, new WebFragment(),
                                Utils.Web_Fragment).commit();
            }
        });

        // showpDialog();

        mWebView = root.findViewById(R.id.webView);

        settings = mWebView.getSettings();

        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);

        mWebView.addJavascriptInterface(new WebAppInterface(getActivity()), "Android");

        // mWebView.setWebViewClient(new WebViewClient());

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
        });

        mWebView.setWebViewClient(new MyWebClient());

        mWebView.loadUrl("https://web.betgamesng.com/withdrawal?uuid="+session.getUserID());



        return root;

    }


    public void displayConfirm(String titler, String messager, final String[] parax){
        if(session.getChallengeState() != null && session.getChallengeState().equals("active") && session.getAppState() != null && session.getAppState().equals("true")){
            // Toast.makeText(getApplicationContext(), "Display No Confirm !!! "+session.getChallengeState(), Toast.LENGTH_SHORT).show();
        } else {
            if (session.getBackState().equals("active")) {
                /**
                 Intent intentBX = new Intent(getActivity(),DialogActivity.class);
                 intentBX.putExtra("title",titler);
                 intentBX.putExtra("message",messager);
                 intentBX.putExtra("parax",parax);
                 intentBX.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                 startActivity(intentBX);
                 */

            } else {

                session.setChallengeState("active");

                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Light_Dialog);
                dialog.setCancelable(false);
                dialog.setTitle(titler);
                dialog.setMessage(messager+"\r\nOpponent: "+parax[2]+"\r\nBet Amount: ₦"+parax[4]);
                session.setPlay(null);
                session.setReqTitle(null);
                session.setReqStop(null);
                session.setReqNota(null);
                session.setUserBeta(null);
                session.setUserAcc(null);
                session.setBetId(parax[3]);

                dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.d("ReceiverCancelled", "Nota Clicked!" + parax[1]);

                        RestService serviceStart = RestClient.getClient(session).create(RestService.class);
                        Call<User> callStop = serviceStart.userStop("Bearer " + session.getUserToken(), parax[1], parax[1], parax[2], parax[3]);
                        callStop.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {

                                //Toast.makeText()
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        session.setChallengeState(null);
                                        session.setAppState("true");

                                        session.setNotaRec(null);
                                        session.setNoteRec(null);
                                        session.setNotiRec(null);
                                        session.setNotoRec(null);
                                        session.setNotuRec(null);
                                        session.setNotyRec(null);
                                        session.setWinaRec(null);
                                        session.setLosaRec(null);
                                        session.setPlay("false");
                                        session.setReqNota(null);
                                        session.setUserBeta(null);
                                        session.setUserAcc(null);
                                        session.setBetId(null);

                                        String jsonResponse = response.body().getSuccessor();
                                        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                        ChallengerFragment challengerFrag = new ChallengerFragment();
                                        if (challengerFrag.isAdded()) {
                                            challengerFrag.removeHandler();
                                        }

                                        Log.d("onStop", "Success: " + response.body().getSuccessor());


                                    } else {
                                        session.setChallengeState(null);
                                        Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    session.setChallengeState(null);
                                    Toast.makeText(getActivity(), "Unable to fetch data", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                session.setChallengeState(null);
                                Log.d("onFailure", t.toString());
                                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.d("ReceiverConfirm", "Nota Clicked!" + parax[2]);

                        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date now = new Date();
                        String strDate = sdfDate.format(now);

                        Toast.makeText(getActivity(), "Time Check: "+findDifference(parax[0],strDate)+" first :"+parax[0]+" second: "+strDate, Toast.LENGTH_SHORT).show();
                        if (findDifference(parax[0],strDate) >= 60) {
                            displayer("Game Challenge.", "The Game Challenge Invitation has Expired!");
                            session.setAppState(null);
                            session.setChallengeState(null);
                            session.setBetId(null);

                        } else {
                            showpDialog();

                            RestService serviceConfirm = RestClient.getClient(session).create(RestService.class);
                            Call<User> call = serviceConfirm.userConfirm("Bearer " + session.getUserToken(), parax[1], parax[2], parax[3]);
                            call.enqueue(new Callback<User>() {
                                @Override
                                public void onResponse(Call<User> call, Response<User> response) {

                                    if (response.isSuccessful()) {
                                        if (response.body() != null) {
                                            String jsonResponse = response.body().getSuccessor();

                                            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                            ChallengerFragment challengerFrag = new ChallengerFragment();
                                            if (challengerFrag.isAdded()) {
                                                challengerFrag.removeHandler();
                                            }

                                            Log.d("onConfirm", "Success: " + response.body().getSuccessor());

                                        } else {
                                            session.setChallengeState(null);
                                            session.setAppState(null);
                                            session.setBetId(null);
                                            Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        session.setChallengeState(null);
                                        session.setAppState(null);
                                        session.setBetId(null);
                                        hidepDialog();
                                        Toast.makeText(getActivity(), "Unable to fetch data", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<User> call, Throwable t) {
                                    session.setChallengeState(null);
                                    session.setAppState(null);
                                    session.setBetId(null);
                                    hidepDialog();
                                }
                            });
                        }


                    }
                });
                final AlertDialog alert = dialog.create();
                if (!getActivity().isFinishing()) {
                    alert.show();
                    playChallengeSound();
                } else {
                    // Toast.makeText(this, "Display: Error here !!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }


    public void playChallengeSound(){
        MediaPlayer mediaPlayer;
        mediaPlayer = MediaPlayer.create(getActivity(), R.raw.challenge);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }

    // Function to print difference in
    // time start_date and end_date
    private long findDifference(String start_date,String end_date) {

        long result = 0;
        // SimpleDateFormat converts the
        // string format to date object
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Try Block
        try {

            // parse method is used to parse
            // the text from a string to
            // produce the date
            Date d1 = sdf.parse(start_date);
            Date d2 = sdf.parse(end_date);

            // Calucalte time difference
            // in milliseconds
            long difference_In_Time
                    = d2.getTime() - d1.getTime();

            // Calucalte time difference in
            // seconds, minutes, hours, years,
            // and days
            long difference_In_Seconds
                    = (difference_In_Time
                    / 1000);
            // % 60;

            result = difference_In_Seconds;
        }

        // Catch the Exception
        catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public class MyWebClient extends WebViewClient
    {


        public MyWebClient() {
            pDialog = new ProgressDialog(getActivity(),  R.style.AppTheme_Dialog);
            pDialog.setIndeterminate(true);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            showpDialog();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            hidepDialog();
        }

        @SuppressWarnings("deprecation")
        public void onReceivedError(WebView webView, int errorCode, String description, String failingUrl) {
            try {
                webView.stopLoading();
            } catch (Exception e) {
                Toast.makeText(getActivity(), "Not Loading!", Toast.LENGTH_SHORT).show();
            }

            if (webView.canGoBack()) {
                webView.goBack();
            }

            webView.loadUrl("about:blank");
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Check your internet connection and try again.");
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Try Again", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                    startActivity(getActivity().getIntent());
                }
            });

            alertDialog.show();
            super.onReceivedError(webView, errorCode, description, failingUrl);
        }

        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
            // Redirect to deprecated method, so you can use it in all SDK versions
            onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
        }


    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @Override
    public void onClick(View v) {

    }

    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(),R.style.Theme_AppCompat_Light_Dialog);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    public void displayStop(String titler, String messager){

        session.setChallengeState(null);
        // session.setAppState(null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        session.setPlay(null);
        session.setReqTitle(null);
        session.setReqStop(null);
        session.setReqNota(null);
        session.setUserBeta(null);
        session.setUserAcc(null);
        //  session.setBetId(null);

        dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                RestService service = RestClient.getClient(session).create(RestService.class);
                Call<User> call = service.userRemStop("Bearer " + session.getUserToken(), session.getUserID());
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        //Toast.makeText()
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                Log.d("StopConfirm", "Yeta Clicked!");
                                // Replace account fragment with
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction ft = fragmentManager.beginTransaction();
                                ft.detach(Web2Fragment.this).attach(Web2Fragment.this).commit();
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast.makeText(getActivity(), "Request Cancelled Failed!", Toast.LENGTH_SHORT).show();

                    }
                });


            }
        });

        final AlertDialog alert = dialog.create();
        alert.show();

    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}